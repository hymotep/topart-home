import { GET_YEAR_COMPTA, GET_COMPTAS, COMPTA_ERROR } from '../actions/types'

const initialState = {
  yearcompta: null,
  comptas: null,
  loading: true,
  error: {}
}

export default function (state = initialState, action) {
  const { type, payload } = action

  switch (type) {
    case GET_YEAR_COMPTA:
      return {
        ...state,
        yearcompta: payload,
        loading: false
      }
    case GET_COMPTAS:
      return {
        ...state,
        comptas: payload,
        loading: false
      }

    case COMPTA_ERROR:
      return {
        ...state,
        error: payload,
        loading: false
      }

    default:
      return state
  }
}
