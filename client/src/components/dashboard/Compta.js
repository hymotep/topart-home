import React, { useState, useEffect } from 'react'
import { Modal, Button, Form } from 'react-bootstrap'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Spinner from '../layout/Spinner'

import { getThisYearCompta, getStat } from '../../store/actions/compta.js'

import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
  BarChart,
  Bar
} from 'recharts'

const Compta = ({
  compta: { yearcompta, comptas },
  getThisYearCompta,
  getStat
}) => {
  let temp = new Date().getMonth()
  let temp2 = temp === 12 ? '1' : temp + 1
  let month = temp2 < 10 ? '0' + temp2.toString() : temp2.toString()
  let [formdata, setformdata] = React.useState({ date: month })

  useEffect(() => {
    getThisYearCompta()
  }, [getThisYearCompta])
  useEffect(() => {
    getStat(month)
  }, [])

  const chooseDate = (e) => {
    setformdata({ ...formdata, [e.target.name]: e.target.value })
    getStat(e.target.value)
  }

  return (
    yearcompta &&
    comptas && (
      <div>
        {/* <select onChange={chooseDate} name="date" value={formdata.date}>
          <option value="0">Full Year</option>
          <option value="01">Janvier</option>
          <option value="02">Fevrier</option>
          <option value="03">Mars</option>
          <option value="04">Avril</option>
          <option value="05">Mai</option>
          <option value="06">Juin</option>
          <option value="07">Juillet</option>
          <option value="08">Aout</option>
          <option value="09">Septembre</option>
          <option value="10">Octobre</option>
          <option value="11">Novembre</option>
          <option value="12">Decembre</option>
        </select> */}

        <div>Total Htva : {yearcompta.totalHtva}</div>
        <div>Total tva : {yearcompta.tvaAmount}</div>
        <h2>Statistique Devis </h2>
        <div>
          <LineChart
            width={600}
            height={300}
            data={comptas.devis}
            margin={{
              top: 5,
              right: 30,
              left: 20,
              bottom: 5
            }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip />
            <Legend />
            <Line
              type="monotone"
              dataKey="total"
              stroke="#8884d8"
              activeDot={{ r: 12 }}
            />
            <Line
              type="monotone"
              dataKey="accepte"
              stroke="#82ca9d"
              activeDot={{ r: 12 }}
            />
          </LineChart>
        </div>
        <div>
          <BarChart
            width={600}
            height={300}
            data={comptas.devis}
            margin={{
              top: 5,
              right: 30,
              left: 20,
              bottom: 5
            }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip />
            <Legend />
            {/* 
            <Bar dataKey="totalHtva" fill="#82ca9d" />
            <Bar dataKey="acceptedHtva" fill="#8884d8" /> */}

            <Bar dataKey="accepteHtva" stackId="a" fill="#82ca9d" />
            <Bar dataKey="totalHtva" stackId="a" fill="#8884d8" />
          </BarChart>
        </div>
      </div>
    )
  )
}

Compta.propTypes = {
  compta: PropTypes.object.isRequired,

  getThisYearCompta: PropTypes.func.isRequired,
  getStat: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  compta: state.compta
})

export default connect(mapStateToProps, { getThisYearCompta, getStat })(Compta)
