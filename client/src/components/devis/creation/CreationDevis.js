import React from 'react'
import PropTypes from 'prop-types'

import { connect } from 'react-redux'
import { getAllUsers } from '../../../store/actions/user.js'
import { Table, Form, Col, Button } from 'react-bootstrap'
import '../../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css'

import { uploadDevisCustom } from '../../../store/actions/devis.js'

const CreationDevis = ({
  demande: { onedemande },
  getAllUsers,
  user: { users },
  uploadDevisCustom
}) => {
  // useEffect(() => {
  //   getAllUsers()
  // }, [])

  ///////////////prendre photo////////////////

  // function capture() {
  //   html2canvas(document.querySelector('#deviss'), {
  //     scrollY: -window.scrollY,
  //     scrollX: -window.scrollX
  //   }).then((canvas) => {
  //     // document.body.appendChild(canvas) // if you want see your screenshot in body.
  //     const imgData = canvas.toDataURL('image/png')
  //     const pdf = new jsPDF()
  //     pdf.setFontSize(8)
  //     pdf.addImage(logo, 10, 10, 80, 50)
  //     pdf.addImage(imgData, 'PNG', 15, 100)
  //     pdf.text(150, 15, 'Client : nom client')
  //     pdf.text(150, 25, 'Devis : num devis')

  //     pdf.save('num devis' + '.pdf')
  //   })
  // }

  /////////////////////////////////////////////////

  let list = ['', '', '', '']
  let calcul = {
    totalHtva: 0,
    totalTvac: 0,
    tva: 21,
    tvaAmount: 0
  }
  let data = {
    client: false,
    name: '',
    email: '',
    commentaire: '',
    titre: '',
    dateDeb: '',
    dateFin: ''
  }

  let [formData, setformData] = React.useState(data)

  const [inputList, setInputList] = React.useState([list])
  const [totalList, setTotalList] = React.useState(calcul)
  const [errors, setErrors] = React.useState({})
  // handle input change
  const handleInputChange = (e, index) => {
    const { name, value } = e.target
    const list = [...inputList]

    if (name === '3') {
      list[index][3] = Number(value)
      list[index][2] = ''
      list[index][1] = ''
    } else if (name === '2' || name === '1') {
      list[index][name] = value
      list[index][3] = list[index][1] * list[index][2]
    } else {
      list[index][name] = value
    }

    setInputList(list)
    let totalHtva = 0
    for (let i = 0; i < list.length; i++) {
      totalHtva += list[i][3]
    }
    totalList.totalHtva = totalHtva
    totalList.totalTvac = roundDecimal(totalHtva * (1 + totalList.tva / 100))
    totalList.tvaAmount = roundDecimal(totalHtva * (totalList.tva / 100))
    if (!!errors[index.toString() + name])
      setErrors({
        ...errors,
        [index.toString() + name]: null
      })
  }

  // handle click event of the Remove button
  const handleRemoveClick = (index) => {
    const list = [...inputList]
    list.splice(index, 1)
    setInputList(list)
  }

  // handle click event of the Add button
  const handleAddClick = () => {
    setInputList([...inputList, ['', null, null, null]])
  }

  const onChange = (e) => {
    setformData({ ...formData, [e.target.name]: e.target.checked })
    if (e.target.checked) {
      getAllUsers()
    }
  }
  const onChange2 = (e) => {
    setformData({ ...formData, [e.target.name]: e.target.value })
    if (!!errors[e.target.name])
      setErrors({
        ...errors,
        [e.target.name]: null
      })
  }

  let onChange3 = (e) => {
    setTotalList({
      ...totalList,
      tva: e.target.value,
      totalTvac: roundDecimal(totalList.totalHtva * (1 + e.target.value / 100)),
      tvaAmount: roundDecimal(totalList.totalHtva * (e.target.value / 100))
    })
  }
  function roundDecimal(number) {
    return Math.floor(number * 100) / 100
  }
  // const onSubmit = async (e) => {
  //   e.preventDefault()

  //   let form = {
  //     user: { name: formData.name, email: formData.email },
  //     data: inputList,
  //     total: totalList,
  //     extra: { commentaire: formData.commentaire, titre: formData.titre }
  //   }
  //   uploadDevisCustom(form, formData.client)
  // }

  function prepare(sended) {
    return {
      titre: formData.titre,
      user: { name: formData.name, email: formData.email },
      data: inputList,
      totalHtva: totalList.totalHtva,
      totalTvac: totalList.totalTvac,
      tvaAmount: totalList.tvaAmount,
      tva: totalList.tva,
      extra: {
        commentaire: formData.commentaire,

        dateDeb: formData.dateDeb,
        dateFin: formData.dateFin
      },
      send: sended,
      adresse: formData.adresse
    }
  }

  function restore() {
    setformData(data)
    setInputList([list])
    setTotalList(calcul)
  }

  const save = async (e) => {
    const newErrors = findFormErrors()
    // Conditional logic:
    if (Object.keys(newErrors).length > 0) {
      // We got errors!
      setErrors(newErrors)
    } else {
      uploadDevisCustom(prepare(false), formData.client).then((res) =>
        restore()
      )
    }
  }

  const send = async (e) => {
    const newErrors = findFormErrors()
    // Conditional logic:
    if (Object.keys(newErrors).length > 0) {
      // We got errors!
      setErrors(newErrors)
    } else {
      uploadDevisCustom(prepare(true), formData.client).then((res) => restore())
    }
  }
  const findFormErrors = () => {
    const { client, name, email, dateDeb, dateFin, titre } = formData
    const newErrors = {}

    if (client === false && (!name || name === '')) {
      newErrors.name = 'Indiquer un nom'
    }
    if (!email || email === '') {
      newErrors.email = 'Indiquer un email'
    }
    if (!dateDeb || dateDeb === '') {
      newErrors.dateDeb = 'Indiquer une date'
    }
    if (!dateFin || dateFin === '') {
      newErrors.dateFin = 'Indiquer une date'
    } else if (dateFin < dateDeb) {
      newErrors.dateFin = 'Date précède date de début'
    }
    if (!titre || titre === '') {
      newErrors.titre = 'Indiquer un titre'
    }

    Object.entries(inputList).map(([key, value]) => {
      if (!inputList[key][0] || inputList[key][0] === '') {
        newErrors[key.toString() + '0'] = 'Choisir une désignation!'
      }

      if (!inputList[key][3] || inputList[key][3] === '') {
        newErrors[key.toString() + '3'] = 'Prix total ou unitaire!'
      }
    })

    return newErrors
  }

  return (
    <div>
      <button onClick={() => window.history.back()} className="demdevisbutton2">
        Retour
      </button>
      <div>
        <h3>Devis Personnalisé</h3>
        <Form.Check
          type="switch"
          id="switch"
          name="client"
          value={formData.client}
          label="Client enregistré "
          onChange={onChange}
        />
        {formData.client ? (
          <div>
            <Form.Control
              style={{ width: '200px' }}
              as="select"
              name="email"
              value={formData.email}
              onChange={onChange2}
              isInvalid={!!errors.email}
            >
              {users &&
                Object.values(users).map((test) => (
                  <option value={test.email}>
                    {test.name} / {test.email}
                  </option>
                ))}
            </Form.Control>
            <Form.Control.Feedback type="invalid">
              {errors.email}
            </Form.Control.Feedback>
          </div>
        ) : (
          <div id="newclient">
            <Form.Row style={{ width: '400px' }}>
              <Col>
                Nom:{' '}
                <Form.Control
                  style={{ width: '170px' }}
                  name="name"
                  value={formData.name}
                  onChange={onChange2}
                  type="string"
                  isInvalid={!!errors.name}
                ></Form.Control>
                <Form.Control.Feedback type="invalid">
                  {errors.name}
                </Form.Control.Feedback>
              </Col>
              <Col>
                {'  '} Email:{' '}
                <Form.Control
                  style={{ width: '170px' }}
                  name="email"
                  value={formData.email}
                  onChange={onChange2}
                  type="email"
                  isInvalid={!!errors.email}
                ></Form.Control>
                <Form.Control.Feedback type="invalid">
                  {errors.email}
                </Form.Control.Feedback>
              </Col>
            </Form.Row>
          </div>
        )}
        <div>
          <Form.Row style={{ width: '400px' }}>
            <Col>
              Début souhaite:{' '}
              <Form.Control
                style={{ width: '170px' }}
                type="date"
                name="dateDeb"
                value={formData.dateDeb}
                onChange={onChange2}
                isInvalid={!!errors.dateDeb}
              ></Form.Control>
              <Form.Control.Feedback type="invalid">
                {errors.dateDeb}
              </Form.Control.Feedback>
            </Col>
            <Col>
              Fin souhaité:{' '}
              <Form.Control
                style={{ width: '170px' }}
                type="date"
                name="dateFin"
                value={formData.dateFin}
                onChange={onChange2}
                isInvalid={!!errors.dateFin}
              ></Form.Control>
              <Form.Control.Feedback type="invalid">
                {errors.dateFin}
              </Form.Control.Feedback>
            </Col>
          </Form.Row>
        </div>
        <div>
          Adresse:{' '}
          <Form.Control
            style={{ width: '350px' }}
            type="string"
            name="adresse"
            value={formData.adresse}
            onChange={onChange2}
          ></Form.Control>
        </div>
        <Form.Row>
          <Col>
            Titre :{' '}
            <Form.Control
              name="titre"
              value={formData.titre}
              onChange={onChange2}
              style={{ width: '370px' }}
              isInvalid={!!errors.titre}
            ></Form.Control>
            <Form.Control.Feedback type="invalid">
              {errors.titre}
            </Form.Control.Feedback>
          </Col>
        </Form.Row>
        <div>Tva de :</div>
        <div>
          <Form.Control
            as="select"
            name="tva"
            value={totalList.tva}
            onChange={onChange3}
            style={{ marginBottom: '20px', width: '170px' }}
          >
            <option value={21}>21%</option>
            <option value={6}>6%</option>
          </Form.Control>
        </div>
      </div>
      <div className="tabledevis">
        <Table bordered id="deviss">
          <thead className="testing">
            <tr>
              <th>Designation</th>
              <th>Prix unitaire</th>
              <th>Quantité </th>
              <th>Total</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td colSpan="4" style={{ textTransform: 'capitalize' }}>
                {formData.titre}
              </td>
            </tr>
            {inputList.map((x, i) => {
              return (
                <tr key={x}>
                  <td>{x[0]}</td>
                  <td>{x[1]}</td>
                  <td>{x[2]}</td>
                  <td>{x[3]}</td>
                </tr>
              )
            })}

            <tr>
              <td colSpan="3">Total Htva</td>
              <td>{totalList.totalHtva} </td>
            </tr>
            <tr>
              <td colSpan="3">Tva {totalList.tva} % </td>
              <td>{totalList.tvaAmount} </td>
            </tr>
            <tr>
              <td colSpan="3">Total Tvac</td>
              <td>{totalList.totalTvac}</td>
            </tr>
          </tbody>
        </Table>
      </div>

      {inputList.map((x, i) => {
        return (
          <div style={{ marginBottom: '5px' }}>
            <Form.Row style={{ width: '704px' }}>
              <Col lg={4}>
                <Form.Control
                  className="createdevisinputdes"
                  name="0"
                  value={x[0]}
                  placeholder="Désignation"
                  onChange={(e) => handleInputChange(e, i)}
                  isInvalid={!!errors[i.toString() + '0']}
                />
                <Form.Control.Feedback type="invalid">
                  {errors[i.toString() + '0']}
                </Form.Control.Feedback>
              </Col>
              <Col lg={2}>
                <Form.Control
                  className="createdevisinput"
                  type="number"
                  min="0"
                  name="1"
                  value={x[1]}
                  placeholder="Prix Unit"
                  onChange={(e) => handleInputChange(e, i)}
                  isInvalid={!!errors[i.toString() + '1']}
                />
                <Form.Control.Feedback type="invalid">
                  {errors[i.toString() + '1']}
                </Form.Control.Feedback>
              </Col>
              <Col lg={2}>
                <Form.Control
                  className="createdevisinput"
                  type="number"
                  min="0"
                  name="2"
                  value={x[2]}
                  placeholder="Quant"
                  onChange={(e) => handleInputChange(e, i)}
                  isInvalid={!!errors[i.toString() + '2']}
                />
                <Form.Control.Feedback type="invalid">
                  {errors[i.toString() + '2']}
                </Form.Control.Feedback>
              </Col>
              <Col lg={2}>
                <Form.Control
                  className="createdevisinput"
                  type="number"
                  min="0"
                  name="3"
                  value={x[3]}
                  placeholder="Total"
                  onChange={(e) => handleInputChange(e, i)}
                  isInvalid={!!errors[i.toString() + '3']}
                />
                <Form.Control.Feedback type="invalid">
                  {errors[i.toString() + '3']}
                </Form.Control.Feedback>
              </Col>
              {inputList.length !== 1 && i > 0 && (
                <Col lg={1}>
                  <button className="mr10" onClick={() => handleRemoveClick(i)}>
                    Remove
                  </button>
                </Col>
              )}
            </Form.Row>
          </div>
        )
      })}
      <button onClick={() => handleAddClick()} className="demdevisbutton2">
        Nouvelle Ligne
      </button>
      <Form.Row>
        <Col>
          <div>Commentaires :</div>
          {/* <Form.Control
            style={{ width: '370px', height: '100px', marginBottom: '20px' }}
            name="commentaire"
            value={formData.commentaire}
            onChange={onChange2}
          ></Form.Control> */}
          <textarea
            id="commentaire"
            name="commentaire"
            style={{ height: '88px' }}
            value={formData.commentaire}
            rows="5"
            onChange={onChange2}
            placeholder="Indiquer toutes informations que vous jugez utiles"
          />
        </Col>
      </Form.Row>
      {/* <button onClick={() => capture()}>Télécharger en pdf</button> */}
      <div>
        <Button id="validbutton" onClick={save}>
          Enregistrer
        </Button>
        <Button id="validbutton" onClick={send}>
          Envoyer
        </Button>
      </div>
    </div>
  )
}

CreationDevis.propTypes = {
  getAllUsers: PropTypes.func.isRequired,
  uploadDevisCustom: PropTypes.func.isRequired,
  demande: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  demande: state.demande,
  user: state.user
})

export default connect(mapStateToProps, { getAllUsers, uploadDevisCustom })(
  CreationDevis
)
