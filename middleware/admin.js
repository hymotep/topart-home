const jwt = require('jsonwebtoken')
const config = require('config')
const User = require('../models/User.js')

const admin = async (req, res, next) => {
  //reprendre token via header
  const token = req.header('x-auth-token')
  //check si il y a un token
  if (!token) {
    return res.status(401).json({ msg: 'pas de token' })
  }
  //verification du token
  try {
    const decoded = jwt.verify(token, config.get('jwtSecret'))
    req.user = decoded.user

    //récupere l'user connecté  et vérifier si il est admin
    let user = await User.findById(decoded.user.id).select('-password')
    if (req.user && user.admin === true) {
      return next()
    }
  } catch (err) {
    res.status(401).json({ msg: 'Token pas valide' })
  }
}
module.exports = admin
