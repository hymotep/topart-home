import React, { Fragment, useState } from 'react'
// import axios from 'axios';
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { login } from '../../store/actions/auth.js'

const Login = ({ login, isAuthenticated, auth: { user, loading } }) => {
  const [formData, setFormData] = useState({
    email: '',
    password: ''
  })

  const { email, password } = formData

  const onChange = (e) =>
    setFormData({ ...formData, [e.target.name]: e.target.value })

  const onSubmit = async (e) => {
    e.preventDefault()
    login(email, password)
  }
  if (!loading) {
    if (user && user.admin === true) {
      return <Redirect to="/alldevis" />
    }
    if (isAuthenticated && user && !user.admin) {
      return <Redirect to="/devisfacture" />
    }
  }

  return (
    <Fragment>
      <h1 className="large text-primary">Se connecter</h1>
      <p className="lead">
        <i className="fas fa-user"></i> Connectez vous à votre compte
      </p>
      <form className="form" onSubmit={(e) => onSubmit(e)}>
        <div className="form-group">
          <input
            type="email"
            placeholder="Adresse e-mail"
            name="email"
            value={email}
            onChange={(e) => onChange(e)}
            required
            autoFocus
          />
        </div>
        <div className="form-group">
          <input
            type="password"
            placeholder="Mot de passe"
            name="password"
            minLength="6"
            value={password}
            onChange={(e) => onChange(e)}
            required
          />
        </div>

        <input type="submit" className="btn btn-primary" value="se connecter" />
      </form>
      {/* <p className="my-1">
        Vous n'avez pas de compte? <Link to="register">Connexion</Link>
      </p> */}
    </Fragment>
  )
}

Login.propTypes = {
  login: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool,
  auth: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
  auth: state.auth
})

export default connect(mapStateToProps, { login })(Login)
