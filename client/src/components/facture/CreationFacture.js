import React, { useEffect, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { Table, Form } from 'react-bootstrap'

import { getChantierById } from '../../store/actions/chantier.js'
import {
  getFactureByChantier,
  saveFacture
} from '../../store/actions/facture.js'
import Spinner from '../layout/Spinner'

import { Redirect } from 'react-router-dom'

const CreationFacture = ({
  chantier: { chantier, loading },
  getChantierById,
  match,
  saveFacture,
  getFactureByChantier,
  facture
}) => {
  let dat = new Date()

  let dateToday =
    (dat.getDate() < 10 ? '0' + dat.getDate() : dat.getDate()) +
    '/' +
    (new Date().getMonth() < 10
      ? '0' + new Date().getMonth()
      : new Date().getMonth()) +
    '/' +
    dat.getFullYear().toString()
  let dat2 = new Date(new Date().getTime() + 21 * 24 * 60 * 60 * 1000)
  let dateEcheance =
    (dat2.getDate() < 10 ? '0' + dat2.getDate() : dat2.getDate()) +
    '/' +
    (dat2.getMonth() < 10 ? '0' + dat2.getMonth() : dat2.getMonth()) +
    '/' +
    dat2.getFullYear().toString()

  let total = {
    totalHtva: 0,
    totalTvac: 0,
    tvaAmount: 0,
    tva: 21
  }
  let data = {
    adresse: ''
  }
  const [inputList, setInputList] = React.useState({})
  const [totalList, setTotalList] = React.useState(total)
  const [formData, setFormData] = React.useState(data)
  const [errors, setErrors] = React.useState({})

  useEffect(() => {
    getChantierById(match.params.id)
  }, [getChantierById, match.params.id])
  // useEffect(() => {
  //   getFactureByChantier(match.params.id)
  // }, [])

  const onChange2 = (e) => {
    chantier.user.adresse = e.target.value
    setFormData({ ...formData, [e.target.name]: e.target.value })
    if (!!errors['adresse'])
      setErrors({
        ...errors,
        adresse: null
      })
  }
  function setAdresse(lieu) {
    switch (lieu) {
      case 'client': {
        setFormData({ ...formData, ['adresse']: chantier.user.adresse })
      }
      case 'chantier': {
        setFormData({ ...formData, ['adresse']: chantier.adresse })
      }
      default: {
      }
    }
    if (!!errors['adresse'])
      setErrors({
        ...errors,
        adresse: null
      })
  }

  function roundDecimal(number) {
    return Math.floor(number * 100) / 100
  }

  function add(key) {
    totalList.tva = chantier.total.tva
    let temp = chantier.total.tva / 100
    chantier.total.totalHtva = roundDecimal(
      chantier.total.totalHtva - chantier.line[key][3]
    )
    chantier.total.tvaAmount = roundDecimal(chantier.total.totalHtva * temp)
    chantier.total.totalTvac = roundDecimal(
      chantier.total.totalHtva * (1 + temp)
    )
    // total.total += chantier.line[key][3]
    setTotalList({
      ...totalList,
      totalHtva: roundDecimal(totalList.totalHtva + chantier.line[key][3]),
      tvaAmount: roundDecimal(
        (totalList.totalHtva + chantier.line[key][3]) * temp
      ),
      totalTvac: roundDecimal(
        (totalList.totalHtva + chantier.line[key][3]) * (1 + temp)
      )
    })
    setInputList({ ...inputList, [key]: chantier.line[key] })
    // chantier.line.splice(key, 1)
    delete chantier.line[key]

    // chantier.total.total -= chantier.line[key][3]
  }
  function del(key) {
    let temp = chantier.total.tva / 100
    chantier.total.totalHtva = chantier.total.totalHtva + inputList[key][3]
    chantier.total.tvaAmount = roundDecimal(
      chantier.total.totalHtva + inputList[key][3] * temp
    )
    chantier.total.totalTvac = roundDecimal(
      chantier.total.totalHtva + inputList[key][3] * (1 + temp)
    )
    chantier.line[key] = inputList[key]
    setTotalList({
      ...totalList,
      totalHtva: roundDecimal(totalList.totalHtva - inputList[key][3]),
      tvaAmount: roundDecimal((totalList.totalHtva - inputList[key][3]) * temp),
      totalTvac: roundDecimal(
        (totalList.totalHtva - inputList[key][3]) * (1 + temp)
      )
    })
    setInputList({ ...inputList, [key]: [] })
    // inputList.splice(key, 1)
  }

  const findFormErrors = () => {
    const { adresse } = formData
    const newErrors = {}
    // name errors
    if (!adresse || adresse === '')
      newErrors.adresse = 'introduire une adresse !'

    return newErrors
  }

  function prepare() {
    return {
      titre: chantier.titre,
      user: chantier.user._id,
      email: chantier.user.email,
      name: chantier.user.name,
      chantier: chantier._id,
      line: inputList,
      oldline: chantier.line.filter((el) => {
        return el !== null && el !== ''
      }),
      totalHtva: totalList.totalHtva,
      totalTvac: totalList.totalTvac,
      tvaAmount: totalList.tvaAmount,
      tva: totalList.tva,
      oldtotal: chantier.total,
      dateLivraison: chantier.dateDeb,
      date: dateToday,
      dateEcheance: dateEcheance,
      adresse: formData.adresse
    }
  }

  function restore(res) {
    setInputList({})
    setTotalList(total)
    setErrors({})
  }

  function send() {
    const newErrors = findFormErrors()
    // Conditional logic:
    if (Object.keys(newErrors).length > 0) {
      // We got errors!
      setErrors(newErrors)
    } else {
      saveFacture(prepare()).then((res) => restore(res))
    }
  }
  // if (!loading) {
  //   if (chantier.stillToBill === 0) {
  //     return <Redirect to="/chantiers" />
  //   }
  // }

  return (
    <Fragment>
      {loading ? (
        <Spinner />
      ) : (
        chantier && (
          <div>
            <button
              onClick={() => window.history.back()}
              className="demdevisbutton2"
            >
              Retour
            </button>

            <div>Reste à payer:{chantier.stillToPay} </div>
            <div>Reste à facturer:{chantier.stillToBill} </div>
            <Table bordered id="deviss">
              <thead className="testing">
                <tr>
                  <th>Designation</th>
                  <th>Prix unitaire</th>
                  <th>Quantité </th>
                  <th>Total</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td colSpan="4">{chantier.titre}</td>
                </tr>

                {chantier.line &&
                  chantier.line !== null &&
                  Object.entries(chantier.line).map(([key, value]) => (
                    <tr>
                      {chantier.line[key].map((x) => (
                        <td>{x} </td>
                      ))}
                      <td>
                        <button
                          onClick={() => {
                            add(key)
                          }}
                          style={{ backgroundColor: '#66cc00' }}
                        >
                          <i class="fas fa-arrow-down"></i>
                        </button>
                      </td>
                    </tr>
                  ))}

                <tr>
                  <td colSpan="3">Total Htva</td>
                  <td>{chantier.total.totalHtva} </td>
                </tr>
                <tr>
                  <td colSpan="3">Taux de {chantier.total.tva} %</td>
                  <td>{chantier.total.tvaAmount}</td>
                </tr>

                <tr>
                  <td colSpan="3">Total Tvac</td>
                  <td>{chantier.total.totalTvac}</td>
                </tr>
              </tbody>
            </Table>
            <div>Nom Client :{chantier.user.name}</div>
            <div>
              Adresse :
              <button
                onClick={() => {
                  setAdresse('client')
                }}
              >
                adresse client
              </button>
              <button
                onClick={() => {
                  setAdresse('chantier')
                }}
              >
                adresse chantier
              </button>
              <Form.Control
                value={formData.adresse}
                name="adresse"
                onChange={onChange2}
                isInvalid={!!errors.adresse}
              />
              <Form.Control.Feedback type="invalid">
                {errors.adresse}
              </Form.Control.Feedback>
            </div>
            <div></div>
            <div>Date de Livraison:{chantier.dateDeb} </div>
            <div>Date de facturation:{dateToday} </div>
            <div>Echéance :{dateEcheance} </div>

            <Table bordered id="deviss">
              <thead className="testing">
                <tr>
                  <th>Designation</th>
                  <th>Prix unitaire</th>
                  <th>Quantité </th>
                  <th>Total</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td colSpan="4">{chantier.titre}</td>
                </tr>

                {Object.entries(inputList).map(
                  ([key, value]) =>
                    inputList[key].length !== 0 && (
                      <tr>
                        {inputList[key].map((x) => (
                          <td>{x} </td>
                        ))}
                        <td>
                          <button
                            onClick={() => {
                              del(key)
                            }}
                            style={{ backgroundColor: 'red' }}
                          >
                            <i class="fas fa-arrow-up"></i>
                          </button>
                        </td>
                      </tr>
                    )
                )}

                <tr>
                  <td colSpan="3">Total Htva</td>
                  <td>{totalList.totalHtva} </td>
                </tr>
                <tr>
                  <td colSpan="3">Tva de 21%</td>
                  <td>{totalList.tvaAmount}</td>
                </tr>
                <tr>
                  <td colSpan="3">Total Tvac</td>
                  <td>{totalList.totalTvac}</td>
                </tr>
              </tbody>
            </Table>
            {totalList.totalHtva > 0 && (
              <button onClick={send} className="demdevisbutton2">
                send
              </button>
            )}
          </div>
        )
      )}
    </Fragment>
  )
}

CreationFacture.propTypes = {
  getChantierById: PropTypes.func.isRequired,
  getFactureByChantier: PropTypes.func.isRequired,
  saveFacture: PropTypes.func.isRequired,
  chantier: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  chantier: state.chantier,
  facture: state.facture
})

export default connect(mapStateToProps, {
  saveFacture,
  getChantierById,
  getFactureByChantier
})(CreationFacture)
