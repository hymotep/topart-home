const mongoose = require('mongoose')

const DemandeDevisSchema = new mongoose.Schema({
  numero: { type: Number, required: true },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user',
    required: true
  },
  data: { type: Object, required: true },

  date: {
    type: Date,
    default: Date.now
  },
  devis: { type: Boolean, default: false }
})

module.exports = DemandeDevis = mongoose.model(
  'demandeDevis',
  DemandeDevisSchema
)
