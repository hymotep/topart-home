const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
  // _id: { type: String, unique: true },
  email: { type: String, required: true, unique: true },
  name: { type: String, required: true },
  password: { type: String, required: true },
  birthdate: { type: Date },

  codePostal: {
    type: Number
  },
  adresse: {
    type: String
  },
  tva: {
    type: String
  },
  new: { type: Boolean, default: true },
  phone: {
    number: { type: String, default: '' }
  },
  creationDate: { type: Date, default: Date.now },

  admin: { type: Boolean, default: false },
  tvaNumber: { type: String }
})

module.exports = User = mongoose.model('user', UserSchema)
