import {
  GET_ALLCHANTIER,
  GET_CHANTIER,
  UPDATE_CHANTIER,
  CHANTIER_ERROR
} from './types'
import api from '../../outils/api'
import { setAlert } from './alert'
//Get all chantiers
export const getAllChantier = () => async (dispatch) => {
  try {
    const res = await api.get('/chantier')
    dispatch({
      type: GET_ALLCHANTIER,
      payload: res.data
    })
  } catch (err) {
    dispatch({
      type: CHANTIER_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    })
  }
}
//Get all chantier  by user
export const getAllUserChantier = (userId) => async (dispatch) => {
  try {
    const res = await api.get(`/chantier/user/${userId}`)
    dispatch({
      type: GET_ALLCHANTIER,
      payload: res.data
    })
  } catch (err) {
    dispatch({
      type: CHANTIER_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    })
  }
}

// Get chantier by ID
export const getChantierById = (id) => async (dispatch) => {
  try {
    await api.get(`/chantier/${id}`).then((res) =>
      dispatch({
        type: GET_CHANTIER,
        payload: res.data
      })
    )
  } catch (err) {
    dispatch({
      type: CHANTIER_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    })
  }
}

export const updateChantier = (data) => async (dispatch) => {
  const body = JSON.stringify(data)

  try {
    const res = await api.put('/chantier/', body)

    dispatch({
      type: UPDATE_CHANTIER,
      payload: res.data
    })
    dispatch(setAlert('Chantier mis à jour', 'success'))
  } catch (err) {
    dispatch({
      type: CHANTIER_ERROR,
      payload: { msg: err.response }
    })
  }
}

// Delete chantier

// export const deleteChantier = (demandeId) => async (dispatch) => {
//   try {
//     await api.delete(`/chantier/${demandeId}`)

//     dispatch({
//       type: DELETE_CHANTIER,
//       payload: demandeId
//     })

//     dispatch(setAlert('Devis Supprimé', 'success'))
//   } catch (err) {
//     // dispatch({
//     //   type: POST_ERROR,
//     //   payload: { msg: err.response.statusText, status: err.response.status }
//     // })
//   }
// }
