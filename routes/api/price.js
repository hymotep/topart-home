const express = require('express')
const router = express.Router()

const multer = require('multer')
const Devis = require('../../models/Devis.js')
const path = require('path')

const User = require('../../models/User.js')
const Price = require('../../models/Price.js')
const DemandeDevis = require('../../models/DemandeDevis.js')
const Counters = require('../../models/Counters.js')
const auth = require('../../middleware/auth.js')

const admin = require('../../middleware/admin.js')
const checkObjectId = require('../../middleware/checkObjectId')

//ajouter nouveau type de liste de prix
router.post('/', admin, async (req, res) => {
  const {
    name,
    type,
    longueur,
    largeur,
    prixHtva,
    prixTvac,
    poteauHtva,
    poteauTvac,
    hauteur,
    nombre
  } = req.body

  try {
    const doc = await Price.findOneAndUpdate(
      { type: type, name: name, nombre: nombre },
      {
        $set: {
          longueur,
          largeur,
          prixHtva,
          prixTvac,
          nombre,
          poteauHtva,
          poteauTvac
        }
      },
      { upsert: true, new: true }
    )
    res.json(doc)
  } catch (err) {
    console.error(err.message)
    res.status(500).send('Server error')
  }
})

//Get all prices

router.get('/', admin, async (req, res) => {
  try {
    const prices = await Price.find()
    res.json(prices)
  } catch (err) {
    console.error(err.message)
    res.status(500).send('Server Error')
  }
})

// Get  price by type
router.post('/few/', admin, async (req, res) => {
  try {
    const price = await Price.find({
      type: { $in: [req.body.un, req.body.deux] }
    })
    res.json(price)
  } catch (err) {
    console.error(err.message)
    res.status(500).send('Server Error')
  }
})

//delete one demande
router.delete('/:id', [admin, checkObjectId('id')], async (req, res) => {
  try {
    const price = await Price.findById(req.params.id)

    await price.remove()

    res.json({ msg: 'Post removed' })
  } catch (err) {
    console.error(err.message)

    res.status(500).send('Server Error')
  }
})

module.exports = router
