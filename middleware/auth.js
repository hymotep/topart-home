const jwt = require('jsonwebtoken')
const config = require('config')

module.exports = function (req, res, next) {
  //reprendre token via header
  const token = req.header('x-auth-token')

  //check si il y a un token
  if (!token) {
    return res.status(401).json({ msg: 'pas de token' })
  }
  //verification du token
  try {
    const decoded = jwt.verify(token, config.get('jwtSecret'))
    req.user = decoded.user
    next()
  } catch (err) {
    res.status(401).json({ msg: 'Token pas valide' })
  }
}
