import axios from 'axios'

const apifile = axios.create({
  baseURL: '/apifile',
  headers: {
    'Content-Type': 'multipart/form-data'
  }
})
export default apifile
