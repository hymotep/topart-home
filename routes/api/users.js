const express = require('express')
const router = express.Router()
const { check, validationResult } = require('express-validator')
// import User from '../../models/User.js';
const User = require('../../models/User.js')

const admin = require('../../middleware/admin.js')

const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const config = require('config')

//register new user

router.post(
  '/',
  [
    check('name', 'Name is required').not().isEmpty(),
    check('email', 'place include a valid email').isEmail(),
    check('password', 'min 6 characters').isLength({ min: 6 })
  ],
  async (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() })
    }

    const { name, email, password, codePostal } = req.body

    try {
      //User exists?
      let user = await User.findOne({ email })
      if (user == null) {
        //création user
        user = new User({
          // _id: email,
          name,
          email,
          password,
          codePostal
        })

        //hash password
        const salt = await bcrypt.genSalt(10)
        user.password = await bcrypt.hash(password, salt)

        //enregistrement user
        await user.save()

        //création token en vue d'une connection directe
        const payload = {
          user: {
            id: user.id
          }
        }
        jwt.sign(
          payload,
          config.get('jwtSecret'),
          { expiresIn: '5 days' },
          (err, token) => {
            if (err) throw err
            res.json({ token })
          }
        )
      } else {
        res.status(400).json({ errors: [{ msg: 'email déja utilisé ' }] })
      }
    } catch (err) {
      console.error(err.message)
      res.status(500).send('Server error')
    }
  }
)

//get all User

router.get('/', admin, async (req, res) => {
  try {
    let users = await User.find({ admin: false })

    res.json(users)
  } catch (err) {
    console.error(err.message)
    res.status(500).send('Server Error')
  }
})
router.get('/exist/:email', async (req, res) => {
  try {
    let user = await User.exists({ email: req.params.email })
    res.json(user)
  } catch (err) {
    console.error(err.message)
    res.status(500).send('Server Error')
  }
})

// Delete all user except admin

router.delete('/', admin, async (req, res) => {
  try {
    // Remove user posts
    await User.deleteMany({ admin: false })

    res.json({ msg: 'User deleted' })
  } catch (err) {
    console.error(err.message)
    res.status(500).send('Server Error')
  }
})

module.exports = router
