const Event = require('../../models/Event.js')
const express = require('express')
const router = express.Router()
const DemandeDevis = require('../../models/DemandeDevis.js')
const admin = require('../../middleware/admin.js')
const checkObjectId = require('../../middleware/checkObjectId')
const { body } = require('express-validator')
const { check, validationResult } = require('express-validator')

//créer event
// router.post('/', admin, async (req, res) => {
//   try {
//     const data = req.body

//     let event = new Event({
//       numero: numeroDemandeDevis.sequence_value,
//       user: temp._id,
//       data: req.body
//     })

//     await event.save()

//     res.json(event)
//   } catch (err) {
//     console.error(err.message)
//     res.status(500).send('Server error')
//   }
// })

// get all event
router.get('/', admin, async (req, res) => {
  try {
    const events = await Event.find()
      .populate('id', ['email', 'name'])
      .select('-password')
    res.json(events)
  } catch (err) {
    console.error(err.message)
    res.status(500).send('Server Error')
  }
})

// //get events by user
// router.get('/user/:id', [auth, checkObjectId('id')], async (req, res) => {
//   try {
//     const event = await Event.find({ user: req.params.id }).sort({
//       numero: -1
//     })
//     res.json(event)
//   } catch (err) {
//     console.error(err.message)
//     res.status(500).send('Server Error')
//   }
// })

//get one event by id

router.get('/:id', [admin, checkObjectId('id')], async (req, res) => {
  try {
    const event = await Event.findById(req.params.id)
      .populate('user', ['email', 'name'])
      .select('-password')

    res.json(event)
  } catch (err) {
    console.error(err.message)

    res.status(500).send('Server Error')
  }
})

//delete one demande
router.delete('/:id', [admin, checkObjectId('id')], async (req, res) => {
  try {
    const event = await DemandeDevis.findById(req.params.id)

    await event.remove()

    res.json({ msg: 'Post removed' })
  } catch (err) {
    console.error(err.message)

    res.status(500).send('Server Error')
  }
})

//delete all demande
router.delete('/', admin, async (req, res) => {
  try {
    await DemandeDevis.deleteMany()

    res.json({ msg: 'All demande deleted' })
  } catch (err) {
    console.error(err.message)
    res.status(500).send('Server Error')
  }
})
//update event et chantier
router.put(
  '/',
  [
    admin,
    [
      check('id', 'id is required').not().isEmpty(),
      check('start', 'startDate is required').not().isEmpty(),
      check('end', 'endDate is required').not().isEmpty(),
      check('chantier', 'chantier is required').not().isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() })
    }
    try {
      await Event.findOneAndUpdate(
        { _id: req.body.id },
        {
          start: req.body.start,
          end: req.body.end
        }
      )

      let temp = req.body.end.split('-')
      temp[2] = Number(temp[2]) - 1
      dateFin = temp[0] + '-' + temp[1] + '-' + temp[2]

      const chant = await Chantier.findOneAndUpdate(
        { _id: req.body.chantier },
        {
          dateDeb: req.body.start,
          dateFin: dateFin
        }
      )
      return chant
    } catch (err) {
      console.error(err.message)
      res.status(500).send('Server error')
    }
  }
)

module.exports = router
