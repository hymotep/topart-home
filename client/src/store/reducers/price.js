import {
  GET_PRICE,
  GET_PRICES,
  EDIT_PRICE,
  ADD_PRICE,
  DELETE_PRICE,
  PRICE_ERROR
} from '../actions/types'

const initialState = {
  prices: null,
  price: null,
  loading: true,
  error: {}
}

export default function (state = initialState, action) {
  const { type, payload } = action

  switch (type) {
    case GET_PRICE:
      return {
        ...state,
        price: payload,
        loading: false
      }
    case ADD_PRICE:
      return {
        ...state,
        prices: [...state.prices, payload],
        loading: false
      }
    case EDIT_PRICE: {
      const index = state.prices.findIndex((price) => price._id !== payload._id)
      const newArray = state.prices
      newArray[index - 1] = payload
      return {
        ...state,
        prices: newArray,
        loading: false
      }
    }
    case DELETE_PRICE:
      return {
        ...state,
        prices: state.prices.filter((price) => price._id !== payload),
        loading: false
      }
    case GET_PRICES:
      return {
        ...state,
        prices: payload,
        loading: false
      }
    case PRICE_ERROR:
      return {
        ...state,
        error: payload,
        loading: false
      }
    default:
      return state
  }
}
