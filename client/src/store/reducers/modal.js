import { ActionTypes } from '../actions/types'

const initialState = {
  demandeDevisOpen: false,
  userDataOpen: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.SHOW_MODAL:
      return {
        ...state,
        demandeDevisOpen: true
      }
    case ActionTypes.HIDE_MODAL:
      return {
        ...state,
        demandeDevisOpen: false
      }
    case ActionTypes.SHOW_MODAL_USER:
      return {
        ...state,
        userDataOpen: true
      }
    case ActionTypes.HIDE_MODAL_USER:
      return {
        ...state,
        userDataOpen: false
      }
    default:
      return state
  }
}
