const express = require('express')
const router = express.Router()

const { check, validationResult } = require('express-validator')
const nodemailer = require('nodemailer')
const GOOGLEKEY = require('../../config/keys.js')
// send email
router.post(
  '/',
  [
    check('email', 'Introduire  un email valide').isEmail(),
    check('name', 'Introduire un nom').exists(),
    check('comment', 'Introduire une demande').exists()
  ],
  async (req, res) => {
    try {
      const data = req.body
      // creation d'un transporter avec les data de l'admin
      let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'julien.topart@gmail.com',
          pass: 'Nalajul1hana..'
        },
        tls: {
          rejectUnauthorized: false
        }
      })

      let htmltext = //concaténation des differents elements nécéssaire
        '<h2>' + //pour le contenu du mail sous forme d' html
        data.title +
        '</h2>' +
        '<div>' +
        data.comment +
        '</div>' +
        '<img src="cid:logo" width="300"/>'

      let info = await transporter.sendMail({
        from: 'julien.topart@gmail.com', // sender address
        to: data.send ? data.email : 'info@toparthome.com', // si c'est un envoie l'envoie au client
        //sinon envoyé à l'admin
        subject: data.title, // L'object de l'email
        html: htmltext,
        attachments: [
          //Attachement du logo de l'entreprise dans le mail.
          {
            filename: 'logo3.png',
            path: __dirname + '/logo3.png',
            cid: 'logo'
          }
        ]
      })
    } catch (err) {
      console.error(err.message)
      res.status(500).send('Server Error')
    }
  }
)
module.exports = router
