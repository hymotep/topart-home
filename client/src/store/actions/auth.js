import { setAlert } from './alert'
import {
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  USER_LOADED,
  AUTH_ERROR,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT,
  CLEAR_PROFILE,
  STOP_REDIRECT,
  CLEAR_DEVIS,
  CLEAR_DEMANDE,
  CLEAR_FACTURE,
  CLEAR_CHANTIER,
  CLEAR_EVENT
} from './types'
import api from '../../outils/api'
import setAuthToken from '../../outils/setAuthToken'

export const stopRedirect = () => (dispatch) => {
  dispatch({
    type: STOP_REDIRECT
  })
}
//chargement user
export const loadUser = () => async (dispatch) => {
  if (localStorage.token) {
    setAuthToken(localStorage.token)
  }
  try {
    const res = await api.get('/auth')

    dispatch({
      type: USER_LOADED,
      payload: res.data
    })
  } catch (err) {
    dispatch({
      type: AUTH_ERROR
    })
  }
}
//enregistrement user
export const register =
  ({ name, email, password, admin, connected }) =>
  async (dispatch) => {
    const body = JSON.stringify({ name, email, password })

    try {
      const res = await api.post('/users', body)
      if (!admin || !connected) {
        dispatch({
          type: REGISTER_SUCCESS,
          payload: res.data
        })
      }

      // dispatch(loadUser());
    } catch (err) {
      const errors = err.response.data.errors

      if (errors) {
        errors.forEach((error) => dispatch(setAlert(error.msg, 'danger')))
      }

      dispatch({
        type: REGISTER_FAIL
      })
    }
  }

//login
export const login = (email, password) => async (dispatch) => {
  const body = JSON.stringify({ email, password })
  try {
    const res = await api.post('/auth', body)
    dispatch({
      type: LOGIN_SUCCESS,
      payload: res.data
    })
    dispatch(loadUser())
  } catch (err) {
    const errors = err.response.data.errors

    if (errors) {
      errors.forEach((error) => dispatch(setAlert(error.msg, 'danger')))
    }

    dispatch({
      type: LOGIN_FAIL
    })
  }
}

//logout
export const logout = () => (dispatch) => {
  dispatch({ type: CLEAR_PROFILE })
  dispatch({ type: CLEAR_DEVIS })
  dispatch({ type: CLEAR_DEMANDE })
  dispatch({ type: CLEAR_FACTURE })
  dispatch({ type: CLEAR_CHANTIER })
  dispatch({ type: CLEAR_EVENT })
  dispatch({ type: LOGOUT })
}
