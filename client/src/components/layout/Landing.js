import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import './callbuton.css'
import ReactFullpage from '@fullpage/react-fullpage'
import logo from '../img/last2.png'
import { Link } from 'react-router-dom'
import { Carousel, Row, Col } from 'react-bootstrap'
import terrasse2 from '../img/terasse2.jpg'
import terrasse3 from '../img/terasse3.jpg'
import terrasse from '../img/terasse.jpg'
import show from '../img/susp.jpg'
import Iframe from 'react-iframe'
import React, { useEffect, Fragment } from 'react'
import 'animate.css'
const Landing = ({ auth: { user, isAuthenticated, loading, redirect } }) => {
  useEffect(() => {
    test1()
  }, [])

  if (isAuthenticated && redirect) {
    return <Redirect to="/devisfacture" />
  }
  function test1() {
    setTimeout(function () {
      var button = document.getElementById('arrowdown')
      button.click()
    }, 4000)
  }
  // if(window.innerWidth > 900 && )

  // document.getElementById('navvs').style.display = 'none'
  return (
    <div>
      <ReactFullpage
        //fullpage options
        licenseKey={'YOUR_KEY_HERE'}
        scrollingSpeed={1400} /* Options here */
        render={({ state, fullpageApi }) => {
          return (
            <div>
              <ReactFullpage.Wrapper>
                <div
                  className="section "
                  // style={{ marginTop: '200px' }}
                  // style={{ backgroundColor: 'black' }}
                >
                  <p className="text1">
                    <h2 className="animate__animated animate__backInRight">
                      <i id="company">
                        <img id="imglogo" src={logo} alt="..." />
                      </i>
                    </h2>
                  </p>
                  <p className="text2">
                    <h2
                      id="reatitle"
                      className="animate__animated animate__flipInX animate__delay-1s	1s"
                    >
                      {' '}
                      Réalisations de{' '}
                    </h2>
                    <h1
                      id="reatitle2"
                      className="animate__animated animate__flipInX  animate__delay-1s	1s "
                    >
                      terrasses en bois
                    </h1>
                  </p>
                  {/* <p className="text1">Terrasses en bois et amenagement</p> */}
                  <div id="section1"></div>
                  <button
                    id="tosection3"
                    onClick={() => fullpageApi.moveTo(3)}
                    className="fas fa-chevron-down fa-3x"
                    aria-hidden="true"
                    hidden
                  ></button>
                  <i
                    id="arrowdown"
                    onClick={() => fullpageApi.moveSectionDown()}
                    className="fas fa-chevron-down fa-3x animate__animated animate__heartBeat animate__infinite"
                    aria-hidden="true"
                  ></i>
                </div>

                <div className="section">
                  <div id="section21">
                    {/* <Row className="justify-content-md-center">
                      <Col> */}
                    <p className="text31">
                      TOPART HOME , vos envies , sur mesure
                    </p>
                    <p className="text3">
                      Topart Home est une société d'aménagement extérieur
                      spécialisée dans la conception et l'installation de
                      terrasses en bois et projets sur mesure.
                    </p>
                    {/* </Col>
                      <Col></Col>
                    </Row> */}
                  </div>
                  <div id="section22" className="container-fluid">
                    <Row className="justify-content-md-center">
                      <Col md>
                        <img
                          id="imgtest"
                          className="d-block w-100"
                          src={show}
                          alt="Second slide"
                          // style={{height :  }}
                        />
                      </Col>
                      <Col>
                        <Row id="section23">
                          <h1>WELCOME</h1>
                        </Row>
                        <Row id="section24">
                          Nous sommes disponibles pour tout autres projets :
                          pose panneaux parevue, bancs, clotures, gardes-corps,
                          éclairages… Nous intervenons à Bruxelles et Brabants .
                          En dehors de ces zones, nous étudions également tout
                          projet de taille conséquente.
                        </Row>
                      </Col>
                    </Row>
                  </div>
                  {/* <div id="section23"></div> */}

                  <i
                    id="arrowup"
                    // style={{ marginTop: '120px' }}
                    onClick={() => fullpageApi.moveSectionUp()}
                    // onClick={() => fullpageApi.moveTo(1)}
                    className="fas fa-chevron-up fa-3x arrowup1 animate__animated animate__heartBeat animate__infinite"
                    aria-hidden="true"
                  ></i>
                  <i
                    id="arrowdown"
                    onClick={() => fullpageApi.moveSectionDown()}
                    className="fas fa-chevron-down fa-3x animate__animated animate__heartBeat animate__infinite"
                    aria-hidden="true"
                  ></i>
                </div>
                <div className="section">
                  <i
                    id="arrowup"
                    // style={{ marginTop: '120px' }}
                    onClick={() => fullpageApi.moveSectionUp()}
                    // onClick={() => fullpageApi.moveTo(1)}
                    className="fas fa-chevron-up fa-3x arrowup1 animate__animated animate__heartBeat animate__infinite"
                    aria-hidden="true"
                  ></i>
                  <p className="text2">
                    {/* <Iframe
                      url="http://www.youtube.com/embed/xDMP3i36naA"
                      width="450px"
                      height="450px"
                      id="myId"
                      className="myClassname"
                      display="initial"
                      position="relative"
                    /> */}
                  </p>
                </div>
              </ReactFullpage.Wrapper>
            </div>
          )
        }}
      />
    </div>
  )
}

Landing.propTypes = {
  auth: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  auth: state.auth
})

export default connect(mapStateToProps, {})(Landing)
