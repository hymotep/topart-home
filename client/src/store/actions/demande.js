import { setAlert } from './alert'
import {
  REGISTER_SUCCESS,
  GET_DEMANDE,
  GET_DEMANDES,
  DELETE_DEMANDE,
  ADD_DEMANDE,
  SET_NUMBERDEVIS,
  DEMANDE_ERROR
} from './types'
import api from '../../outils/api'
import apifile from '../../outils/apifile'

export const uploadDemande =
  (form, file, admin, connected) => async (dispatch) => {
    const body2 = JSON.stringify(form)

    const photo = new FormData()
    // file.map((tmp) => photo.append('file', tmp))

    let tempName
    let temp
    let d = new Date()
    let date =
      d.getFullYear().toString() +
      d.getMonth().toString() +
      d.getDate().toString() +
      d.getHours().toString() +
      d.getMinutes().toString() +
      d.getSeconds().toString()

    if (file.filep1) {
      tempName = form.user.email.split('@')
      temp = file.filep1.name.split('.')
      photo.append(
        'file',
        file.filep1,
        tempName[0] + date + '0.' + temp[temp.length - 1]
      )
    }
    if (file.filep2) {
      tempName = form.user.email.split('@')
      temp = file.filep2.name.split('.')
      photo.append(
        'file',
        file.filep2,
        tempName[0] + date + '1.' + temp[temp.length - 1]
      )
    }
    if (file.filep3) {
      tempName = form.user.email.split('@')
      temp = file.filep3.name.split('.')
      photo.append(
        'file',
        file.filep3,
        tempName[0] + date + '2.' + temp[temp.length - 1]
      )
    }
    // photo.append('file', file.filep1)
    // photo.append('file', file.filep2)
    // photo.append('file', file.filep3)

    if (file.filep1) {
      try {
        await apifile.post('/uploadfile/upload', photo)

        if (!connected && !admin) {
          let name = form.user.name
          let email = form.user.email
          let temp = form.user.email.split('@')

          let password = temp[0] + 'th21'
          const body = JSON.stringify({ name, email, password })
          const res1 = await api.post('/users', body)

          await api.post('/demande/', body2).then(
            (res2) =>
              dispatch({
                type: ADD_DEMANDE,
                payload: res2.data
              }),
            dispatch({
              type: REGISTER_SUCCESS,
              payload: res1.data
            })
          )
        } else if (admin) {
          let name = form.user.name
          let email = form.user.email
          let temp = form.user.email.split('@')

          let password = temp[0] + 'th21'
          const body = JSON.stringify({ name, email, password })
          await api.post('/users', body)

          await api.post('/demande/', body2).then((res2) =>
            dispatch({
              type: ADD_DEMANDE,
              payload: res2.data
            })
          )
        } else {
          const res2 = await api.post('/demande/', body2)
          dispatch({
            type: ADD_DEMANDE,
            payload: res2.data
          })
        }

        dispatch(setAlert('Demande envoyée', 'success'))
      } catch (err) {
        dispatch({
          type: DEMANDE_ERROR,
          payload: { msg: err.response }
        })
      }
    }
  }

//Get all demandes
export const getAllDemande = () => async (dispatch) => {
  try {
    const res = await api.get('/demande')
    dispatch({
      type: GET_DEMANDES,
      payload: res.data
    })
  } catch (err) {
    dispatch({
      type: DEMANDE_ERROR,
      payload: { msg: err.response }
    })
  }
}
export const getAllUserDemande = (userId) => async (dispatch) => {
  try {
    const res = await api.get(`/demande/user/${userId}`)

    dispatch({
      type: GET_DEMANDES,
      payload: res.data
    })
  } catch (err) {
    dispatch({
      type: DEMANDE_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    })
  }
}

// Get demande by ID
export const getDemandeById = (demandeId) => async (dispatch) => {
  try {
    const res = await api.get(`/demande/${demandeId}`)

    dispatch({
      type: GET_DEMANDE,
      payload: res.data
    })

    const res2 = await api.get(`/devis/number/${demandeId}`)

    dispatch({
      type: SET_NUMBERDEVIS,
      payload: res2.data
    })
  } catch (err) {
    dispatch({
      type: DEMANDE_ERROR,
      payload: err.response
    })
  }
}

// Delete demande
export const deleteDemande = (demandeId) => async (dispatch) => {
  try {
    await api.delete(`/demande/${demandeId}`)

    dispatch({
      type: DELETE_DEMANDE,
      payload: demandeId
    })

    dispatch(setAlert('Demande Supprimée', 'success'))
  } catch (err) {
    dispatch({
      type: DEMANDE_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    })
  }
}
