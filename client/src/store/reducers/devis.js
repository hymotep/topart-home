import {
  SET_NUMBERDEVIS,
  GET_ALLDEVIS,
  GET_DEVIS,
  ADD_DEVIS,
  DELETE_DEVIS,
  UPDATE_DEVIS,
  DEVIS_ERROR,
  CLEAR_DEVIS
} from '../actions/types'

const initialState = {
  alldevis: null,
  devis: null,
  loading: true,
  number: 0,
  error: {}
}

export default function (state = initialState, action) {
  const { type, payload } = action

  switch (type) {
    case SET_NUMBERDEVIS:
      return {
        ...state,
        number: payload,
        loading: false
      }
    case GET_ALLDEVIS:
      return {
        ...state,
        alldevis: payload,
        loading: false
      }
    case GET_DEVIS:
      return {
        ...state,
        devis: payload,
        loading: false
      }

    case ADD_DEVIS:
      return {
        ...state,
        alldevis: [payload, ...state.alldevis],
        loading: false
      }
    case DELETE_DEVIS:
      return {
        ...state,
        alldevis: state.alldevis.filter((devis) => devis._id !== payload),
        loading: false
      }
    case UPDATE_DEVIS:
      return {
        ...state,
        devis: payload,
        alldevis: state.alldevis.map((devis) =>
          devis._id === payload.id ? { ...devis, payload } : devis
        ),
        loading: false
      }
    case DEVIS_ERROR:
      return {
        ...state,
        error: payload,

        loading: false
      }
    case CLEAR_DEVIS:
      return {
        ...state,
        devis: null,
        alldevis: null,

        loading: false
      }

    default:
      return state
  }
}
