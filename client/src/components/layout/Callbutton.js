import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

const Callbutton = ({ auth: { isAuthenticated } }) => {
  return (
    <div>
      {!isAuthenticated && (
        <div className="wrapper">
          <div className="ring">
            <div
              className="coccoc-alo-phone coccoc-alo-green coccoc-alo-show"
              href="callto://+32471906347"
            >
              <div className="coccoc-alo-ph-circle"></div>
              <div className="coccoc-alo-ph-circle-fill"></div>
              <a href="tel://+32471906347">
                <div className="coccoc-alo-ph-img-circle"></div>
              </a>
            </div>
          </div>
        </div>
      )}
    </div>
  )
}

Callbutton.propTypes = {
  auth: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  auth: state.auth
})

export default connect(mapStateToProps)(Callbutton)
