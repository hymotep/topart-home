import {
  ADD_EVENT,
  DELETE_EVENT,
  GET_EVENTS,
  GET_EVENT,
  SHOW_CALENDAR,
  HIDE_CALENDAR,
  EVENT_ERROR,
  CLEAR_EVENT
} from '../actions/types'

const initialState = {
  events: null,
  event: null,
  loading: true,
  calendarOpen: false,
  error: {}
}

export default function (state = initialState, action) {
  const { type, payload } = action

  switch (type) {
    case GET_EVENTS:
      return {
        ...state,
        events: payload,
        loading: false
      }
    case GET_EVENT:
      return {
        ...state,
        event: payload,
        loading: false
      }

    case ADD_EVENT:
      return {
        ...state,
        events: [payload, ...state.event],
        loading: false
      }
    case DELETE_EVENT:
      return {
        ...state,
        events: state.events.filter((event) => event._id !== payload),
        loading: false
      }
    case SHOW_CALENDAR:
      return {
        ...state,
        events: payload,
        calendarOpen: true,
        loading: false
      }
    case HIDE_CALENDAR:
      return {
        ...state,
        calendarOpen: false
      }
    case EVENT_ERROR:
      return {
        ...state,
        calendarOpen: false,
        loading: false
      }
    case CLEAR_EVENT:
      return {
        ...state,
        calendarOpen: false,
        event: null,
        events: null,
        loading: false
      }

    default:
      return state
  }
}
