import React, { useEffect, Fragment, useRef, useState } from 'react'
import PropTypes from 'prop-types'

import { connect } from 'react-redux'
import {
  getDevisById,
  sign,
  updateDevis,
  read,
  download
} from '../../store/actions/devis.js'
import { setAlert } from '../../store/actions/alert.js'
import { Table, Form, Col, Button } from 'react-bootstrap'

import Spinner from '../layout/Spinner'

import '../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css'

import { Link } from 'react-router-dom'

import html2canvas from 'html2canvas'
// import logo from '../img/toparthomelogodevis.png'
import logo from '../img/toparthomelogodevis2.png'
import { jsPDF } from 'jspdf'
import SignaturePad from 'react-signature-canvas'
import auth from '../../store/reducers/auth.js'

const Devis = ({
  auth: { user },
  getDevisById,
  read,
  download,
  updateDevis,
  devis: { devis, loading },
  match,
  sign
}) => {
  function reading() {
    if (devis && !devis.read && user._id === devis.user._id) {
      read(devis._id)
    }
  }

  useEffect(() => {
    reading()
  }, [reading()])

  let sigPad = useRef({})

  let list = ['', 0, 0, 0]
  const [inputList, setInputList] = React.useState([])

  let but = { edit: 'false' }
  const [buttonData, setButtonData] = React.useState(but)

  const [form, setForm] = useState({})
  const [errors, setErrors] = useState({})

  useEffect(() => {
    getDevisById(match.params.id)
  }, [])

  const onChange = (e) => {
    setButtonData({ ...buttonData, [e.target.name]: e.target.checked })
    if (e.target.checked && e.target.name.localeCompare('edit') === 0) {
      setInputList(devis.lineExtra)
    }
  }

  const onChange2 = (e) => {
    devis.adresse = e.target.value
    setForm({
      ...form,
      [e.target.name]: e.target.value
    })
    if (!!errors[e.target.name])
      setErrors({
        ...errors,
        [e.target.name]: null
      })
  }
  // const [totalList, setTotalList] = React.useState(calcul)

  ///////////////prendre photo////////////////

  function cleared() {
    sigPad.current.clear()
  }
  function roundDecimal(number) {
    return Math.floor(number * 100) / 100
  }
  /////////////////////////////////////////////////

  // handle input change
  const handleInputChange = (e, index) => {
    const { name, value } = e.target
    const list = [...inputList]
    if (name === '3') {
      list[index][3] = Number(value)
      list[index][2] = ''
      list[index][1] = ''
    } else if (name === '2' || name === '1') {
      list[index][name] = value
      list[index][3] = list[index][1] * list[index][2]
    } else {
      list[index][name] = value
    }

    setInputList(list)

    // const { name, value } = e.target
    // // const list = [...inputList]
    // devis.lineExtra[index][name] = value
    // devis.lineExtra[index][3] =
    //   devis.lineExtra[index][1] * devis.lineExtra[index][2]

    let totalhtva = 0
    for (let i = 0; i < list.length; i++) {
      totalhtva += list[i][3]
    }
    if (devis.lineAuto) {
      for (let i = 0; i < devis.lineAuto.length; i++) {
        totalhtva += devis.lineAuto[i][3]
      }
    }

    devis.totalHtva = totalhtva
    devis.totalTvac = roundDecimal(totalhtva * (1 + devis.tva / 100))
    devis.tvaAmount = roundDecimal(totalhtva * (devis.tva / 100))
    if (!!errors[index.toString() + name])
      setErrors({
        ...errors,
        [index.toString() + name]: null
      })
  }

  // handle click event of the Remove button
  const handleRemoveClick = (index) => {
    // const list = [...inputList]
    // devis.lineExtra.splice(index, 1)
    // setInputList(list)
    const list = [...inputList]
    list.splice(index, 1)
    setInputList(list)

    let totalhtva = 0
    for (let i = 0; i < list.length; i++) {
      totalhtva += list[i][3]
    }
    if (devis.lineAuto) {
      for (let i = 0; i < devis.lineAuto.length; i++) {
        totalhtva += devis.lineAuto[i][3]
      }
    }

    devis.totalHtva = totalhtva
    devis.totalTvac = roundDecimal(totalhtva * 1 + devis.tva / 100)
    devis.tvaAmount = roundDecimal(totalhtva * (devis.tva / 100))
  }

  // handle click event of the Add button
  const handleAddClick = () => {
    // setInputList([
    //   ...inputList,
    //   { designation: '', quantite: 0, prixunit: 0, total: '' }
    // ])
    // devis.lineExtra.push('', '', '', '')
    setInputList([...inputList, ['', null, null, null]])
  }

  function prepare(sended) {
    return {
      id: devis._id,
      lineExtra: buttonData.edit === true ? inputList : devis.lineExtra,
      totalHtva: devis.totalHtva,
      totalTvac: devis.totalTvac,
      tvaAmount: devis.tvaAmount,
      tva: devis.tva,

      send: sended
    }
  }
  const save = async (e) => {
    const newErrors = findFormErrors('send')
    // Conditional logic:
    if (Object.keys(newErrors).length > 0) {
      // We got errors!
      setErrors(newErrors)
    } else {
      updateDevis(prepare(false)).then((res) => window.history.back())
    }
  }

  const send = async (e) => {
    const newErrors = findFormErrors('send')
    // Conditional logic:
    if (Object.keys(newErrors).length > 0) {
      // We got errors!
      setErrors(newErrors)
    } else {
      updateDevis(prepare(true))
    }
  }
  function capture() {
    const pdf = new jsPDF() //creation du fichier pdf

    const newErrors = findFormErrors('adresse') // verifie les erreurs du formulaire
    if (Object.keys(newErrors).length > 0) {
      //si l'adresse est introduite il peut télécharger le devis
      setErrors(newErrors)
    } else {
      if (devis.pdf === false) {
        download(devis._id) //si ce n'est pas encore fait enregistre en db que le client à téléchargé le devis
      }
      html2canvas(document.querySelector('#deviss'), {
        //récupère une image du tableau du devis
        scrollY: -window.scrollY,
        scrollX: -window.scrollX
      }).then((canvas) => {
        const imgData = canvas.toDataURL('image/png')

        pdf.addImage(logo, 10, 10, 80, 40) //rajout de tous les éléments nécéssaire au devis.
        pdf.addImage(imgData, 'PNG', 15, 100, 170, 100)
        pdf.setFontSize(8)
        var myIm
        pdf.text(
          110,
          240,
          'En signant le client accepte le devis et les Conditions générale de vente'
        )
        pdf.text(130, 245, 'Signature : ')

        pdf.rect(128, 248, 64, 34)
        if (devis.accepted) {
          myIm = require('../../upload/signatures/' + devis.numero + '.jpg')
          pdf.addImage(myIm, 'png', 130, 250, 60, 30)
        } else {
          myIm = sigPad.current.toDataURL('image/png')
          pdf.addImage(myIm, 'png', 130, 250, 60, 30)
        }

        pdf.text(120, 15, 'Devis : ' + devis.numero)
        pdf.text(120, 25, 'Client : ' + devis.user.name)

        pdf.text(120, 35, 'Adresse Chantier :' + devis.adresse)

        pdf.text(120, 45, 'Date :' + devis.date.substring(0, 10))
        pdf.text(120, 55, 'Durée de validité : 30jours')
        pdf.text(120, 65, 'Début chantier : ' + devis.extra.dateDeb)
        pdf.save(devis.numero + '.pdf') //telechargement du devis avec comme nom son numéro
      })
    }
  }

  const sendSign = async (e) => {
    e.preventDefault()
    const newErrors = findFormErrors('adresse')
    const myIms = sigPad.current.toDataURL('image/jpeg', 0.5).toString()

    if (Object.keys(newErrors).length > 0) {
      setErrors(newErrors)
    }
    var x
    var x2
    if (myIms.slice(-6).localeCompare('D/2Q==') === 0) {
      //si = 'D/2Q==' le canvas est vide
      x = document.getElementById('errorsignature') //un message d'erreur apparait
      x.style.display = 'block'
      x2 = document.getElementById('bordersignature') //la bordure du canvas devient rouge
      x2.style.borderColor = '#F08080'
    } else {
      x = document.getElementById('errorsignature') //sinon disparition de l'erreur et retour à
      x.style.display = 'none' //une bordure noir
      x2 = document.getElementById('bordersignature')
      x2.style.borderColor = 'black'
    }

    if (
      Object.keys(newErrors).length === 0 &&
      myIms.slice(-6).localeCompare('D/2Q==') !== 0
    ) {
      sign(myIms, devis.numero, devis._id, devis.adresse).then((res) =>
        getDevisById(match.params.id)
      )
    }
  }

  const findFormErrors = (type) => {
    const adresse = devis.adresse
    const newErrors = {}
    switch (type) {
      case 'send': {
        Object.entries(inputList).map(([key, value]) => {
          if (!inputList[key][0] || inputList[key][0] === '') {
            newErrors[key.toString() + '0'] = 'Choisir une désignation!'
          }

          if (!inputList[key][3] || inputList[key][3] === '') {
            newErrors[key.toString() + '3'] = 'Prix total ou unitaire!'
          }
        })

        break
      }
      case 'adresse': {
        if (!adresse || adresse === '')
          newErrors.adresse = "Veuillez introduire l'adresse du chantier"
        break
      }
      default: {
      }
    }
    // name errors

    return newErrors
  }

  return (
    <Fragment>
      <button onClick={() => window.history.back()} className="demdevisbutton2">
        Retour
      </button>
      {loading ? (
        <Spinner />
      ) : (
        devis && (
          <div id="rotate">
            {!devis.send && (
              <Form.Check
                type="switch"
                id="switch"
                name="edit"
                value={buttonData.edit}
                label="Editer"
                onChange={onChange}
              />
            )}
            <Form.Row style={{ width: '700px' }}>
              <Col>
                <img
                  style={{ width: '300px', height: '150px' }}
                  src={logo}
                  alt="..."
                ></img>
              </Col>
              <Col>
                <div>
                  <div>Devis n° :{devis.numero}</div>
                  <div>Nom Client :{devis.user.name}</div>
                  <div></div>
                  <div></div>
                  <div>Date :{devis.date.substring(0, 10)} </div>
                  <div>Durée de validité : 30jours</div>
                </div>
              </Col>
            </Form.Row>

            <div>
              Date possible de début de chantier :{devis.extra.dateDeb}{' '}
            </div>
            <div className="tabledevis">
              <Table bordered id="deviss">
                <thead className="testing">
                  <tr>
                    <th>Designation</th>
                    <th>Prix unitaire</th>
                    <th>Quantité </th>
                    <th>Total</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td colSpan="4" style={{ textTransform: 'capitalize' }}>
                      {devis.titre}
                    </td>
                  </tr>

                  {devis.lineAuto &&
                    Object.entries(devis.lineAuto).map(([key, value]) => (
                      <tr>
                        {devis.lineAuto[key].map((x) => (
                          <td>{x} </td>
                        ))}
                      </tr>
                    ))}
                  {buttonData.edit === true
                    ? Object.entries(inputList).map(([key, value]) => (
                        <tr>
                          {inputList[key].map((x) => (
                            <td>{x} </td>
                          ))}
                        </tr>
                      ))
                    : Object.entries(devis.lineExtra).map(([key, value]) => (
                        <tr>
                          {devis.lineExtra[key].map((x) => (
                            <td>{x} </td>
                          ))}
                        </tr>
                      ))}
                  <tr>
                    <td colSpan="4"></td>
                  </tr>
                  <tr>
                    <td colSpan="3">Total Htva</td>
                    <td>{devis.totalHtva} </td>
                  </tr>
                  <tr>
                    <td colSpan="3">Tva {devis.tva} % </td>
                    <td>{devis.tvaAmount} </td>
                  </tr>
                  <tr>
                    <td colSpan="3">Total Tvac</td>
                    <td>{devis.totalTvac}</td>
                  </tr>
                </tbody>
              </Table>

              <body>
                <div id="preview-pane"></div>
              </body>
            </div>

            {devis.send ? (
              <div>
                <h6>
                  En signant le client accepte le devis et les{' '}
                  <Link to="/conditiongenerale">
                    Conditions générale de vente
                  </Link>
                </h6>

                <Form>
                  <Form.Group>
                    <Form.Label>Adresse Chantier</Form.Label>
                    <div>
                      {devis.accepted ? (
                        <div>{devis.adresse}</div>
                      ) : (
                        <div>
                          <Form.Control
                            type="string"
                            name="adresse"
                            value={devis.adresse}
                            onChange={onChange2}
                            isInvalid={!!errors.adresse}
                            style={{ width: '200px' }}
                          />
                          <Form.Control.Feedback type="invalid">
                            {errors.adresse}
                          </Form.Control.Feedback>
                        </div>
                      )}
                    </div>
                  </Form.Group>
                </Form>
                <h4>Signature :</h4>
                <div
                  id="errorsignature"
                  style={{ display: 'none', color: 'LightCoral' }}
                >
                  Veuillez signer le devis !{' '}
                </div>
                <div>
                  <div
                    id="bordersignature"
                    style={{
                      marginLeft: '-20px',
                      border: 'solid',
                      width: 350,
                      height: 210
                    }}
                  >
                    {devis.accepted ? (
                      <img
                        style={{ width: '340px', height: '200px' }}
                        src={require('../../upload/signatures/' +
                          devis.numero +
                          '.jpg')}
                        alt="..."
                      />
                    ) : (
                      <SignaturePad
                        // id="canvassign"
                        penColor="blue"
                        ref={sigPad}
                        backgroundColor="rgba(255, 255, 255)"
                        canvasProps={{
                          width: 340,
                          height: 200,
                          className: 'sigCanvas'
                        }}
                      />
                    )}
                  </div>
                </div>
                {!devis.accepted && (
                  <div>
                    <button
                      onClick={() => cleared()}
                      className="demdevisbutton2"
                    >
                      Recommencer signature
                    </button>
                    <button
                      onClick={(e) => sendSign(e)}
                      className="demdevisbutton2"
                    >
                      Envoyer le devis signé
                    </button>
                  </div>
                )}
              </div>
            ) : (
              <div>
                {buttonData.edit === true && (
                  <div>
                    <div>
                      {inputList.map((x, i) => {
                        return (
                          <div style={{ marginBottom: '5px' }}>
                            <Form.Row className="devisinputlist">
                              <Col>
                                <Form.Control
                                  className="createdevisinputdes"
                                  name="0"
                                  value={x[0]}
                                  placeholder="Désignation"
                                  onChange={(e) => handleInputChange(e, i)}
                                  isInvalid={!!errors[i.toString() + '0']}
                                />
                                <Form.Control.Feedback type="invalid">
                                  {errors[i.toString() + '0']}
                                </Form.Control.Feedback>
                              </Col>
                              <Col>
                                <Form.Control
                                  className="createdevisinput"
                                  type="number"
                                  min="0"
                                  name="1"
                                  value={x[1]}
                                  placeholder="Prix Unit"
                                  onChange={(e) => handleInputChange(e, i)}
                                  isInvalid={!!errors[i.toString() + '1']}
                                />
                                <Form.Control.Feedback type="invalid">
                                  {errors[i.toString() + '1']}
                                </Form.Control.Feedback>
                              </Col>
                              <Col>
                                <Form.Control
                                  className="createdevisinput"
                                  type="number"
                                  min="0"
                                  name="2"
                                  value={x[2]}
                                  placeholder="Quant"
                                  onChange={(e) => handleInputChange(e, i)}
                                  isInvalid={!!errors[i.toString() + '2']}
                                />
                                <Form.Control.Feedback type="invalid">
                                  {errors[i.toString() + '2']}
                                </Form.Control.Feedback>
                              </Col>
                              <Col>
                                <Form.Control
                                  className="createdevisinput"
                                  type="number"
                                  min="0"
                                  name="3"
                                  value={x[3]}
                                  placeholder="Total"
                                  onChange={(e) => handleInputChange(e, i)}
                                  isInvalid={!!errors[i.toString() + '3']}
                                />
                                <Form.Control.Feedback type="invalid">
                                  {errors[i.toString() + '3']}
                                </Form.Control.Feedback>
                              </Col>
                              {inputList.length !== 1 && i > 0 && (
                                <Col lg={1}>
                                  <button
                                    className="mr10"
                                    onClick={() => handleRemoveClick(i)}
                                  >
                                    Remove
                                  </button>
                                </Col>
                              )}
                            </Form.Row>
                            <button
                              onClick={() => handleAddClick()}
                              className="demdevisbutton2"
                            >
                              Nouvelle Ligne
                            </button>
                          </div>
                        )
                      })}
                    </div>
                  </div>
                )}
              </div>
            )}
            {!devis.send && (
              <div>
                <Button id="validbutton" onClick={save}>
                  Sauvegarder
                </Button>
                <Button id="validbutton" onClick={send}>
                  Envoyer
                </Button>
              </div>
            )}

            <button onClick={() => capture()}>Télécharger en format pdf</button>
          </div>
        )
      )}
    </Fragment>
  )
}

Devis.propTypes = {
  getDevisById: PropTypes.func.isRequired,
  updateDevis: PropTypes.func.isRequired,
  setAlert: PropTypes.func.isRequired,
  sign: PropTypes.func.isRequired,
  read: PropTypes.func.isRequired,
  download: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  demande: PropTypes.object.isRequired,
  devis: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  demande: state.demande,
  devis: state.devis
})

export default connect(mapStateToProps, {
  getDevisById,
  sign,
  setAlert,
  updateDevis,
  read,
  download
})(Devis)
