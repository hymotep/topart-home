import { combineReducers } from 'redux'
// import { reducer as modal } from 'redux-modal';
import modal from './modal'
import alert from './alert'
import auth from './auth'
import demande from './demande'
import price from './price'
import devis from './devis'
import user from './user'
import event from './event'
import chantier from './chantier'
import facture from './facture'
import email from './email'
import compta from './compta'
export default combineReducers({
  alert,
  auth,
  modal,
  demande,
  price,
  devis,
  user,
  event,
  chantier,
  facture,
  email,
  compta
})
