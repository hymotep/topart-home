import {
  GET_FACTURE,
  GET_FACTURES,
  UPDATE_FACTURE,
  FACTURE_ERROR,
  CLEAR_FACTURE
} from '../actions/types'

const initialState = {
  facture: null,
  factures: null,
  loading: true,
  errors: {}
}

export default function (state = initialState, action) {
  const { type, payload } = action

  switch (type) {
    case GET_FACTURE:
      return {
        ...state,
        facture: payload,
        loading: false
      }
    case GET_FACTURES:
      return {
        ...state,
        factures: payload,
        loading: false
      }
    case UPDATE_FACTURE:
      const index = state.factures.findIndex(
        (facture) => facture._id === payload._id
      )
      const newArray = [...state.factures]
      newArray[index].payed = payload.payed
      return {
        ...state,
        factures: newArray,
        facture: payload,
        loading: false
      }
    case FACTURE_ERROR:
      return {
        ...state,
        errors: payload,
        loading: false
      }
    case CLEAR_FACTURE:
      return {
        ...state,
        facture: null,
        factures: null,
        loading: false
      }

    default:
      return state
  }
}
