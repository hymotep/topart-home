import { setAlert } from './alert'
import {
  ADD_EVENT,
  UPDATE_CHANTIER,
  DELETE_EVENT,
  GET_EVENTS,
  GET_EVENT,
  SHOW_CALENDAR,
  HIDE_CALENDAR,
  EVENT_ERROR
} from './types'
import api from '../../outils/api'

//openCalendar
export const showCalendar = (formData) => async (dispatch) => {
  try {
    const res = await api.get('/event')
    dispatch({
      type: SHOW_CALENDAR,
      payload: res.data
    })
  } catch (error) {}
}

//hideCalendar
export const hideCalendar = (formData) => async (dispatch) => {
  dispatch({
    type: HIDE_CALENDAR
  })
}

//Get all event
export const getEvents = () => async (dispatch) => {
  try {
    const res = await api.get('/event')
    dispatch({
      type: GET_EVENTS,
      payload: res.data
    })
  } catch (err) {
    dispatch({
      type: EVENT_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    })
  }
}

//Update event
export const updateEvent = (data) => async (dispatch) => {
  try {
    const body = JSON.stringify(data)
    await api.put('/event', body).then(
      (res) =>
        dispatch({
          type: UPDATE_CHANTIER,
          payload: res.data
        }),
      dispatch(setAlert('Event updated', 'success'))
    )
  } catch (err) {
    dispatch({
      type: EVENT_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    })
  }
}

//Add one event
// export const addEvent = (formData) => async (dispatch) => {
//   try {
//     const res = await api.post('/event', formData)
//     dispatch({
//       type: ADD_EVENT,
//       payload: res.data
//     })
//     dispatch(setAlert('Event Added', 'success'))
//   } catch (err) {
//     const errors = err.response.data.errors

//     if (errors) {
//       errors.forEach((error) => dispatch(setAlert(error.msg, 'danger')))
//     }
//     dispatch({
//       type: EVENT_ERROR,
//       payload: { msg: err.response.statusText, status: err.response.status }
//     })
//   }
// }

//Get one event
// export const getEvent = (id) => async (dispatch) => {
//   try {
//     const res = await api.get(`/event/one/${id}`)
//     dispatch({
//       type: GET_EVENT,
//       payload: res.data
//     })
//   } catch (err) {
//     dispatch({
//       type: EVENT_ERROR,
//       payload: { msg: err.response.statusText, status: err.response.status }
//     })
//   }
// }

// Delete demande
// export const deleteEvent = (id) => async (dispatch) => {
//   try {
//     await api.delete(`/event/${id}`)

//     dispatch({
//       type: DELETE_EVENT,
//       payload: id
//     })

//     dispatch(setAlert('Event Supprimé', 'success'))
//   } catch (err) {
//     dispatch({
//       type: EVENT_ERROR,
//       payload: { msg: err.response.statusText, status: err.response.status }
//     })
//   }
// }
