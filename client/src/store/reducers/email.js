import { ModalEmail } from '../actions/types'

const initialState = {
  emailOpen: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case ModalEmail.SHOW_EMAIL:
      return {
        ...state,
        emailOpen: true
      }
    case ModalEmail.HIDE_EMAIL:
      return {
        ...state,
        emailOpen: false
      }
    default:
      return state
  }
}
