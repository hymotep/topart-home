const mongoose = require('mongoose')
const datefull = Date.now.Date

const CountersSchema = new mongoose.Schema({
  _id: { type: String },
  sequence_value: { type: Number }
})

module.exports = Counters = mongoose.model('counters', CountersSchema)
