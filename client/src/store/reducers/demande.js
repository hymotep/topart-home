import {
  GET_DEMANDE,
  GET_DEMANDES,
  DELETE_DEMANDE,
  ADD_DEMANDE,
  DEMANDE_ERROR,
  CLEAR_DEMANDE
} from '../actions/types'

const initialState = {
  demandes: null,
  onedemande: null,
  loading: true,
  error: {}
}

export default function (state = initialState, action) {
  const { type, payload } = action

  switch (type) {
    case GET_DEMANDES:
      return {
        ...state,
        demandes: payload,
        loading: false
      }
    case GET_DEMANDE:
      return {
        ...state,
        onedemande: payload,
        loading: false
      }

    case ADD_DEMANDE:
      return {
        ...state,
        demandes: [...state.demandes, payload],
        loading: false
      }
    case DELETE_DEMANDE:
      return {
        ...state,
        demandes: state.demandes.filter((demande) => demande._id !== payload),
        loading: false
      }
    // case GET_PROFILE:
    //   return {
    //     ...state,
    //     profile: payload,
    //     loading: false
    //   };
    case DEMANDE_ERROR:
      return {
        ...state,
        error: payload,
        loading: false
      }
    case CLEAR_DEMANDE:
      return {
        ...state,
        demande: null,
        demandes: null,
        loading: false
      }
    default:
      return state
  }
}
