import React, { useEffect, Fragment } from 'react'
import PropTypes from 'prop-types'

import { connect } from 'react-redux'
import { getDemandeById } from '../../../store/actions/demande.js'
import { getAPrice } from '../../../store/actions/price.js'
import { Button, Table, Form, Col } from 'react-bootstrap'
import '../../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css'

import { Link } from 'react-router-dom'
import Spinner from '../../layout/Spinner'

import html2canvas from 'html2canvas'
import logo from '../../img/toparthomelogodevis.png'
// import test2 from './tva.PNG'
import jsPDF from 'jspdf'
import { uploadDevis } from '../../../store/actions/devis'

const DevisTerrasseBois = ({
  getDemandeById,
  getAPrice,
  demande: { onedemande, loading },
  price: { price, prices },
  uploadDevis,
  match,
  devis
}) => {
  //////////////////////////////////////////////
  ///////////////convert en pdf ////////////////
  //////////////////////////////////////////////

  function capture() {
    html2canvas(document.querySelector('#deviss'), {
      scrollY: -window.scrollY,
      scrollX: -window.scrollX
    }).then((canvas) => {
      // document.body.appendChild(canvas) // if you want see your screenshot in body.
      const imgData = canvas.toDataURL('image/png')
      const pdf = new jsPDF()
      pdf.addImage(logo, 10, 10, 80, 50)
      pdf.addImage(imgData, 'PNG', 15, 100)
      pdf.text(150, 15, 'Client : ' + onedemande.data.user.name)
      pdf.text(150, 25, 'Devis : ' + formData.numero)
      pdf.save(formData.numero + '.pdf')
    })
  }

  /////////////////////////////////////////////////
  /////////////////////////////////////////////////
  /////////////////////////////////////////////////

  let data = {
    date: '',
    numero: '',
    ecart: 0,
    ecartLamb: 0,
    ecartPlot: 0,
    compositebool: '',
    nbrelamb: 0,
    nbreTotalLamb: 0,
    test: '',
    fixe: false,
    plot: false,
    dalle: false,
    contour: false,
    nbreVis: 0,
    dimdecoupe: 0,
    titre: '',
    dateFin: '',
    dateDeb: '',
    adresse: ' ',
    mat: '',
    typestruct: ''
  }
  let planche = {
    nbrePlancheLongueur: '',
    nbrePlancheLargeur: '',
    nbrePlanche: 0
  }
  let ecart
  let compositebool

  let [formData, setFormData] = React.useState(data)
  let [nbrePlancheData, setnbrePlancheData] = React.useState(planche)
  let [devisligneData, setdevisligneData] = React.useState({})

  let dat = new Date().getFullYear()
  let dateToday =
    new Date().getDate() +
    '/' +
    (new Date().getMonth() < 10
      ? '0' + new Date().getMonth()
      : new Date().getMonth()) +
    '/' +
    dat

  // let compteur = 0
  let [compteur] = React.useState({ compteur: 0 })
  let calcul = {
    totalHtva: 0,
    totalTvac: 0,
    tva: 21,
    tvaAmount: 0
  }

  const [totalList, setTotalList] = React.useState(calcul)
  let list = ["Main d'oeuvre", '', '', '']
  const [inputList, setInputList] = React.useState([list])

  const [errors, setErrors] = React.useState({})

  const findFormErrors = (type) => {
    const newErrors = {}
    switch (type) {
      case 'planche': {
        if (
          !onedemande.data.typemat ||
          onedemande.data.typemat === '' ||
          onedemande.data.typemat === undefined
        ) {
          newErrors.typemat = 'Choisir un bois!'
        }
        if (
          !onedemande.data.typestruct ||
          onedemande.data.typestruct === '' ||
          onedemande.data.typestruct === undefined ||
          onedemande.data.typestruct === null
        ) {
          newErrors.typestruct = 'Choisir une structure!'
        }
        break
      }
      case 'send': {
        if (!formData.dateDeb || formData.dateDeb === '') {
          newErrors.dateDeb = 'Choisir une date!'
        }
        if (!formData.dateFin || formData.dateFin === '') {
          newErrors.dateFin = 'Choisir une date!'
        } else if (formData.dateFin < formData.dateDeb) {
          newErrors.dateFin = 'Date précède date de début'
        }

        Object.entries(inputList).forEach(([key, value]) => {
          if (!inputList[key][0] || inputList[key][0] === '') {
            newErrors[key.toString() + '0'] = 'Choisir une désignation!'
          }

          if (!inputList[key][3] || inputList[key][3] === '') {
            newErrors[key.toString() + '3'] = 'Prix total ou unitaire!'
          }
        })

        break
      }
      default: {
      }
    }

    return newErrors
  }

  // handle input change
  const handleInputChange = (e, index) => {
    const { name, value } = e.target
    const list = [...inputList]

    if (name === '3') {
      list[index][3] = Number(value)
      list[index][2] = ''
      list[index][1] = ''
    } else if (name === '2' || name === '1') {
      list[index][name] = value
      list[index][3] = list[index][1] * list[index][2]
    } else {
      list[index][name] = value
    }

    setInputList(list)
    let totalHtva = 0
    for (let i = 0; i < list.length; i++) {
      totalHtva += list[i][3]
    }
    for (let i = 0; i < devisligneData.length; i++) {
      totalHtva += devisligneData[i][3]
    }
    totalList.totalHtva = roundDecimal(totalHtva)
    totalList.totalTvac = roundDecimal(totalHtva * (1 + totalList.tva / 100))
    totalList.tvaAmount = roundDecimal(totalHtva * (totalList.tva / 100))
    if (!!errors[index.toString() + name])
      setErrors({
        ...errors,
        [index.toString() + name]: null
      })
  }

  // handle click event of the Remove button
  const handleRemoveClick = (index) => {
    const list = [...inputList]
    list.splice(index, 1)
    setInputList(list)

    setTotalList({
      ...totalList,
      totalHtva: roundDecimal(totalList.totalHtva - inputList[index][3]),
      totalTvac: roundDecimal(
        (totalList.totalHtva - inputList[index][3]) * (1 + totalList.tva / 100)
      ),
      tvaAmount: roundDecimal(
        (totalList.totalHtva - inputList[index][3]) * (totalList.tva / 100)
      )
    })
  }

  // handle click event of the Add button
  const handleAddClick = (index) => {
    // devisligneData.push(inputList[index])
    setInputList([...inputList, ['', null, null, null]])
    // setTotalList({
    //   ...totalList,
    //   total: roundDecimal(totalList.total + inputList[index][3]),
    //   totalTva: roundDecimal((totalList.total + inputList[index][3]) * 1.21)
    // })
  }
  let search = { un: 'tbois', deux: 'tboisaccess' }

  useEffect(() => {
    getDemandeById(match.params.id)
  }, [getDemandeById, match.params.id])
  useEffect(() => {
    getAPrice(search)
  }, [])

  function init() {
    // get our new errors
    const newErrors = findFormErrors('planche')
    // Conditional logic:

    if (Object.keys(newErrors).length > 0) {
      // We got errors!
      setErrors(newErrors)
    } else {
      var x = document.getElementById('showdevis1')
      if (x.style.display === 'none') {
        x.style.display = 'block'
      }
      var x2 = document.getElementById('showcalcul')

      if (x2.style.display === 'block') {
        x2.style.display = 'none'
      }

      formData.mat = prices.filter(
        (price) => price.name === onedemande.data.typemat
      )
      formData.typestruct = prices.filter(
        (price) => price.name === onedemande.data.typestruct
      )
      setFormData({
        ...formData,
        mat: prices.filter((price) => price.name === onedemande.data.typemat),
        typestruct: prices.filter(
          (price) => price.name === onedemande.data.typestruct
        )
      })

      if (
        onedemande.data.typemat.localeCompare('compositeb') === 0 ||
        onedemande.data.typemat.localeCompare('compositea') === 0
      ) {
        compositebool = true
      }
      ecart = compositebool ? 0.3 : 0.5

      //recupère et enregistre les données

      let nbrePlancheLong = onedemande.data.longueur / formData.mat[0].longueur
      nbrePlancheLong =
        (onedemande.data.longueur - (Math.ceil(nbrePlancheLong) - 1) * ecart) /
        formData.mat[0].longueur

      let nbrePlancheLarg =
        onedemande.data.largeur / (formData.mat[0].largeur + ecart)

      let temptotal = nbrePlancheLong * nbrePlancheLarg

      let decoupe = formData.mat[0].longueur * (nbrePlancheLong % 1)

      if (compositebool) {
        temptotal = Math.ceil(temptotal / 3)
      }
      setFormData({
        ...formData,
        date: dateToday,
        numero: Number(
          dat.toString() +
            onedemande.numero.toString() +
            devis.number.toString()
        ),
        ecart: compositebool ? 0.3 : 0.5,
        compositebool: compositebool,
        dimdecoupe: roundDecimal(decoupe),
        adresse: onedemande.user.adresse
      })

      setnbrePlancheData({
        ...nbrePlancheData,
        nbrePlancheLongueur: nbrePlancheLong,
        nbrePlancheLargeur: nbrePlancheLarg,
        nbrePlanche: temptotal
      })
    }
  }

  let onChange = (e) => {
    switch (e.target.name) {
      case 'typemat': {
        onedemande.data.typemat = e.target.value
        setFormData({ ...formData, [e.target.name]: e.target.value })
        if (!!errors['typemat'])
          setErrors({
            ...errors,
            typemat: null
          })
        break
      }
      case 'typestruct': {
        onedemande.data.typestruct = e.target.value
        setFormData({ ...formData, [e.target.name]: e.target.value })
        if (!!errors['typestruct'])
          setErrors({
            ...errors,
            typestruct: null
          })
        break
      }
      default: {
        setFormData({ ...formData, [e.target.name]: e.target.value })
        if (!!errors[e.target.name])
          setErrors({
            ...errors,
            [e.target.name]: null
          })
      }
    }
  }
  let onChange3 = (e) => {
    setTotalList({
      ...totalList,
      tva: e.target.value,
      totalTvac: roundDecimal(totalList.totalHtva * (1 + e.target.value / 100)),
      tvaAmount: roundDecimal(totalList.totalHtva * (e.target.value / 100))
    })
  }

  let onChange2 = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.checked })
    if (e.target.name.localeCompare('contour') === 0) {
      let temp =
        (onedemande.data.largeur / formData.mat[0].longueur +
          nbrePlancheData.nbrePlancheLongueur) *
        2
      if (formData.compositebool) {
        temp = temp / 3
      }
      if (e.target.checked === true) {
        setnbrePlancheData({
          ...nbrePlancheData,
          nbrePlanche: nbrePlancheData.nbrePlanche + temp
        })
      } else {
        setnbrePlancheData({
          ...nbrePlancheData,
          nbrePlanche: nbrePlancheData.nbrePlanche - temp
        })
      }
    }
  }

  function roundDecimal(number) {
    return Math.floor(number * 100) / 100
  }

  function calculLongueur(rajout, endessous) {
    let range = [0, 1 / 8, 1 / 7, 1 / 6, 1 / 5, 1 / 4, 1 / 3, 1 / 2]
    if (formData.fixe === false && !endessous) {
      range.push(2 / 3, 1, 1)
    } else {
      range.push(1, 1)
    }
    for (let i = 0; ; i++) {
      if (rajout === range[i]) {
        if (endessous) {
          return range[i - 1]
        } else {
          return range[i + 1]
        }
      } else if (rajout > range[i] && rajout < range[i + 1]) {
        if (endessous) {
          if (
            rajout === 0.19999999999999996 ||
            rajout === 0.6638935108153079 ||
            rajout === 0.3333333333333335
          ) {
            return range[i - 2]
          }

          return range[i]
        } else {
          if (
            rajout === 0.19999999999999996 ||
            rajout === 0.6638935108153079 ||
            rajout === 0.33333333333333326
          ) {
            return range[i + 2]
          }
          return range[i + 1]
        }
      }
    }
  }
  function calculLamb(rajout) {
    let range = [0, 1 / 8, 1 / 7, 1 / 6, 1 / 5, 1 / 4, 1 / 3, 1 / 2, 2 / 3, 1]
    for (let i = 0; ; i++) {
      if (rajout === 0 || rajout === range[i + 1]) {
        return rajout
      } else if (rajout > range[i] && rajout < range[i + 1]) {
        return range[i + 1]
      }
    }
  }
  function calculPlot(longueur) {
    let ecartPlot
    switch (
      onedemande.data.typestruct //selon type de structure defini l'ecart entre plot
    ) {
      case 'lambtraite': {
        ecartPlot = 60
        break
      }
      case 'lambcomp': {
        ecartPlot = 35
        break
      }
      case 'lambazobe': {
        ecartPlot = 60
        break
      }
      case 'lambalu': {
        ecartPlot = 70
        break
      }
      default: {
      }
    }
    let largeur = Math.round(onedemande.data.largeur / ecartPlot) //nombre arrondi de plot en largeur

    return [longueur * largeur, ecartPlot] //renvoie le nombre de plot et l'ecart
  }

  function calculsVis(longueurTotalLamb) {
    let nbreVis
    let nbreBoite
    let prixBoite
    let vis

    if (
      onedemande.data.typemat.localeCompare('compositea') === 0 ||
      onedemande.data.typemat.localeCompare('compositeb') === 0
    ) {
      vis = prices.filter((price) => price.name === 'clip')
      nbreVis =
        (nbrePlancheData.nbrePlancheLargeur - 1) * Math.ceil(longueurTotalLamb)
      nbreBoite = Math.ceil(nbreVis / vis[0].nombre)
      prixBoite = vis[0].prixTvac
    } else if (onedemande.data.typestruct.localeCompare('lambalu') === 0) {
      vis = prices.filter((price) => price.name === 'visMetal')
      nbreVis =
        nbrePlancheData.nbrePlancheLargeur * Math.ceil(longueurTotalLamb) * 2

      nbreBoite = Math.ceil(nbreVis / vis[0].nombre)
      prixBoite = vis[0].prixTvac
    } else {
      vis = prices.filter((price) => price.name === 'visNormal')
      nbreVis =
        nbrePlancheData.nbrePlancheLargeur * Math.ceil(longueurTotalLamb) * 2

      nbreBoite = Math.ceil(nbreVis / vis[0].nombre)
      prixBoite = vis[0].prixTvac
    }
    return [Math.ceil(nbreVis), nbreBoite, prixBoite]
  }
  function calculdecoupe(nbreMorceau) {
    let temp = nbreMorceau
    if (formData.fixe) {
      temp = calculLongueur(nbreMorceau, false)
    }
    switch (temp) {
      case 1 / 8: {
        return 7
      }
      case 1 / 7: {
        return 6
      }
      case 1 / 6: {
        return 5
      }
      case 1 / 5: {
        return 4
      }
      case 1 / 4: {
        return 3
      }
      case 1 / 3: {
        return 2
      }
      case 1 / 2:
      case 2 / 3: {
        return 1
      }
      case 1: {
        return 0
      }
      case 0: {
        return 0
      }
      default: {
      }
    }
  }

  function totalPlanche(temptotal) {
    if (formData.compositebool) {
      temptotal = Math.ceil(temptotal / 3)
    }
    return temptotal
  }
  //arrondir vers le bas
  function roundFloor(islargeur) {
    if (islargeur) {
      //si on veut arrondir la largeur
      nbrePlancheData.nbrePlancheLargeur = Math.floor(
        //on arrondit vers l'entier de planche inférieur
        nbrePlancheData.nbrePlancheLargeur
      )

      setnbrePlancheData({
        ...nbrePlancheData,
        nbrePlanche: totalPlanche(
          //on recalcule le total de planche  en multipliant
          nbrePlancheData.nbrePlancheLongueur * //le nombre de planche en longueur
            nbrePlancheData.nbrePlancheLargeur // par le nouveau nbre de planche en largeur
        )
      })
      onedemande.data.largeur = //on recalcule la largeur de la terrasse
        nbrePlancheData.nbrePlancheLargeur *
          (formData.mat[0].largeur + formData.ecart) -
        formData.ecart
    } else {
      //si on veut arrondir la longueur
      let entier = Math.floor(nbrePlancheData.nbrePlancheLongueur) //on arrondis à l'entier inférieur
      formData.ecart = entier >= 1 ? formData.ecart : 0 //si il n'y a max 1 planche en longueur pas d'écart.

      //on arrondit le reste à un nombre de bout divisible égaux d'une planche(fraction de 1/8 à 1/2)
      let reste = calculLongueur(nbrePlancheData.nbrePlancheLongueur % 1, true)

      let total = entier + reste //addition des deux

      setnbrePlancheData({
        ...nbrePlancheData,
        nbrePlancheLongueur: total, //on sauvegarde la nouvelle valeur du nombre de planche en longueur
        nbrePlanche: totalPlanche(total * nbrePlancheData.nbrePlancheLargeur) // on recalcule le total de planche
      })

      let long = //on recalcule la longueur effective de la terrasse
        entier * formData.mat[0].longueur +
        (Math.ceil(total) - 1) * formData.ecart +
        (formData.mat[0].longueur - calculdecoupe(reste) * 0.24) * reste

      let decoupe = roundDecimal(
        //on calcul la di mension des découpes pour faciliter le travail lors du chantier
        (formData.mat[0].longueur - calculdecoupe(reste) * 0.24) * (total % 1)
      )
      onedemande.data.longueur = long
      setFormData({
        //on sauvegarde les 2 nouvelles valeur précédantes
        ...formData,
        longueur: long,
        dimdecoupe: decoupe
      })
    }
  }
  //arrondir vers le haut
  function roundCeil(islargeur) {
    if (islargeur) {
      nbrePlancheData.nbrePlancheLargeur =
        nbrePlancheData.nbrePlancheLargeur % 1 === 0
          ? nbrePlancheData.nbrePlancheLargeur + 1
          : Math.ceil(nbrePlancheData.nbrePlancheLargeur)

      setnbrePlancheData({
        ...nbrePlancheData,
        nbrePlanche: totalPlanche(
          nbrePlancheData.nbrePlancheLongueur *
            nbrePlancheData.nbrePlancheLargeur
        )
      })

      onedemande.data.largeur =
        nbrePlancheData.nbrePlancheLargeur *
          (formData.mat[0].largeur + formData.ecart) -
        formData.ecart
    } else {
      let entier = Math.floor(nbrePlancheData.nbrePlancheLongueur)
      let reste = calculLongueur(nbrePlancheData.nbrePlancheLongueur % 1, false)

      // if (entier >= 1 && reste >= 0.5) {
      //   reste = 1
      // }
      let total = entier + reste
      setnbrePlancheData({
        ...nbrePlancheData,
        nbrePlancheLongueur: total,
        nbrePlanche: totalPlanche(total * nbrePlancheData.nbrePlancheLargeur)
      })

      let long

      if (formData.fixe === true) {
        long = onedemande.data.longueur
      } else {
        long =
          entier * formData.mat[0].longueur +
          (Math.ceil(total) - 1) * formData.ecart +
          (formData.mat[0].longueur - calculdecoupe(reste) * 0.24) * reste
      }
      let decoupe = roundDecimal(
        (formData.mat[0].longueur - calculdecoupe(reste) * 0.24) * (total % 1)
      )
      onedemande.data.longueur = long

      setFormData({
        ...formData,
        dimdecoupe: decoupe,
        ecartLamb: long / (formData.nbrelamb - 1)
      })
    }
  }

  function calculDevis() {
    {
      compteur.compteur = 0
      ///////////////////////////////////////////////////////
      //////// Calcul nombre de rangée de lambourdes ////////
      let nbrerangee
      if (onedemande.data.typemat === 'bangkiraiL') {
        nbrerangee = onedemande.data.longueur / 40 + 1
      } else {
        nbrerangee =
          (onedemande.data.longueur / 45) % 1 > 0.3
            ? Math.ceil(onedemande.data.longueur / 45) + 1
            : Math.ceil(onedemande.data.longueur / 45)
      }

      ///////////////////////////????????????//////////////////////////////

      // let temp = nbrePlancheData.nbrePlancheLongueur
      // if (formData.fixe) {
      //   temp = onedemande.data.longueur / formData.typestruct[0].longueur
      // }

      // if (temp > 1) {
      //   if (temp % 1 === 0) {
      //     nbrerangee = nbrerangee + temp - 1
      //   } else {
      //     nbrerangee = nbrerangee + Math.ceil(temp)
      //   }
      // }
      //////////////////////////////////////////////////////////////////////////
      ///////////////// Calcul nombre de lambourde à acheter //////////////////
      //////////////////////////////////////////////////////////////////////////

      let nbrelambparrangee =
        onedemande.data.largeur / formData.typestruct[0].longueur
      if (nbrelambparrangee === 0) {
      }
      let entier = Math.floor(
        onedemande.data.largeur / formData.typestruct[0].longueur
      )
      let reste = calculLamb(
        (onedemande.data.largeur / formData.typestruct[0].longueur) % 1
      )

      let rangeesup =
        nbrePlancheData.nbrePlancheLongueur > 1
          ? nbrePlancheData.nbrePlancheLongueur >
            Math.floor(nbrePlancheData.nbrePlancheLongueur)
            ? Math.floor(nbrePlancheData.nbrePlancheLongueur)
            : Math.floor(nbrePlancheData.nbrePlancheLongueur) - 1
          : 0
      let longueurTotalLamb = (nbrerangee + rangeesup) * (entier + reste)
      // ((onedemande.data.largeur * nbrerangee) / price.data[onedemande.data.typestruct][0]) % 1 >=
      // 0.11
      //   ? Math.ceil(
      //       (onedemande.data.largeur * nbrerangee) / price.data[onedemande.data.typestruct][0]
      //     )
      //   : (onedemande.data.largeur * nbrerangee) / price.data[onedemande.data.typestruct][0]

      /////////////////////////////////////////////////////////////
      ////////// Calcul nombre de vis / clips et prix  ////////////
      /////////////////////////////////////////////////////////////

      let [nbreViss, nbreBoite, prixBoite] = calculsVis(nbrerangee)
      let [nbreplot, ecartplot] = calculPlot(nbrerangee)
      let ecartlamb = roundDecimal(onedemande.data.longueur / (nbrerangee - 1))

      setFormData({
        ...formData,
        nbrelamb: nbrerangee,
        nbreVis: nbreViss,
        ecartPlot: ecartplot,
        ecartLamb: ecartlamb,
        dateDeb: onedemande.data.datedeb,
        dateFin: onedemande.data.datefin,
        titre:
          onedemande.data.type +
          ' ' +
          roundDecimal(onedemande.data.longueur) +
          ' x ' +
          roundDecimal(onedemande.data.largeur) +
          'cm(' +
          roundDecimal(
            (onedemande.data.largeur * onedemande.data.longueur) / 10000
          ) +
          ' m²)'
      })

      //////////////////////////////////////////////////////////////
      ////////////////// Calcul et affichage devis//////////////////
      //////////////////////////////////////////////////////////////

      let list
      list = []

      list[compteur.compteur] = [
        'Planche ' +
          onedemande.data.typemat +
          ' (' +
          formData.mat[0].longueur +
          'cm)',
        formData.mat[0].prixTvac,
        Math.ceil(nbrePlancheData.nbrePlanche),

        roundDecimal(
          formData.mat[0].prixTvac * Math.ceil(nbrePlancheData.nbrePlanche)
        )
      ]
      compteur.compteur = compteur.compteur + 1

      list[compteur.compteur] = [
        'Lambourde ' +
          onedemande.data.typestruct +
          ' (' +
          formData.typestruct[0].longueur +
          'cm)',
        formData.typestruct[0].prixTvac,
        Math.ceil(longueurTotalLamb),

        roundDecimal(
          formData.typestruct[0].prixTvac * Math.ceil(longueurTotalLamb)
        )
      ]
      compteur.compteur = compteur.compteur + 1

      list[compteur.compteur] = [
        'Vis',
        prixBoite,
        nbreBoite,

        roundDecimal(nbreBoite * prixBoite)
      ]
      if (formData.plot) {
        compteur.compteur = compteur.compteur + 1
        list[compteur.compteur] = [
          'Plot',
          2.5,
          roundDecimal(nbreplot),
          2.5 * nbreplot
        ]
      }
      if (formData.dalle) {
        compteur.compteur = compteur.compteur + 1
        list[compteur.compteur] = [
          'Dalle',
          0.99,
          roundDecimal(nbreplot),

          roundDecimal(0.99 * nbreplot)
        ]
      }

      compteur.compteur = compteur.compteur + 1
      list[compteur.compteur] = ['Livraison', '', '', 69]

      if (
        formData.contour &&
        (onedemande.data.typemat.localeCompare('compositeb') === 0 ||
          onedemande.data.typemat.localeCompare('compositea') === 0)
      ) {
        compteur.compteur = compteur.compteur + 1

        let cont = prices.filter((price) => price.name === 'contour')

        let nbrefinition = Math.ceil(
          (Number(onedemande.data.longueur) + Number(onedemande.data.largeur)) /
            cont[0].nombre
        )

        let prixfinition =
          formData.contour &&
          onedemande.data.typemat.localeCompare(
            'compositeb' || 'compositea'
          ) === 0
            ? roundDecimal(cont[0].prixTvac * nbrefinition)
            : 0

        list[compteur.compteur] = [
          'Finition',
          cont[0].prixTvac,
          nbrefinition,

          prixfinition
        ]
      }

      setdevisligneData(list)
      let totalHtva = 0
      for (let i = 0; i < list.length; i++) {
        totalHtva += list[i][3]
      }
      totalList.totalHtva = roundDecimal(totalHtva)
      totalList.totalTvac = roundDecimal(totalHtva * (1 + totalList.tva / 100))
      totalList.tvaAmount = roundDecimal(totalHtva * (totalList.tva / 100))
    }

    var x = document.getElementById('showdevis')
    if (x.style.display === 'none') {
      x.style.display = 'block'
    }
    var x2 = document.getElementById('showcalcul')
    var dev = document.getElementById('showdevis1')

    if (x2.style.display === 'block') {
      x2.style.display = 'none'
    }
    if (dev.style.display === 'block') {
      dev.style.display = 'none'
    }
  }

  function prepare(sended) {
    return {
      titre: formData.titre,
      user: onedemande.user,
      demande: onedemande._id,
      numero: formData.numero,
      lineAuto: devisligneData,
      lineExtra: inputList,
      total: totalList,
      totalHtva: totalList.totalHtva,
      totalTvac: totalList.totalTvac,
      tvaAmount: totalList.tvaAmount,
      tva: totalList.tva,
      extra: {
        decoupe: formData.dimdecoupe,
        ecartLamb: formData.ecartLamb,
        ecartPlot: formData.ecartPlot,
        nbreLamb: formData.nbrelamb,
        dateDeb: formData.dateDeb,
        dateFin: formData.dateFin
      },
      send: sended,
      adresse: formData.adresse
    }
  }
  const save = async (e) => {
    const newErrors = findFormErrors('send')
    // Conditional logic:
    if (Object.keys(newErrors).length > 0) {
      // We got errors!
      setErrors(newErrors)
    } else {
      uploadDevis(prepare(false)).then((res) =>
        document.getElementById('next').click()
      )
    }
  }

  const send = async (e) => {
    const newErrors = findFormErrors('send')
    // Conditional logic:
    if (Object.keys(newErrors).length > 0) {
      // We got errors!
      setErrors(newErrors)
    } else {
      uploadDevis(prepare(true)).then((res) =>
        document.getElementById('next').click()
      )
    }
  }
  return (
    <Fragment>
      {loading ? (
        <Spinner />
      ) : (
        onedemande && (
          <Fragment>
            <div>
              <div>
                <Link
                  id="next"
                  to="/allDevis"
                  className="demdevisbutton2"
                  style={{ marginTop: '20px' }}
                >
                  Retour
                </Link>
              </div>
              <button
                onClick={() => document.location.reload()}
                style={{ marginTop: '20px' }}
              >
                Refresh
              </button>
              <div id="showcalcul" style={{ display: 'block' }}>
                <div>
                  {formData.numero}:{onedemande.data.type}
                  {''}
                  <div>
                    <Form.Control
                      as="select"
                      name="typemat"
                      onChange={onChange}
                      isInvalid={!!errors.typemat}
                      style={{ width: '220px' }}
                      defaultValue={onedemande.data.typemat}
                    >
                      <option value=""></option>

                      <option value="premprix">premprix (12.5€/m²)</option>
                      <option value="meleze">Mélèze (24.6€/m²)</option>
                      <option value="compositeb">
                        Composite brun (41.99€/m²)
                      </option>
                      <option name="compositea" value="compositea">
                        Composite anthracite (41.99€/m²)
                      </option>
                      <option value="pinthermo">Pin thermo (44.3€/m²)</option>
                      <option value="bangkiraiL">
                        Bangkirai lisse (+-54.5€/m²)
                      </option>

                      <option value="bangkirai">
                        Bangkirai rainuré (+-65.5€/m²)
                      </option>
                    </Form.Control>
                    <Form.Control.Feedback type="invalid">
                      {errors.typemat}
                    </Form.Control.Feedback>
                    <Form.Control
                      as="select"
                      name="typestruct"
                      value={onedemande.data.typestruct}
                      onChange={onChange}
                      isInvalid={!!errors.typestruct}
                      style={{ width: '220px' }}
                    >
                      <option value=""></option>
                      <option value="lambtraite">
                        Traité autoclave (2.44€/m)
                      </option>
                      <option value="lambcomp">Composite (4.55€/m)</option>
                      <option value="lambazobe">Azobe (5€/m)</option>
                      <option value="lambalu">Aluminium (6.25€/m)</option>
                    </Form.Control>
                    <Form.Control.Feedback type="invalid">
                      {errors.typestruct}
                    </Form.Control.Feedback>
                  </div>
                </div>
                <div>
                  <Form.Check
                    type="checkbox"
                    label="mesures fixe"
                    name="fixe"
                    value={formData.fixe}
                    onChange={onChange2}
                  />
                  {/* <button onClick={() => init()}> initialise</button> */}
                </div>
                <button onClick={() => init()} id="validbutton">
                  Calcul Planche
                </button>
              </div>
              <div id="showdevis1" style={{ display: 'none' }}>
                <div>
                  {onedemande.data.photo &&
                    Object.values(onedemande.data.photo).map((test, key) => (
                      <div
                        style={{
                          paddingLeft: '10px',
                          width: '280px',
                          marginTop: '10px',
                          border: '1px solid #66cc00'
                        }}
                        key={key}
                      >
                        <img
                          style={{ width: '250px' }}
                          src={require('../../../upload/photos/' + test ||
                            '../../../upload/photos/error.png')}
                          alt="..."
                        />
                      </div>
                    ))}
                </div>

                <Table striped bordered hover responsive>
                  <thead>
                    <tr>
                      <th>L</th>
                      <th>l</th>
                      <th>m²</th>
                      {/* <th>Link</th> */}
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        <Form.Row>
                          {roundDecimal(onedemande.data.longueur)}
                        </Form.Row>
                        <Form.Row>
                          <div>
                            {/* {Math.floor(nbrePlancheData.nbrePlancheLongueur * 100) /
                      100} */}
                            {roundDecimal(nbrePlancheData.nbrePlancheLongueur)}
                            planches
                          </div>
                          <div>
                            <button onClick={() => roundFloor(false)}>
                              down
                            </button>
                            <button onClick={() => roundCeil(false)}>up</button>
                          </div>
                        </Form.Row>
                      </td>
                      <td>
                        <Form.Row>{onedemande.data.largeur}</Form.Row>
                        <Form.Row>
                          <div>
                            {roundDecimal(nbrePlancheData.nbrePlancheLargeur)}
                            planches
                          </div>
                          <div>
                            <button onClick={() => roundFloor(true)}>
                              down
                            </button>
                            <button onClick={() => roundCeil(true)}>up</button>
                          </div>
                        </Form.Row>
                      </td>
                      <td>
                        {roundDecimal(
                          (onedemande.data.largeur * onedemande.data.longueur) /
                            10000
                        )}
                      </td>
                    </tr>
                  </tbody>
                </Table>
                <Table striped bordered hover responsive>
                  <thead>
                    <tr>
                      <th>Hauteur</th>
                      <th>Type Sol</th>
                      <th>Détails</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>{onedemande.data.hauteur}</td>
                      <td>{onedemande.data.typesol}</td>
                      <td>
                        {onedemande.data.details &&
                          Object.entries(onedemande.data.details).map(
                            ([key, value]) => (
                              //  if({lang}==code)
                              <div>
                                {key} : {value === true ? 'true' : value}
                              </div>
                            )
                          )}
                      </td>
                    </tr>
                  </tbody>
                </Table>
                <div>ecart planche= {formData.ecart} </div>
                <div>
                  total ={roundDecimal(nbrePlancheData.nbrePlanche)} planches{' '}
                </div>
                <Form.Check
                  type="checkbox"
                  label="Plot?"
                  name="plot"
                  value={formData.plot}
                  onChange={onChange2}
                />
                <Form.Check
                  type="checkbox"
                  label="Dalle?"
                  name="dalle"
                  value={formData.dalle}
                  onChange={onChange2}
                />
                <Form.Check
                  type="checkbox"
                  label="Contour?"
                  name="contour"
                  value={formData.contour}
                  onChange={onChange2}
                />
                <button
                  onClick={() => {
                    calculDevis()
                  }}
                  id="validbutton"
                >
                  Calcul Devis
                </button>
              </div>
              <div id="showdevis" style={{ display: 'none' }}>
                <h5>Infos chantier</h5>
                <div>Numéro de devis {formData.numero}</div>
                <div
                  style={{
                    paddingLeft: '10px',
                    width: '300px',
                    marginTop: '10px',
                    border: '1px solid #66cc00'
                  }}
                >
                  <div>Découpe de {formData.dimdecoupe}cm</div>
                  <div>écart entre lambourdes = {formData.ecartLamb}</div>
                  <div>ecartPlot : {formData.ecartPlot}</div>
                  <div>nombre rangée de lambourdes : {formData.nbrelamb}</div>
                  <div>nombre de vis : {formData.nbreVis}</div>
                </div>

                <div>
                  <div>
                    date début :{' '}
                    <Form.Control
                      style={{ width: '160px' }}
                      type="date"
                      name="dateDeb"
                      defaultValue={formData.dateDeb}
                      onChange={onChange}
                      isInvalid={!!errors.dateDeb}
                    ></Form.Control>
                    <Form.Control.Feedback type="invalid">
                      {errors.dateDeb}
                    </Form.Control.Feedback>
                  </div>

                  <div>
                    date fin :{' '}
                    <Form.Control
                      style={{ width: '160px' }}
                      type="date"
                      name="dateFin"
                      defaultValue={formData.dateFin}
                      onChange={onChange}
                      isInvalid={!!errors.dateFin}
                    ></Form.Control>
                    <Form.Control.Feedback type="invalid">
                      {errors.dateFin}
                    </Form.Control.Feedback>
                  </div>
                  <div>
                    adresse:{' '}
                    <input
                      style={{ width: '350px' }}
                      name="adresse"
                      defaultValue={formData.adresse}
                      onChange={onChange}
                    />
                  </div>
                  <div>Tva de :</div>
                  <div>
                    <Form.Control
                      as="select"
                      name="tva"
                      value={totalList.tva}
                      onChange={onChange3}
                      style={{ width: '170px' }}
                    >
                      <option value={21}>21%</option>
                      <option value={6}>6%</option>
                    </Form.Control>
                  </div>
                </div>
                <div id="deviss">
                  <div className="tabledevis">
                    <Table
                      // hover
                      bordered
                      // id="tabledevis"
                      // style={{ border: '1px solid #ccc', width: '650px' }}
                    >
                      <thead className="testing">
                        <tr>
                          <th>Désignation</th>
                          <th>Prix unitaire</th>
                          <th>Quantité</th>

                          <th>Total</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td colSpan="4">{formData.titre}</td>
                        </tr>
                        {Object.entries(devisligneData).map(([key, value]) => (
                          <tr key={value}>
                            {devisligneData[key].map((x) => (
                              <td key={x}>{x} </td>
                            ))}
                          </tr>
                        ))}
                        {Object.entries(inputList).map(([key, value]) => (
                          <tr key={value}>
                            {inputList[key].map((x) => (
                              <td key={x}>{x} </td>
                            ))}
                          </tr>
                        ))}
                        <tr>
                          <td colSpan="3">Total Htva</td>
                          <td>{totalList.totalHtva} </td>
                        </tr>
                        <tr>
                          <td colSpan="3">Tva {totalList.tva} %</td>
                          <td>{totalList.tvaAmount} </td>
                        </tr>
                        <tr>
                          <td colSpan="3">Total Tvac</td>
                          <td>{totalList.totalTvac}</td>
                        </tr>
                      </tbody>
                    </Table>
                  </div>
                </div>
                <h5 style={{ marginTop: '10px' }}>Rajout ligne Devis</h5>
                {inputList.map((x, i) => {
                  return (
                    <div key={i}>
                      <Form.Row style={{ width: '704px' }}>
                        <Col lg={4}>
                          <Form.Control
                            className="createdevisinputdes"
                            name="0"
                            value={x[0]}
                            placeholder="Désignation"
                            onChange={(e) => handleInputChange(e, i)}
                            isInvalid={!!errors[i.toString() + '0']}
                          />
                          <Form.Control.Feedback type="invalid">
                            {errors[i.toString() + '0']}
                          </Form.Control.Feedback>
                        </Col>
                        <Col lg={2}>
                          <Form.Control
                            className="createdevisinput"
                            type="number"
                            min="0"
                            name="1"
                            value={x[1]}
                            placeholder="Prix Unit"
                            onChange={(e) => handleInputChange(e, i)}
                            isInvalid={!!errors[i.toString() + '1']}
                          />
                          <Form.Control.Feedback type="invalid">
                            {errors[i.toString() + '1']}
                          </Form.Control.Feedback>
                        </Col>
                        <Col lg={2}>
                          <Form.Control
                            className="createdevisinput"
                            type="number"
                            min="0"
                            name="2"
                            value={x[2]}
                            placeholder="Quant"
                            onChange={(e) => handleInputChange(e, i)}
                            isInvalid={!!errors[i.toString() + '2']}
                          />
                          <Form.Control.Feedback type="invalid">
                            {errors[i.toString() + '2']}
                          </Form.Control.Feedback>
                        </Col>
                        <Col lg={2}>
                          <Form.Control
                            className="createdevisinput"
                            type="number"
                            min="0"
                            name="3"
                            value={x[3]}
                            placeholder="Total"
                            onChange={(e) => handleInputChange(e, i)}
                            isInvalid={!!errors[i.toString() + '3']}
                          />
                          <Form.Control.Feedback type="invalid">
                            {errors[i.toString() + '3']}
                          </Form.Control.Feedback>
                        </Col>
                        {inputList.length !== 1 && i > 0 && (
                          <Col lg={1}>
                            <button
                              className="mr10"
                              onClick={() => handleRemoveClick(i)}
                            >
                              Supprimer
                            </button>
                          </Col>
                        )}
                      </Form.Row>
                    </div>
                  )
                })}
                <button
                  onClick={() => handleAddClick()}
                  className="demdevisbutton2"
                >
                  Nouvelle Ligne
                </button>
                <div>
                  <button onClick={capture}>Télécharger</button>
                </div>
                <div>
                  <Button id="validbutton" onClick={save}>
                    Sauvegarder
                  </Button>
                  <Button id="validbutton" onClick={send}>
                    Envoyer
                  </Button>
                </div>
              </div>

              <div id="endpage"></div>
            </div>
          </Fragment>
        )
      )}
    </Fragment>
  )
}

DevisTerrasseBois.propTypes = {
  getDemandeById: PropTypes.func.isRequired,
  getAPrice: PropTypes.func.isRequired,
  uploadDevis: PropTypes.func.isRequired,
  demande: PropTypes.object.isRequired,
  price: PropTypes.object.isRequired,
  devis: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  demande: state.demande,
  price: state.price,
  devis: state.devis
})

export default connect(mapStateToProps, {
  getDemandeById,
  getAPrice,
  uploadDevis
})(DevisTerrasseBois)
