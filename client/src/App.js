import React, { Fragment, useEffect } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Navbar from './components/layout/Navbar'
import Landing from './components/layout/Landing'
import Callbutton from './components/layout/Callbutton'
import Login from './components/auth/Login'
import Alert from './components/layout/Alert'
import Dashboard from './components/dashboard/Dashboard'
import Compta from './components/dashboard/Compta'
import Realisation from './components/vitrine/Realisation'
import ConditionGenerale from './components/vitrine/ConditionGenerale'

// import Calendar from './components/calendar/Calendar'

import AllDevis from './components/devis/allDevis'
import MesDevis from './components/devis/DevisFacture'

import AllFacture from './components/facture/AllFacture'
import Facture from './components/facture/Facture'
import Devis from './components/devis/Devis'
import CreationFacture from './components/facture/CreationFacture'
import CreationFactureCustom from './components/facture/CreationFactureCustom'
import CreationDevis from './components/devis/creation/CreationDevis'
import ListePrix from './components/liste/ListePrix'
import AllChantier from './components/chantier/AllChantier'
import Chantier from './components/chantier/Chantier'
import DevisTerrassebois from './components/devis/creation/devisTerrassebois.js'
import DemandeDevis2 from './components/devis/creation/DemandeDevis2.js'
import DevisCloture from './components/devis/creation/DevisCloture'
import PrivateRoute from './components/routing/PrivateRoute'
import AdminRoute from './components/routing/AdminRoute'
import { loadUser } from './store/actions/auth'
import setAuthToken from './outils/setAuthToken'
import NotFound from './components/layout/NotFound'
import 'bootstrap/dist/css/bootstrap.min.css'
import Email from './components/vitrine/Email'
//redux
import { Provider } from 'react-redux'
import store from './store/store'

import './App.css'

const App = () => {
  useEffect(() => {
    setAuthToken(localStorage.token)
    store.dispatch(loadUser())
  }, [])
  return (
    <Provider store={store}>
      <Router>
        <Fragment>
          <Navbar />
          <section className=" contain">
            <Route exact path="/" component={Landing} />
          </section>
          <Callbutton />
          <Alert />
          <Email />
          <section className=" container1">
            <Switch>
              <Route exact path="/realisation" component={Realisation} />
              <Route exact path="/DemandeDevis" component={DemandeDevis2} />
              {/* <Route exact path="/register" component={Register} /> */}
              <Route exact path="/login" component={Login} />

              <PrivateRoute
                exact
                path="/conditiongenerale"
                component={ConditionGenerale}
              />
              <PrivateRoute exact path="/devis/:id" component={Devis} />
              <PrivateRoute exact path="/devisfacture" component={MesDevis} />
              <PrivateRoute exact path="/facture/:id" component={Facture} />
              <AdminRoute
                exact
                path="/creationfacture/:id"
                component={CreationFacture}
              />
              <AdminRoute
                exact
                path="/creationfacturecustom"
                component={CreationFactureCustom}
              />
              <AdminRoute exact path="/allfacture" component={AllFacture} />

              <AdminRoute exact path="/alldevis" component={AllDevis} />
              <AdminRoute exact path="/chantiers" component={AllChantier} />
              <AdminRoute exact path="/chantier/:id" component={Chantier} />
              <AdminRoute
                exact
                path="/devisterrassebois/:id"
                component={DevisTerrassebois}
              />
              <AdminRoute
                exact
                path="/deviscloture/:id"
                component={DevisCloture}
              />
              <AdminRoute exact path="/listeprix" component={ListePrix} />
              <AdminRoute
                exact
                path="/creationdevis"
                component={CreationDevis}
              />

              <AdminRoute exact path="/dashboard" component={Dashboard} />
              <AdminRoute exact path="/compta" component={Compta} />
              <Route component={NotFound} />
            </Switch>
          </section>
        </Fragment>
      </Router>
    </Provider>
  )
}

export default App
