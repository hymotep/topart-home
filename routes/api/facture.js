const express = require('express')
const router = express.Router()
const Facture = require('../../models/Facture.js')
const Compta = require('../../models/Compta.js')
const checkObjectId = require('../../middleware/checkObjectId')
const { body } = require('express-validator')

const admin = require('../../middleware/admin.js')
const auth = require('../../middleware/auth.js')
const { check, validationResult } = require('express-validator')

function roundDecimal(number) {
  return Math.floor(number * 100) / 100
}

//renvoie les facture par chantier
router.get('/chantier/:id', [admin, checkObjectId('id')], async (req, res) => {
  try {
    const factures = await Facture.find({ chantier: req.params.id })
    res.json(factures)
  } catch (err) {
    console.error(err.message)
    res.status(500).send('Server Error')
  }
})
//renvoie toute les factures
router.get('/', admin, async (req, res) => {
  try {
    const factures = await Facture.find().populate('user', [
      'email',
      'name',
      'adresse'
    ])
    res.json(factures)
  } catch (err) {
    console.error(err.message)
    res.status(500).send('Server Error')
  }
})
//renvoie toute les factures par user
router.get('/user/:id', [auth, checkObjectId('id')], async (req, res) => {
  try {
    const factures = await Facture.find({ user: req.params.id })
    res.json(factures)
  } catch (err) {
    console.error(err.message)
    res.status(500).send('Server Error')
  }
})

//Get one facture
router.get('/:id', [auth, checkObjectId('id')], async (req, res) => {
  try {
    const facture = await Facture.findById(req.params.id)
      .populate('user', ['email', 'name', 'adresse'])
      .populate('chantier', ['dateDeb'])

    res.json(facture)
  } catch (err) {
    console.error(err.message)

    res.status(500).send('Server Error')
  }
})

//enregistrer facture et mettre à jour le reste à facturer
router.post('/', admin, async (req, res) => {
  const d = new Date().getFullYear().toString()
  try {
    const numeroFacture = await Counters.findOneAndUpdate(
      { _id: 'numfacture' },
      { $inc: { sequence_value: 1 } }
    )
    let data = req.body
    facture = new Facture({
      titre: data.titre,
      numero: d + numeroFacture.sequence_value.toString(),
      user: data.user,
      chantier: data.chantier,
      line: data.line,

      totalHtva: data.totalHtva,
      totalTvac: data.totalTvac,
      tvaAmount: data.tvaAmount,
      tva: data.tva,

      dateLivraison: data.dateLivraison,
      date: data.date,
      dateEcheance: data.dateEcheance,
      adresse: data.adresse,
      payed: false
    })
    let chantier = await Chantier.findById(data.chantier).populate('user', [
      'email',
      'name',
      'adresse'
    ])
    chantier.line = data.oldline

    chantier.total = data.oldtotal
    chantier.stillToBill = chantier.total.totalTvac
    await facture.save()

    await chantier.save()

    res.json(chantier)
  } catch (err) {
    console.error(err.message)
    res.status(500).send('Server Error')
  }
})

//crée facture  personnalisée
router.post('/custom/', admin, async (req, res) => {
  const d = new Date().getFullYear().toString()
  try {
    const data = req.body
    let temp = await User.findOne({ email: data.user.email })

    const numeroFacture = await Counters.findOneAndUpdate(
      { _id: 'numfacture' },
      { $inc: { sequence_value: 1 } }
    )
    facture = new Facture({
      titre: data.titre,
      user: temp._id,
      numero: d + numeroFacture.sequence_value.toString(),
      line: data.line,

      totalHtva: data.totalHtva,
      totalTvac: data.totalTvac,
      tvaAmount: data.tvaAmount,
      tva: data.tva,

      dateLivraison: data.dateLivraison,
      date: data.date,
      dateEcheance: data.dateEcheance,
      adresse: data.adresse,
      payed: false
    })
    const fact = await facture.save()
    res.json(fact)
  } catch (err) {
    console.error(err.message)
    res.status(500).send('Server error')
  }
})

//valider/annuler le paiement d'une facture
router.put('/', admin, async (req, res) => {
  try {
    let fact = await Facture.findOne({ _id: req.body.id })
    let chant

    fact.payed = req.body.payed
    if (fact.chantier) {
      //si la facture n'est pas une facture custom
      chant = await Chantier.findOne({ _id: fact.chantier })
      if (req.body.payed) {
        //selon l'état actuel déduis ou rajouter le montant
        chant.stillToPay = roundDecimal(chant.stillToPay - fact.totalTvac) //de la facture du montant à facturer du chantier
      } else {
        chant.stillToPay = roundDecimal(chant.stillToPay + fact.totalTvac)
      }
      chant.stillToPay = chant.stillToPay < 0.1 ? 0 : chant.stillToPay //dans certain cas le programme affichait qques centimes remise à zero du montantà payé dans ce cas .

      await chant.save()
    }
    let date
    if (fact.chantier) {
      date = fact.date.split('/')[2]
    } else {
      date = fact.date.split('-')[0]
    }
    let compta = await Compta.findOne({ year: date })
    if (!compta) {
      compta = new Compta({ year: date }) //prend la compta de cette année
    }
    if (req.body.payed) {
      // en fonction modifie la compta de cette année qui totalise les gains
      compta.totalHtva = roundDecimal(compta.totalHtva + fact.totalHtva)
      compta.totalTvac = roundDecimal(compta.totalTvac + fact.totalTvac)
      compta.tvaAmount = roundDecimal(compta.tvaAmount + fact.tvaAmount)
    } else {
      compta.totalHtva = roundDecimal(compta.totalHtva - fact.totalHtva)
      compta.totalTvac = roundDecimal(compta.totalTvac - fact.totalTvac)
      compta.tvaAmount = roundDecimal(compta.tvaAmount - fact.tvaAmount)
    }
    await compta.save()

    await fact.save()
    fact = await Facture.findById(fact._id).populate('chantier')
    res.json(fact)
  } catch (err) {
    console.error(err.message)
    res.status(500).send('Server error')
  }
})

module.exports = router
