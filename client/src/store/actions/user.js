import { GET_USERS, USER_ERROR } from './types'
import api from '../../outils/api'

//Get all devis
export const getAllUsers = () => async (dispatch) => {
  try {
    const res = await api.get('/users')

    dispatch({
      type: GET_USERS,
      payload: res.data
    })
  } catch (err) {
    dispatch({
      type: USER_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    })
  }
}

export const testmail = (email) => async (dispatch) => {
  try {
    const res = await api.get(`/users/exist/${email}`)
    return res.data
  } catch (err) {
    dispatch({
      type: USER_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    })
  }
}
