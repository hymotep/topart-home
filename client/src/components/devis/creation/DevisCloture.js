import React, { useEffect, createRef } from 'react'
import PropTypes from 'prop-types'

import { connect } from 'react-redux'
import { getDemandeById } from '../../../store/actions/demande.js'
import { Table, Form, Col } from 'react-bootstrap'

import '../../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css'

import { Link } from 'react-router-dom'
import { tbois } from '../../liste/Prix.js'

import logo from '../../img/toparthomelogodevis.png'
const CreateDevis = ({ getDemandeById, demande: { onedemande }, match }) => {
  ///////////////prendre photo////////////////

  const ref = createRef(null)

  // const download = (image, { name = 'img', extension = 'jpg' } = {}) => {
  //   const a = document.createElement('a')
  //   a.href = image
  //   a.download = createFileName(extension, name)
  //   a.click()
  // }

  // function capture() {
  //   html2canvas(document.querySelector('#deviss'), {
  //     scrollY: -window.scrollY,
  //     scrollX: -window.scrollX
  //   }).then((canvas) => {
  //     document.body.appendChild(canvas) // if you want see your screenshot in body.
  //     const imgData = canvas.toDataURL('image/png')
  //     const pdf = new jsPDF()
  //     pdf.addImage(imgData, 'PNG', 0, 0)
  //     pdf.save(formData.numero + '.pdf')
  //   })
  // }

  // const downloadScreenshot = () => takeScreenShot(ref.current).then(download)

  /////////////////////////////////////////////////

  let data = {
    date: '',
    numero: '',
    longueur: '',
    largeur: '',
    typemat: '',
    typestruct: '',
    ecart: 0,
    compositebool: '',
    nbrelamb: 0,
    nbreTotalLamb: 0,
    test: '',
    fixe: false,
    plot: false,
    contour: false,
    nbreVis: 0
  }
  // let planche = {
  //   nbrePlancheLongueur: '',
  //   nbrePlancheLargeur: '',
  //   nbrePlanche: 0
  // }

  // let ecart
  // let compositebool

  let [formData, setFormData] = React.useState(data)
  // let [nbrePlancheData, setnbrePlancheData] = React.useState(planche)
  // let [devisligneData, setdevisligneData] = React.useState({})

  useEffect(() => {
    getDemandeById(match.params.id)
  }, [getDemandeById, match.params.id])

  // function calculContour(longueur, largeur) {
  //   return formData.contour
  //     ? (longueur + largeur * 2) / tbois[onedemande.data.typemat][0]
  //     : 0
  // }

  function init() {
    switch (onedemande.data.type) {
      case 'Terrasse bois': {
        break
      }
      default: {
      }
    }
  }

  let onChange2 = (e) => {
    // setformdata({ ...formdata, [e.target.name]: e.target.checked })
    setFormData({ ...formData, [e.target.name]: e.target.checked })
  }

  return (
    onedemande && (
      <div>
        <div>
          <Link to="/allDevis">Retour</Link>
        </div>
        <div>
          <Form.Check
            type="checkbox"
            label="Plot?"
            Name="plot"
            value={formData.plot}
            onChange={onChange2}
          />
          <Form.Check
            type="checkbox"
            label="Contour?"
            Name="contour"
            value={formData.contour}
            onChange={onChange2}
          />
          <button onClick={() => init()}> initialise</button>
          <Form.Check
            type="checkbox"
            label="mesures fixe"
            Name="fixe"
            value={formData.fixe}
            onChange={onChange2}
          />
        </div>
        <div>
          {formData.numero}:{onedemande.data.type}
          <div>
            {onedemande.data.typemat} ({tbois[onedemande.data.typemat][0]}cm)-
            {onedemande.data.typestruct}
          </div>
        </div>

        <Table striped bordered hover responsive>
          <thead>
            <tr>
              <th>Hauteur</th>
              <th>Type Sol</th>
              <th>Détails</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>{onedemande.data.hauteur}</td>
              <td>{onedemande.data.typesol}</td>
              <td>
                {onedemande.data.details &&
                  Object.entries(onedemande.data.details).map(
                    ([key, value]) => (
                      //  if({lang}==code)
                      <div>
                        {key} : {value === true ? 'true' : value}
                      </div>
                    )
                  )}
              </td>
            </tr>
          </tbody>
        </Table>

        <div ref={ref} id="deviss" style={{}}>
          <Form.Row
            style={{
              paddingTop: '50px',
              paddingBottom: '20px',
              width: '650px'
            }}
          >
            <Col>
              <img
                style={{
                  width: '270px',
                  height: '170px'
                }}
                className="photoholder"
                src={logo}
                alt="..."
              />
            </Col>
            <Col>
              {/* <p>Numero de Devis :{formData.numero}</p> */}
              <p>Date : {formData.date}</p>
              <p>Client :{onedemande.data.user.name} </p>
            </Col>
          </Form.Row>
        </div>
      </div>
    )
  )
}

CreateDevis.propTypes = {
  getDemandeById: PropTypes.func.isRequired,
  demande: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  demande: state.demande
})

export default connect(mapStateToProps, { getDemandeById })(CreateDevis)
