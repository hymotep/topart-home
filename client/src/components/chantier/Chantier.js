import React, { useEffect, Fragment } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import {
  getChantierById,
  updateChantier
} from '../../store/actions/chantier.js'
import {
  getFactureByChantier,
  factureIsPayed
} from '../../store/actions/facture.js'
import Spinner from '../layout/Spinner'
import { Table } from 'react-bootstrap'

const Chantier = ({
  chantier: { loading, chantier },
  facture: { factures },
  getChantierById,
  updateChantier,
  getFactureByChantier,
  factureIsPayed,
  match
}) => {
  useEffect(() => {
    getChantierById(match.params.id)
  }, [])
  useEffect(() => {
    getFactureByChantier(match.params.id)
  }, [])
  let initialForm = { commentaire: ' ', tools: [] }
  let [formData, setformData] = React.useState(initialForm)

  const onChange2 = (e) => {
    chantier.commentaire = e.target.value
    setformData({ ...formData, [e.target.name]: e.target.value })
  }
  const save = async (e) => {
    let form = {
      id: chantier._id,
      commentaire: chantier.commentaire
    }
    updateChantier(form)
  }
  function pay(id, payed) {
    let forms = { id: id, payed: !payed }
    factureIsPayed(forms)
  }

  return (
    <Fragment>
      {loading ? (
        <Spinner />
      ) : (
        chantier &&
        factures && (
          <div>
            <button
              onClick={() => window.history.back()}
              className="demdevisbutton2"
            >
              Retour
            </button>
            <div>
              <Link
                id="linkresize"
                to={`/devis/${chantier.devis}`}
                className="btn btn-primary btnlist"
              >
                Vers Devis
              </Link>
            </div>

            <h5>Infos chantier</h5>
            <div
              style={{
                paddingLeft: '10px',
                width: '300px',
                marginTop: '10px',
                border: '1px solid #66cc00'
              }}
            >
              {Object.entries(chantier.extra).map((value, key) => (
                <div>
                  {value.map((key2, val) => (
                    <a>
                      {key2}
                      {': '}
                    </a>
                  ))}
                </div>
              ))}
            </div>

            <div>
              Date Début :
              <input type="date" value={chantier.dateDeb} disabled />
            </div>
            <div>
              Date Fin : <input type="date" value={chantier.dateFin} disabled />
            </div>
            <div>Reste à payer:{chantier.stillToPay} </div>
            <div>Reste à facturer:{chantier.stillToBill} </div>
            <div>Adresse :{chantier.adresse} </div>
            <div>Commentaire :</div>
            <div>
              <textarea
                id="commentaire"
                name="commentaire"
                style={{ height: '88px' }}
                value={chantier.commentaire}
                rows="5"
                onChange={onChange2}
                placeholder="Indiquer toutes informations que vous jugez utiles"
              />

              {/* <input
                type="text"
                Name="commentaire"
                value={formData.commentaire}
                onChange={onChange2()}
              ></input> */}
            </div>
            {/* <div>Tools </div> */}
            <button
              onClick={() => {
                save()
              }}
              className="demdevisbutton2"
            >
              Enregistrer le commentaire
            </button>
            {chantier.stillToBill > 0.1 && (
              <Link
                id="linkresize"
                to={`/creationfacture/${chantier._id}`}
                className="btn btn-primary btnlist"
              >
                Creation Facture
              </Link>
            )}
            <h3>Factures </h3>
            <div id="tabledevis">
              <Table striped bordered hover id="myTable">
                <thead>
                  <tr>
                    <th>Num</th>
                    <th>Date Facturation</th>
                    <th>Total tvac</th>
                    <th>Payé</th>
                    <th>Link</th>
                  </tr>
                </thead>
                <tbody>
                  {factures &&
                    factures.map((value, key) => (
                      <tr>
                        <td>{value.numero} </td>
                        <td>{value.date.substring(0, 10)} </td>
                        <td>{value.totalTvac}</td>
                        <td>
                          {value.payed === true ? (
                            <div>
                              <button
                                className="demdevisbutton2"
                                onClick={() => pay(value._id, value.payed)}
                              >
                                <i class="fas fa-euro-sign"></i>
                              </button>
                            </div>
                          ) : (
                            <div>
                              <button
                                className="payno"
                                onClick={() => pay(value._id, value.payed)}
                              >
                                <i class="fas fa-euro-sign"></i>
                              </button>
                            </div>
                          )}
                        </td>
                        <td>
                          <Link
                            id="linkresize"
                            to={`/facture/${value._id}`}
                            className="btn btn-primary btnlist"
                          >
                            <i className="fas fa-plus" />
                          </Link>
                        </td>
                      </tr>
                    ))}
                </tbody>
              </Table>
            </div>
          </div>
        )
      )}
    </Fragment>
  )
}

Chantier.propTypes = {
  chantier: PropTypes.object.isRequired,
  facture: PropTypes.object.isRequired,
  getChantierById: PropTypes.func.isRequired,
  updateChantier: PropTypes.func.isRequired,
  getFactureByChantier: PropTypes.func.isRequired,
  factureIsPayed: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  chantier: state.chantier,
  auth: state.auth,
  facture: state.facture
})

export default connect(mapStateToProps, {
  getFactureByChantier,
  getChantierById,
  updateChantier,
  factureIsPayed
})(Chantier)
