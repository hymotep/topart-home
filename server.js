const express = require('express')
const connectDB = require('./config/db.js')
const path = require('path')
const cors = require('cors')

// import User from './models/User';
const fileUpload = require('express-fileupload')
const app = express()

//connection à la database
connectDB()
//init middleware
app.use(express.json({ extended: false }))
// app.use(cors())
app.use(fileUpload())

//routes
app.use('/api/users', require('./routes/api/users'))
app.use('/api/auth', require('./routes/api/auth'))
app.use('/api/event', require('./routes/api/event'))
app.use('/api/demande', require('./routes/api/demande'))
app.use('/api/devis', require('./routes/api/devis'))
app.use('/api/chantier', require('./routes/api/chantier'))
app.use('/api/price', require('./routes/api/price'))
app.use('/api/facture', require('./routes/api/facture'))
app.use('/api/email', require('./routes/api/email'))
app.use('/api/compta', require('./routes/api/compta'))

// app.use('/api/createdevis', require('./routes/api/createdevis'))

app.use('/apifile/uploadfile', require('./routes/apifile/uploadfile'))

app.use(express.static('client/build'))

app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
})

// Serve static assets in production

// Serve Heroku static assets in production
// if (process.env.NODE_ENV === 'production') {
//   // Set static folder
//   app.use(express.static('client/build'))

//   app.get('*', (req, res) => {
//     res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
//   })
// }

//server apache
// if (process.env.NODE_ENV === 'production') {
//   app.use('/', express.static(__dirname + '/'))
//   app.get('/', (req, res) => {
//     res.sendFile(path.resolve(__dirname, 'index.html'))
//   })
// }

// app.use(express.static(__dirname)) //here is important thing - no static directory, because all static :)
// app.use(express.static('build'))
// app.get('/*', function (req, res) {
//   res.sendFile(path.resolve(__dirname, '../client/build', 'index.html'))
// })

// if (process.env.NODE_ENV === 'production') {
//   app.use(express.static(__dirname))
//   app.get('/*', (req, res) => {
//     res.sendFile(path.join(__dirname, 'index.html'))
//   })
// }

// if (process.env.NODE_ENV === 'production') {
//   app.use(express.static('client/{build}'))

// app.get('*', (req, res) => {
//   res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
// })

// app.get('/', (req, res) => {
//   res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
// })

// }

const port = process.env.PORT || 5000
app.listen(port, () => console.log(`Server running on port ${port}`))
