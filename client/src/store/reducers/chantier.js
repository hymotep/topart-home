import {
  GET_ALLCHANTIER,
  GET_CHANTIER,
  DELETE_CHANTIER,
  UPDATE_CHANTIER,
  CHANTIER_ERROR,
  CLEAR_CHANTIER
} from '../actions/types'

const initialState = {
  chantier: null,
  chantiers: null,
  loading: true,
  error: {}
}

export default function (state = initialState, action) {
  const { type, payload } = action

  switch (type) {
    case GET_CHANTIER:
      return {
        ...state,
        chantier: payload,
        loading: false
      }
    case UPDATE_CHANTIER:
      const index = state.chantiers.findIndex(
        (chantier) => chantier._id === payload._id
      )
      const newArray = state.chantiers
      newArray[index] = payload

      return {
        ...state,
        chantiers: newArray,
        chantier: payload,
        loading: false
      }
    case GET_ALLCHANTIER:
      return {
        ...state,
        chantiers: payload,
        loading: false
      }
    case DELETE_CHANTIER:
      return {
        ...state,
        chantiers: state.chantiers.filter(
          (chantier) => chantier._id !== payload
        ),
        loading: false
      }
    case CHANTIER_ERROR:
      return {
        ...state,
        error: payload,
        loading: false
      }
    case CLEAR_CHANTIER:
      return {
        ...state,
        chantier: null,
        chantiers: null,
        loading: false
      }

    default:
      return state
  }
}
