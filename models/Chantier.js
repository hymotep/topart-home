const mongoose = require('mongoose')

const ChantierSchema = new mongoose.Schema({
  titre: { type: String },
  numero: { type: Number, required: true },

  devis: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'devis'
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  },
  extra: { type: Object },
  dateDeb: { type: String },
  dateFin: { type: String },
  line: { type: Array },
  total: { type: Object },
  stillToPay: { type: Number },
  stillToBill: { type: Number },
  adresse: { type: String },
  commentaire: { type: String }
})

module.exports = Chantier = mongoose.model('chantier', ChantierSchema)
