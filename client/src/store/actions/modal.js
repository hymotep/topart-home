import { ActionTypes } from './types'

export const showModal = () => (dispatch) => {
  dispatch({
    type: ActionTypes.SHOW_MODAL
  })
}

export const hideModal = () => (dispatch) => {
  dispatch({
    type: ActionTypes.HIDE_MODAL
  })
}
export const showModalUser = () => (dispatch) => {
  dispatch({
    type: ActionTypes.SHOW_MODAL_USER
  })
}

export const hideModalUser = () => (dispatch) => {
  dispatch({
    type: ActionTypes.HIDE_MODAL_USER
  })
}
