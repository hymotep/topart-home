import { GET_YEAR_COMPTA, GET_COMPTAS, COMPTA_ERROR } from './types'
import api from '../../outils/api'
import { setAlert } from './alert'

// Get compta from this year
export const getThisYearCompta = () => async (dispatch) => {
  let year = new Date().getFullYear().toString()
  try {
    await api.get(`/compta/${year}`).then((res) =>
      dispatch({
        type: GET_YEAR_COMPTA,
        payload: res.data
      })
    )
  } catch (err) {
    dispatch({
      type: COMPTA_ERROR,
      payload: { msg: err.response }
    })
  }
}

// Get compta from this year
export const getStat = (month) => async (dispatch) => {
  try {
    await api.get(`/compta/stat/${month}`).then((res) =>
      dispatch({
        type: GET_COMPTAS,
        payload: res.data
      })
    )
  } catch (err) {
    dispatch({
      type: COMPTA_ERROR,
      payload: { msg: err.response }
    })
  }
}
