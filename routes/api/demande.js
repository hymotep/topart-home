const express = require('express')
const router = express.Router()
const User = require('../../models/User.js')
const DemandeDevis = require('../../models/DemandeDevis.js')
const Counters = require('../../models/Counters.js')
const auth = require('../../middleware/auth.js')
const admin = require('../../middleware/admin.js')
const checkObjectId = require('../../middleware/checkObjectId')
const { check, validationResult } = require('express-validator')

//create demande
router.post('/', async (req, res) => {
  try {
    const data = req.body
    let numeroDemandeDevis = await Counters.findOneAndUpdate(
      { _id: 'numdevis' },
      { $inc: { sequence_value: 1 } }
    )
    let temp = await User.findOne({ email: data.user.email })
    demandeDevis = new DemandeDevis({
      numero: numeroDemandeDevis.sequence_value,
      user: temp._id,
      data: data
    })

    const demande = await demandeDevis.save()
    const final = await DemandeDevis.findById(demande._id).populate('user', [
      'email',
      'name'
    ])
    res.json(final)
  } catch (err) {
    console.error(err.message)
    res.status(500).send('Server error')
  }
})

// get all demande
router.get('/', admin, async (req, res) => {
  try {
    const demandes = await DemandeDevis.find()
      .populate('user', ['email', 'name'])
      .sort({ numero: -1 })
    res.json(demandes)
  } catch (err) {
    console.error(err.message)
    res.status(500).send('Server Error')
  }
})
//get demandes by user
router.get('/user/:id', [auth, checkObjectId('id')], async (req, res) => {
  try {
    const demandes = await DemandeDevis.find({ user: req.params.id }).sort({
      numero: -1
    })
    res.json(demandes)
  } catch (err) {
    console.error(err.message)
    res.status(500).send('Server Error')
  }
})

//get one demande by id
router.get('/:id', [admin, checkObjectId('id')], async (req, res) => {
  try {
    const demande = await DemandeDevis.findById(req.params.id).populate(
      'user',
      ['email', 'name', 'adresse']
    )

    res.json(demande)
  } catch (err) {
    console.error(err.message)

    res.status(500).send('Server Error')
  }
})

//delete one demande
router.delete('/:id', [admin, checkObjectId('id')], async (req, res) => {
  try {
    const dems = await DemandeDevis.findById(req.params.id)
    await Devis.remove({ demande: req.params.id, accepted: false })
    await dems.remove()

    res.json({ msg: 'Demande supprimée' })
  } catch (err) {
    console.error(err.message)

    res.status(500).send('Server Error')
  }
})

//delete all demande
router.delete('/', async (req, res) => {
  try {
    await DemandeDevis.deleteMany()

    res.json({ msg: 'All demande deleted' })
  } catch (err) {
    console.error(err.message)
    res.status(500).send('Server Error')
  }
})

module.exports = router
