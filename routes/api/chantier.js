const express = require('express')
const router = express.Router()
const auth = require('../../middleware/auth.js')
const Devis = require('../../models/Devis.js')
const Chantier = require('../../models/Chantier.js')
const Event = require('../../models/Event.js')
const randomColor = require('randomcolor')
const checkObjectId = require('../../middleware/checkObjectId')
const admin = require('../../middleware/admin.js')
const { check, validationResult } = require('express-validator')

//create chantier

router.post(
  '/',
  [auth, [check('adresse', 'Adresse is required').not().isEmpty()]],
  async (req, res) => {
    try {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() })
      }

      const { id, adresse } = req.body
      const devis = await Devis.findById(id).populate('user')

      chantier = new Chantier({
        titre: devis.titre,
        numero: devis.numero,
        devis: devis._id,
        user: devis.user._id,
        dateDeb: devis.extra.dateDeb,
        dateFin: devis.extra.dateFin,
        line: devis.lineAuto
          ? devis.lineAuto.concat(devis.lineExtra)
          : devis.lineExtra,
        total: {
          totalTvac: devis.totalTvac,
          totalHtva: devis.totalHtva,
          tvaAmount: devis.tvaAmount,
          tva: devis.tva
        },
        stillToPay: devis.totalTvac,
        stillToBill: devis.totalTvac,
        adresse: adresse,
        extra: {
          decoupe: devis.extra.decoupe,
          ecartLamb: devis.extra.ecartLamb,
          ecartPlot: devis.extra.ecartPlot,
          nbreLamb: devis.extra.nbreLamb
        }
      })
      const result = await chantier.save()

      let dateFin
      if (devis.extra.dateDeb.localeCompare(devis.extra.dateFin) === 0) {
        dateFin = devis.extra.dateFin
      } else {
        let temp = devis.extra.dateFin.split('-')
        temp[2] = Number(temp[2]) + 1
        dateFin = temp[0] + '-' + temp[1] + '-' + temp[2]
      }

      ev = new Event({
        title: devis.user.name + ' ' + devis.titre,
        start: devis.extra.dateDeb,
        end: dateFin,
        groupeId: chantier._id,
        id: devis.user._id,
        color: randomColor()
      })
      await ev.save()

      res.json(result)
    } catch (err) {
      console.error(err.message)
      res.status(500).send('Server Error')
    }
  }
)

// Get all chantiers
router.get('/', admin, async (req, res) => {
  try {
    const chantiers = await Chantier.find()
      .populate('user', ['email', 'name'])
      .sort({ numero: -1 })
    res.json(chantiers)
  } catch (err) {
    console.error(err.message)
    res.status(500).send('Server Error')
  }
})

//Get one chantier
router.get('/:id', [auth, checkObjectId('id')], async (req, res) => {
  try {
    const chantier = await Chantier.findById(req.params.id).populate('user', [
      'email',
      'name',
      'adresse'
    ])

    res.json(chantier)
  } catch (err) {
    console.error(err.message)

    res.status(500).send('Server Error')
  }
})

//delete chantier et event correspondant

router.delete('/:id', [admin, checkObjectId('id')], async (req, res) => {
  try {
    //deleete chantier
    const chant = await Chantier.findOne({ devis: req.params.id })
    //deleete event
    await Event.findOneAndRemove({ groupeId: chant._id })
    await chant.remove()
    res.json(chant)
  } catch (err) {
    console.error(err.message)

    res.status(500).send('Server Error')
  }
})

//update chantier

router.put('/', admin, async (req, res) => {
  const { commentaire, tools } = req.body

  try {
    let chantier = await Chantier.findOneAndUpdate(
      { _id: req.body.id },
      {
        commentaire: commentaire,
        tools: tools
      }
    )
    res.json(chantier)
  } catch (err) {
    console.error(err.message)
    res.status(500).send('Server error')
  }
})

module.exports = router
