const mongoose = require('mongoose')

const ComptaSchema = new mongoose.Schema({
  year: { type: String, required: true, unique: true },
  totalHtva: { type: Number, default: 0 },
  totalTvac: { type: Number, default: 0 },
  tvaAmount: { type: Number, default: 0 },
  stillToPay: { type: Number, default: 0 }
})

module.exports = Compta = mongoose.model('compta', ComptaSchema)
