const express = require('express')
const router = express.Router()

const multer = require('multer')
const Devis = require('../../models/Devis.js')
const path = require('path')

const User = require('../../models/User.js')
const DemandeDevis = require('../../models/DemandeDevis.js')
const auth = require('../../middleware/auth.js')

router.post('/upload', (req, res) => {
  if (req.files === null) {
    return res.status(400).json({ msg: 'No file uploaded' })
  }

  const tmp = req.files.file
  let result = []
  if (req.files.file.length === undefined) {
    tmp.mv(`${__dirname}/../../client/src/upload/photos/${tmp.name}`, (err) => {
      // file.mv(`./${file.name}`, (err) => {
      if (err) {
        console.error(err)
        return res.status(500).send(err)
      }

      result.push({
        fileName: tmp.name,
        filePath: `/../../client/src/upload/photos/${tmp.name}`,
        path: __dirname
      })
    })
  } else {
    tmp.map((fil) =>
      fil.mv(
        `${__dirname}/../../client/src/upload/photos/${fil.name}`,
        (err) => {
          // file.mv(`./${file.name}`, (err) => {
          if (err) {
            console.error(err)
            return res.status(500).send(err)
          }

          result.push({
            fileName: fil.name,
            filePath: `/../../client/src/upload/photos/${fil.name}`,
            path: __dirname
          })
        }
      )
    )
  }
  res.json(result)
  // let result = []
  // tmp.map((fil) =>
  //   fil.mv(`${__dirname}/../../client/src/upload/photos/${fil.name}`, (err) => {
  //     // file.mv(`./${file.name}`, (err) => {
  //     if (err) {
  //       console.error(err)
  //       return res.status(500).send(err)
  //     }

  //     result.push({
  //       fileName: fil.name,
  //       filePath: `/../../client/src/upload/photos/${fil.name}`,
  //       path: __dirname
  //     })
  //   })
  // )
  // res.json(result)
})
router.post('/uploadcanvas', (req, res) => {
  if (req.files === null) {
    return res.status(400).json({ msg: 'No file uploaded' })
  }
  const tmp = req.files.canvasImage
  let result = []

  tmp.mv(
    `${__dirname}/../../client/src/upload/signatures/${tmp.name}`,
    (err) => {
      // file.mv(`./${file.name}`, (err) => {
      if (err) {
        console.error(err)
        return res.status(500).send(err)
      }

      result.push({
        fileName: tmp.name,
        filePath: `/../../client/src/upload/signatures/${tmp.name}`,
        path: __dirname
      })
    }
  )

  res.json(result)
})

module.exports = router
