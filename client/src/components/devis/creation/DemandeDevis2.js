import React from 'react'
import PropTypes from 'prop-types'
import {
  Modal,
  Button,
  Card,
  CardGroup,
  CardDeck,
  Form,
  Col,
  Row
} from 'react-bootstrap'
import { connect } from 'react-redux'
import { hideModal } from '../../../store/actions/modal'
import terrasse from '../../img/terasse.jpg'
import ter from '../../img/ter.jpg'
import rang from '../../img/rang.jpg'
import rigide from '../../img/rigide.jpg'
import souple from '../../img/souple.jpg'
import clot from '../../img/clot.jpg'
import bord from '../../img/bord.jpg'
import palissade from '../../img/palissade.jpg'
import pave from '../../img/pave.jpg'

import typebois2 from '../../img/typebois2.jpg'
import bangk from '../../img/bangk.png'
import bangklisse from '../../img/bangklisse.png'
import compgris from '../../img/compgris.png'
import compbrun from '../../img/compbrun.jpeg'
import pin from '../../img/pin2.PNG'
import meleze from '../../img/meleze.PNG'
import lambalu from '../../img/lambalu.PNG'
import lambazobe from '../../img/lambazobe.PNG'
import lambcomp from '../../img/lambcomp.jpg'
import lambtraite from '../../img/lambtraite.PNG'
import struct from '../../img/struct.png'
import classic from '../../img/classic.jpg'
import nylofor2d from '../../img/nylofor2d.jpg'
import torino from '../../img/torino.PNG'

import addphoto from '../../img/addphoto.png'

import { uploadDemande } from '../../../store/actions/demande'
import { testmail } from '../../../store/actions/user'
let img1 = typebois2
let img2 = struct
let img3 = clot

let step = 1
let initialuser = {
  name: '',
  email: '',
  codePostal: '',
  tel: ''
}

let initialForm = {
  type: 'intro', //intro
  // type: 'tbois', //intro
  m2: '',
  longueur: null,
  hauteurT: '',
  hauteur: '',
  largeur: null,
  typesol: '',
  typemat: '',
  typestruct: '',
  //détails
  commentaire: '',
  datedeb: '',
  datefin: '',
  photo: {},
  details: {},
  user: initialuser
}
let initialdetails = {
  spot: '',
  marches: '',
  lasure: ''
}

let windows = {
  opened: 'intro',
  last: 'intro'
}
//'intro'
let initialphoto = {
  filep1: null,
  filep2: null,
  filep3: null
}

const DemandeDevis = ({
  hideModal,
  auth,
  testmail,
  modal: { demandeDevisOpen },
  uploadDemande
}) => {
  let [window, setwindow] = React.useState(windows)
  let [formdata, setformdata] = React.useState(initialForm)
  const [errors, setErrors] = React.useState({})
  let [detaildata, setdetaildata] = React.useState(initialdetails)
  let [userdata, setuserdata] = React.useState(initialuser)
  let [photolist, setphotolist] = React.useState(initialphoto)
  let [photodata, setphotodata] = React.useState(initialphoto)
  let [photosrc, setphotosrc] = React.useState(initialphoto)

  ///////////
  //actions//
  ///////////

  const findFormErrors = () => {
    const newErrors = {}
    switch (window.opened) {
      case 'Terrasse bois': {
        let { typemat, longueur, largeur } = formdata
        if (!typemat || typemat === '') newErrors.typemat = 'Faites un choix!'
        // else if (name.length > 30) newErrors.name = 'name is too long!'
        // longueur errors
        if (!longueur || longueur === '' || longueur === 0)
          newErrors.longueur = 'indiquer une longueur!'
        // largeur errors
        if (!largeur || largeur === '' || longueur === 0)
          newErrors.largeur = 'indiquer une largeur!'
        if (!photolist.filep1) {
          let x = document.getElementById('firstphoto')
          x.style.borderColor = '#F08080'
          x = document.getElementById('phototext')
          x.style.display = 'block'
          newErrors.photo = 'Ajouter une photo!'
        }
        break
      }
      case 'userdata': {
        let { name, email, codePostal, tel } = userdata
        if (!name || name === '') newErrors.name = 'Indiquer votre nom complet!'
        // else if (name.length > 30) newErrors.name = 'name is too long!'
        // longueur errors
        if (!email || email === '') newErrors.email = 'indiquer un email !'
        else if (!email.match(/[a-z0-9_\-.]+@[a-z0-9_\-.]+.[a-z]+/i)) {
          newErrors.email = 'indiquer un email valide !'
        }
        if (!tel || tel === '') newErrors.tel = 'indiquer un num de téléphone !'
        // if (tel.match(/^(0[1-68])(?:[ _.-]?(\d{2})){4}$/))
        //   newErrors.tel = 'indiquer un numéro de téléphone valide !'
        if (!codePostal || codePostal === '')
          newErrors.codePostal = 'indiquer un code postal!'
        else if (codePostal.length !== 4 && !codePostal.match(0 - 9))
          newErrors.codePostal = 'indiquer un code postal Valide!'
        break
      }
      default: {
      }
    }
    // name errors

    return newErrors
  }

  const onSubmit = async (e) => {
    // e.preventDefault()
    let newErrors = findFormErrors()

    if (Object.keys(newErrors).length > 0) {
      setErrors(newErrors)
    } else {
      testmail(userdata.email).then(function (response) {
        if (response) {
          newErrors.email = 'email déja utilisé'
          setErrors(newErrors)
        } else {
          let admin = auth.isAuthenticated && auth.user.admin ? true : false
          let tempName = userdata.email.split('@')
          let temp
          let d = new Date()
          let date =
            d.getFullYear().toString() +
            d.getMonth().toString() +
            d.getDate().toString() +
            d.getHours().toString() +
            d.getMinutes().toString() +
            d.getSeconds().toString()

          if (photosrc['filep1']) {
            temp = photosrc['filep1'].split('.')
            photosrc['filep1'] =
              tempName[0] + date + '0.' + temp[temp.length - 1]
          }
          if (photosrc['filep2']) {
            temp = photosrc['filep2'].split('.')
            photosrc['filep2'] =
              tempName[0] + date + '1.' + temp[temp.length - 1]
          }
          if (photosrc['filep3']) {
            temp = photosrc['filep3'].split('.')
            photosrc['filep3'] =
              tempName[0] + date + '2.' + temp[temp.length - 1]
          }

          formdata.photo = photosrc
          if (auth.isAuthenticated && !admin) {
            userdata.name = auth.user.name
            userdata.email = auth.user.email
            formdata.user = userdata
          }
          uploadDemande(
            removeEmptyOrNull(formdata),
            photodata,
            admin,
            auth.isAuthenticated
          )
          setwindow({ ...window, opened: 'intro' })
          hideModal()
        }
      })
    }
  }

  const choixPanneau = (e) => {
    const words = e.target.value.split(' ')
    setformdata({ ...formdata, typemat: e.target.value, hauteur: words[1] })
    switch (words[0]) {
      case 'Torino': {
        img3 = torino
        break
      }
      case 'Classic': {
        img3 = classic
        break
      }
      case 'Nylofor2d': {
        img3 = nylofor2d
        break
      }
      default: {
      }
    }
  }

  const chooseWood = (e) => {
    setformdata({ ...formdata, typemat: e.target.value })
    if (!!errors['typemat'])
      setErrors({
        ...errors,
        typemat: null
      })
    switch (e.target.value) {
      case 'typebois2': {
        img1 = typebois2
        break
      }
      case 'meleze': {
        img1 = meleze
        break
      }
      case 'compositeb': {
        img1 = compbrun
        break
      }
      case 'compositea': {
        img1 = compgris
        break
      }
      case 'pinthermo': {
        img1 = pin
        break
      }
      case 'bangkiraiL': {
        img1 = bangklisse
        break
      }
      case 'bangkirai': {
        img1 = bangk
        break
      }
      default: {
      }
    }
    // var element = document.getElementById('cardimg2')
    // element.src = test
  }

  const chooseStruct = (e) => {
    setformdata({ ...formdata, typestruct: e.target.value })
    switch (e.target.value) {
      case 'struct':
      case 'conseille': {
        img2 = struct
        break
      }
      case 'lambtraite': {
        img2 = lambtraite
        break
      }
      case 'lambcomp': {
        img2 = lambcomp
        break
      }
      case 'lambalu': {
        img2 = lambalu
        break
      }
      case 'lambazobe': {
        img2 = lambazobe
        break
      }
      default: {
      }
    }
    // var element = document.getElementById('cardimg3')
    // element.src = test
  }
  const removeEmptyOrNull = (obj) => {
    Object.keys(obj).forEach(
      (k) =>
        (obj[k] && typeof obj[k] === 'object' && removeEmptyOrNull(obj[k])) ||
        (!obj[k] && obj[k] !== undefined && delete obj[k])
    )

    return obj
  }

  function nextStep(val) {
    const newErrors = findFormErrors()
    if (Object.keys(newErrors).length > 0) {
      // We got errors!
      setErrors(newErrors)
    } else if (
      auth.isAuthenticated &&
      !auth.user.admin &&
      val.localeCompare('userdata') === 0
    ) {
      onSubmit()
      // document.getElementById('validbutton').click()
    } else {
      if (val.localeCompare('userdata') !== 0) {
        setformdata({ formdata, type: val })

        setwindow({ ...window, opened: val, last: val })
      } else {
        setwindow({ ...window, opened: val })
      }
    }
  }

  function prevStep(val) {
    // step = step + 1
    setformdata({ ...formdata, type: val })
    setwindow({ ...window, opened: val })
  }

  let onChange = (e) => {
    if (!!errors[e.target.name])
      setErrors({
        ...errors,
        [e.target.name]: null
      })
    switch (e.target.name) {
      case 'longueur': {
        if (formdata.largeur === undefined) {
          setformdata({
            ...formdata,
            [e.target.name]: e.target.value,
            m2: ''
          })
        } else {
          setformdata({
            ...formdata,
            [e.target.name]: e.target.value,
            m2: '= ' + (formdata.largeur * e.target.value) / 10000 + 'm2'
          })
        }
        break
      }
      case 'largeur': {
        if (formdata.longueur === undefined) {
          setformdata({
            ...formdata,
            [e.target.name]: e.target.value,
            m2: ''
          })
        } else {
          setformdata({
            ...formdata,
            [e.target.name]: e.target.value,
            m2: '= ' + (formdata.longueur * e.target.value) / 10000 + 'm2'
          })
        }
        break
      }
      default: {
        setformdata({ ...formdata, [e.target.name]: e.target.value })
      }
    }
  }
  let onChange2 = (e) => {
    // setformdata({ ...formdata, [e.target.name]: e.target.checked })
    setdetaildata({ ...detaildata, [e.target.name]: e.target.checked })
    setformdata({ ...formdata, details: detaildata })
  }
  let onChange4 = (e) => {
    // setformdata({ ...formdata, [e.target.name]: e.target.checked })
    setuserdata({ ...userdata, [e.target.name]: e.target.value })
    setformdata({ ...formdata, user: userdata })
    if (!!errors[e.target.name])
      setErrors({
        ...errors,
        [e.target.name]: null
      })
  }
  let onBlur4 = (e) => {
    // setformdata({ ...formdata, [e.target.name]: e.target.checked })
    setuserdata({ ...userdata, [e.target.name]: e.target.value })
    setformdata({ ...formdata, user: userdata })
  }
  let onChange3 = (e) => {
    setdetaildata({ ...detaildata, [e.target.name]: e.target.value })
    setformdata({ ...formdata, details: detaildata })
  }
  let onBlur2 = (e) => {
    setdetaildata({ ...detaildata, [e.target.name]: e.target.checked })
    setformdata({ ...formdata, details: detaildata })
  }
  let onBlur3 = (e) => {
    setdetaildata({ ...detaildata, [e.target.name]: e.target.value })
    setformdata({ ...formdata, details: detaildata })
  }

  let uploadSingleFile = (e) => {
    e.preventDefault()
    if (e.target.files[0]) {
      setphotodata({
        ...photodata,
        ['file' + e.target.name]: e.target.files[0]
      })
      setphotosrc({
        ...photosrc,
        ['file' + e.target.name]: e.target.files[0].name
      })
      setphotolist({
        ...photolist,
        ['file' + e.target.name]: URL.createObjectURL(e.target.files[0])
      })
      if (e.target.name === 'p1') {
        let x = document.getElementById('firstphoto')
        x.style.borderColor = '#c8b2a1'
        x = document.getElementById('phototext')
        x.style.display = 'none'
      }
    }
  }
  let deletepict = (number) => {
    // document.getElementById('photo' + number).style.display = 'block'
    // document.getElementById('img' + number).style.display = 'none'
    setphotodata({
      ...photodata,
      ['file' + number]: null
    })
    setphotolist({
      ...photolist,
      ['file' + number]: null
    })
    setphotosrc({
      ...photosrc,
      ['file' + number]: null
    })
  }

  ////////////
  //elements//
  ////////////

  function selectPictures() {
    return (
      <div style={{ width: '100%' }}>
        <Form.Label className="demdevisLabel">Photo lieu de pose </Form.Label>

        <Form.Row>
          <div id="firstphoto" className="photocard ">
            <div className="multi-preview">
              {photolist.filep1 ? (
                <div style={{ position: 'relative' }}>
                  <img
                    className="photoholder"
                    id="imgp1"
                    src={photolist.filep1}
                    alt="..."
                  />
                  {!photolist.filep2 && (
                    <div
                      style={{
                        textAlign: 'center',
                        position: 'absolute',
                        top: '10px',
                        right: '5px',
                        width: '45px'
                      }}
                    >
                      <div
                        // className= "deletepc"
                        type="button"
                        style={{
                          backgroundColor: 'transparent',
                          color: 'red'
                        }}
                        onClick={() => deletepict('p1')}
                      >
                        <i className="fa fa-times fa-lg" aria-hidden="true"></i>
                      </div>
                    </div>
                  )}
                </div>
              ) : (
                <div>
                  <label id="photop1">
                    <img src={addphoto} alt="..."></img>
                    <input
                      type="file"
                      name="p1"
                      onChange={uploadSingleFile}
                      accept="image/x-png,image/gif,image/jpeg"
                      hidden
                    ></input>
                  </label>
                </div>
              )}
            </div>
          </div>
          {photolist.filep1 ? (
            <div className="photocard">
              <div className="multi-preview">
                {photolist.filep2 ? (
                  <div style={{ position: 'relative' }}>
                    <img
                      className="photoholder"
                      id="imgp2"
                      src={photolist.filep2}
                      alt="..."
                    />
                    {!photolist.filep3 && (
                      <div
                        style={{
                          textAlign: 'center',
                          position: 'absolute',
                          top: '10px',
                          right: '5px',
                          width: '45px'
                        }}
                      >
                        <div
                          // className= "deletepc"
                          type="button"
                          style={{
                            backgroundColor: 'transparent',
                            color: 'red'
                          }}
                          onClick={() => deletepict('p2')}
                        >
                          <i
                            className="fa fa-times fa-lg"
                            aria-hidden="true"
                          ></i>
                        </div>
                      </div>
                    )}
                  </div>
                ) : (
                  <div>
                    <label id="photop2">
                      <img src={addphoto} alt="..."></img>
                      <input
                        type="file"
                        name="p2"
                        onChange={uploadSingleFile}
                        accept="image/x-png,image/gif,image/jpeg"
                        hidden
                      ></input>
                    </label>
                  </div>
                )}
              </div>
            </div>
          ) : (
            <div></div>
          )}

          {photolist.filep2 ? (
            <div className="photocard">
              <div className="multi-preview">
                {photolist.filep3 ? (
                  <div style={{ position: 'relative' }}>
                    <img
                      className="photoholder"
                      id="imgp3"
                      src={photolist.filep3}
                      alt="..."
                    />

                    <div
                      style={{
                        textAlign: 'center',
                        position: 'absolute',
                        top: '10px',
                        right: '5px',
                        width: '45px'
                      }}
                    >
                      <div
                        // className= "deletepc"
                        type="button"
                        style={{
                          backgroundColor: 'transparent',
                          color: 'red'
                        }}
                        onClick={() => deletepict('p3')}
                      >
                        <i className="fa fa-times fa-lg" aria-hidden="true"></i>
                      </div>
                    </div>
                  </div>
                ) : (
                  <div>
                    <label id="photop3">
                      <img src={addphoto} alt="..."></img>
                      <input
                        type="file"
                        name="p3"
                        onChange={uploadSingleFile}
                        accept="image/x-png,image/gif,image/jpeg"
                        hidden
                      ></input>
                    </label>
                  </div>
                )}
              </div>
            </div>
          ) : (
            <div></div>
          )}
        </Form.Row>
        <div id="phototext" style={{ display: 'none', color: 'LightCoral' }}>
          Ajouter une photo!
        </div>
      </div>
    )
  }

  function body() {
    switch (window.opened) {
      case 'intro':
        return (
          <div>
            <Form>
              <div>
                <CardDeck>
                  <CardGroup>
                    <Card
                      id="card"
                      name="ter"
                      value="ter"
                      onClick={() => nextStep('ter')}
                    >
                      {/* <Card.Header>Terrasse</Card.Header> */}
                      <Card.Img variant="top" src={ter} id="cardimg"></Card.Img>
                      <Card.Body>
                        <Card.Text>Terrasses bois ou pavé</Card.Text>
                      </Card.Body>
                    </Card>
                    <Card
                      id="card"
                      name="clot"
                      value="clot"
                      onClick={() => nextStep('clot')}
                    >
                      {/* <Card.Header>Terrasse</Card.Header> */}
                      <Card.Img
                        variant="top"
                        src={clot}
                        id="cardimg"
                      ></Card.Img>
                      <Card.Body>
                        <Card.Text>Cloture metal ou bois</Card.Text>
                      </Card.Body>
                    </Card>
                    <Card
                      id="card"
                      name="bord"
                      value="bord"
                      onClick={() => nextStep('bord')}
                    >
                      {/* <Card.Header>Terrasse</Card.Header> */}
                      <Card.Img
                        variant="top"
                        src={bord}
                        id="cardimg"
                      ></Card.Img>
                      <Card.Body>
                        <Card.Text>Bordures / Gabions</Card.Text>
                      </Card.Body>
                    </Card>
                  </CardGroup>
                </CardDeck>
              </div>
            </Form>
          </div>
        )
      case 'ter':
        return (
          <div>
            <div>
              <i
                className="fas fa-arrow-alt-circle-left fa-2x"
                onClick={() => prevStep('intro')}
              ></i>
            </div>
            <Form id="formDevis">
              <CardDeck>
                <CardGroup>
                  <Card id="card" onClick={() => nextStep('Terrasse bois')}>
                    {/* <Card.Header>Terrasse</Card.Header> */}
                    <Card.Img variant="top" src={terrasse}></Card.Img>
                    <Card.Body>
                      <Card.Text>Terrasse en bois</Card.Text>
                    </Card.Body>
                  </Card>
                  <Card id="card" onClick={() => nextStep('terrasse pavé')}>
                    <Card.Img variant="top" src={pave} />
                    <Card.Body>
                      {/* <Card.Title>Card title</Card.Title> */}
                      <Card.Text>Pave</Card.Text>
                    </Card.Body>
                  </Card>
                </CardGroup>
              </CardDeck>
              <CardDeck>
                <CardGroup></CardGroup>
              </CardDeck>
            </Form>
          </div>
        )
      case 'clot':
        return (
          <div>
            <div>
              <i
                className="fas fa-arrow-alt-circle-left fa-2x"
                onClick={() => prevStep('intro')}
              ></i>
            </div>
            <Form id="formDevis">
              <CardGroup>
                <Card id="card" onClick={() => nextStep('cloture rigide')}>
                  {/* <Card.Title>Cloture</Card.Title> */}
                  <Card.Img variant="top" src={rigide} />
                  <Card.Body>
                    <Card.Text>Cloture panneau rigide</Card.Text>
                  </Card.Body>
                </Card>

                <Card id="card" onClick={() => nextStep('cloture bois')}>
                  <Card.Img variant="top" src={palissade} />
                  <Card.Body>
                    {/* <Card.Title>Card title</Card.Title> */}
                    <Card.Text>Palissade</Card.Text>
                  </Card.Body>
                </Card>
                <Card id="card" onClick={() => nextStep('cloture souple')}>
                  <Card.Img variant="top" src={souple} />
                  <Card.Body>
                    {/* <Card.Title>Card title</Card.Title> */}
                    <Card.Text>Grillage souple en Rouleau</Card.Text>
                  </Card.Body>
                </Card>
              </CardGroup>
            </Form>
          </div>
        )
      case 'bord':
        return (
          <div>
            tete
            <Button onClick={() => prevStep('intro')}>Retour</Button>
          </div>
        )
      case 'Terrasse bois':
        // setformdata({ ...formdata, typesol: 'terre' })
        return (
          <div>
            <div>
              <i
                className="fas fa-arrow-alt-circle-left fa-2x"
                onClick={() => prevStep('ter')}
              ></i>
            </div>

            <Form id="formDevis" className="formDevs">
              <Form.Group id="groupeterrasse">
                <Form.Row>
                  <Col md={5}>
                    <div style={{ width: '100%' }}>
                      <Form.Label className="demdevisLabel">
                        Type de bois
                      </Form.Label>
                    </div>

                    <Form.Row>
                      <Form.Control
                        as="select"
                        name="typemat"
                        value={formdata.typemat}
                        onChange={chooseWood}
                        isInvalid={!!errors.typemat}
                        placeholder="cliquer pour v oir"
                      >
                        <option value="" disabled selected hidden>
                          Cliquer pour voir
                        </option>
                        <option value="meleze">Mélèze (24.6€/m²)</option>
                        <option value="compositeb">
                          Composite brun (41.99€/m²)
                        </option>
                        <option name="compositea" value="compositea">
                          Composite anthracite (41.99€/m²)
                        </option>
                        <option value="pinthermo">Pin thermo (44.3€/m²)</option>
                        <option value="bangkiraiL">
                          Bangkirai lisse (+-54.5€/m²)
                        </option>

                        <option value="bangkirai">
                          Bangkirai rainuré (+-65.5€/m²)
                        </option>
                      </Form.Control>
                      <Form.Control.Feedback type="invalid">
                        {errors.typemat}
                      </Form.Control.Feedback>
                    </Form.Row>
                    <Form.Row>
                      <Card.Img
                        style={{
                          marginTop: '4px',
                          border: '1px solid #c8b2a1 '
                        }}
                        variant="top"
                        src={img1}
                        id="cardimg1"
                      ></Card.Img>
                    </Form.Row>
                  </Col>

                  <Col md={1}> </Col>
                  <Col md={5} style={{ marginLeft: '10px' }}>
                    <div style={{ width: '100%' }}>
                      <Form.Label className="demdevisLabel">
                        Structure(optionnel)
                      </Form.Label>
                    </div>
                    <Form.Row>
                      <Form.Control
                        as="select"
                        Name="typestruct"
                        value={formdata.typestruct}
                        onChange={chooseStruct}
                      >
                        <option value="struct"></option>
                        <option value="" disabled selected hidden>
                          Cliquer pour voir
                        </option>
                        <option value="lambtraite">
                          Traité autoclave (2.44€/m)
                        </option>
                        <option value="lambcomp">Composite (4.55€/m)</option>
                        <option value="lambazobe">Azobe (5€/m)</option>
                        <option value="lambalu">Aluminium (6.25€/m)</option>
                        <option value="conseille">Conseillé </option>
                      </Form.Control>
                    </Form.Row>
                    <Form.Row>
                      <Card.Img
                        style={{
                          marginTop: '4px',
                          border: '1px solid #c8b2a1 '
                        }}
                        variant="top"
                        src={img2}
                        id="cardimg2"
                      ></Card.Img>
                    </Form.Row>
                  </Col>
                </Form.Row>
                <div style={{ width: '100%' }}>
                  <Form.Label className="demdevisLabel">
                    Surface <em className="boldunder">(en centimètre)</em>
                    <em>{formdata.m2} </em>
                  </Form.Label>
                </div>
                <Form.Row>
                  <Col md={5}>
                    <Form.Control
                      style={{ marginBottom: '10px' }}
                      type="number"
                      Name="longueur"
                      value={formdata.longueur}
                      placeholder="Longueur (sens des planches)"
                      onChange={onChange}
                      isInvalid={!!errors.longueur}
                    />
                    <Form.Control.Feedback type="invalid">
                      {errors.longueur}
                    </Form.Control.Feedback>
                  </Col>
                  <Col md={1}></Col>
                  <Col md={5}>
                    <Form.Control
                      type="number"
                      Name="largeur"
                      value={formdata.largeur}
                      onChange={onChange}
                      placeholder="Largeur"
                      isInvalid={!!errors.largeur}
                    />
                    <Form.Control.Feedback type="invalid">
                      {errors.largeur}
                    </Form.Control.Feedback>
                  </Col>
                </Form.Row>
                <Form.Row>
                  <Col md={5}>
                    <div style={{ width: '100%' }}>
                      <Form.Label className="demdevisLabel">
                        Hauteur (optionnel)
                      </Form.Label>
                    </div>
                    <Form.Row>
                      <Form.Control
                        type="number"
                        Name="hauteur"
                        value={formdata.hauteur}
                        onChange={onChange}
                        placeholder="Mesure"
                      />
                    </Form.Row>
                  </Col>
                  <Col md={1}></Col>

                  <Col md={5}>
                    <div style={{ width: '100%' }}>
                      <Form.Label className="demdevisLabel">
                        Sol actuel (optionnel)
                      </Form.Label>
                    </div>

                    <Form.Row>
                      <Form.Control
                        as="select"
                        Name="typesol"
                        value={formdata.typesol}
                        onChange={onChange}
                      >
                        <option value="" disabled selected hidden>
                          Faire un choix
                        </option>
                        <option></option>
                        <option value="terre">terre</option>
                        <option>pavé/béton</option>
                        <option>gravier</option>
                        <option>toit plat</option>
                      </Form.Control>
                    </Form.Row>
                  </Col>
                </Form.Row>

                <Form.Row>
                  <Col md={5}>
                    <div style={{ width: '100%' }}>
                      <Form.Label className="demdevisLabel">
                        Détails (optionnel)
                      </Form.Label>
                    </div>
                    <div id="details" style={{ width: '100%' }}>
                      <Form.Check
                        type="checkbox"
                        label="Lasure de protection"
                        Name="lasure"
                        value={detaildata.lasure}
                        onChange={onChange2}
                        onBlur={onBlur2}
                      />
                      <Form.Check
                        type="checkbox"
                        label="Marches"
                        Name="marches"
                        value={detaildata.marches}
                        onChange={onChange2}
                        onBlur={onBlur2}
                      />

                      {detaildata.marches && (
                        <div>
                          <Form.Control
                            type="number"
                            Name="marches"
                            value={detaildata.marches}
                            onChange={onChange3}
                            onBlur={onBlur3}
                            placeholder="Nombres"
                          />
                        </div>
                      )}

                      <Form.Check
                        type="checkbox"
                        label="Spot Lumineux"
                        id="idcheck"
                        Name="spot"
                        value={formdata.spot}
                        onChange={onChange2}
                        onBlur={onBlur2}
                      />
                      {detaildata.spot && (
                        <div>
                          <Form.Control
                            type="number"
                            Name="spot"
                            value={detaildata.spot}
                            onChange={onChange3}
                            onBlur={onBlur3}
                            placeholder="Nombres"
                          />
                        </div>
                      )}
                    </div>
                  </Col>
                  <Col md={1}> </Col>
                  <Col md={6}>{selectPictures()}</Col>
                </Form.Row>
              </Form.Group>
            </Form>

            <Button
              // variant="primary"
              id="validbutton2"
              onClick={() => nextStep('userdata')}
            >
              Valider
            </Button>
          </div>
        )
      case 'terrasse pavé':
        return (
          <div>
            <div>
              <i
                className="fas fa-arrow-alt-circle-left fa-2x"
                onClick={() => prevStep('ter')}
              ></i>
            </div>
            <Form id="formDevis">
              <Form.Group>
                <Form.Row>
                  <Form.Label className="demdevisLabel">
                    Type de sol à recouvrir
                  </Form.Label>
                </Form.Row>
                <Form.Row>
                  <Form.Control as="select">
                    <option>terre</option>
                    <option>gravier</option>

                    <option>ancienne terrasse</option>
                  </Form.Control>
                </Form.Row>
                {/* <Form.Row></Form.Row>
                <Form.Row></Form.Row>
                <Form.Row></Form.Row> */}

                <Form.Row>
                  <Form.Label className="demdevisLabel">
                    Type de pavé
                  </Form.Label>
                </Form.Row>
                <Form.Row>
                  <Form.Control
                    type="string"
                    placeholder="Préférences/couleur/dimension"
                  />
                </Form.Row>
                <Form.Row>
                  <Form.Label className="demdevisLabel">Détails</Form.Label>
                </Form.Row>
                <Form.Row>
                  <Col>
                    <Form.Check type="checkbox" label="Bordures" id="idcheck" />
                  </Col>
                  <Col>
                    <Form.Check type="checkbox" label="Marches" id="idcheck" />
                  </Col>
                </Form.Row>
                {selectPictures()}
              </Form.Group>
            </Form>
          </div>
        )
      case 'cloture rigide':
        return (
          <div>
            <div>
              <i
                className="fas fa-arrow-alt-circle-left fa-2x"
                onClick={() => prevStep('clot')}
              ></i>
            </div>
            <Form id="formDevis">
              <Form id="formDevis">
                <Form.Group>
                  <Form.Label className="demdevisLabel">
                    Type de panneau
                  </Form.Label>

                  <Form.Row>
                    <Form.Control
                      as="select"
                      Name="typesol"
                      value={formdata.typemat}
                      onChange={choixPanneau}
                    >
                      <option value="" disabled selected hidden>
                        Cliquer pour voir
                      </option>
                      <option value="" disabled style={{ fontWeight: 'bold' }}>
                        Giardino Torino(200cm large)
                      </option>
                      <option value="Torino 103">103cm (20.5€/m²)</option>
                      <option value="Torino 123">123cm (23.9€/m²)</option>
                      <option value="Torino 153">153cm (29.9€/m²)</option>
                      <option value="Torino 173">173cm (32.9€/m²)</option>
                      <option value="Torino 203">203cm (39.9€/m²)</option>
                      <option value="" disabled style={{ fontWeight: 'bold' }}>
                        Betafence Classic(200cm large)
                      </option>
                      <option value="Classic 63">63cm (25.89€)</option>
                      <option value="Classic 103">103cm (31.9€)</option>
                      <option value="Classic 123">123cm (36.91€)</option>
                      <option value="Classic 153">153cm (44.9€)</option>
                      <option value="Classic 173">173cm (50.9€)</option>
                      <option value="Classic 203">203cm (57.9€)</option>
                      <option value="" disabled style={{ fontWeight: 'bold' }}>
                        Betafence Nylofor 2d(250cm large)
                      </option>
                      <option value="Nylofor2d 103">103cm (44.9€/m²)</option>
                      <option value="Nylofor2d 123">123cm (52.9€/m²)</option>
                      <option value="Nylofor2d 143">143cm (62.9€/m²)</option>
                      <option value="Nylofor2d 163">163cm (69.99€/m²)</option>
                      <option value="Nylofor2d 183">183cm (75.9€/m²)</option>
                    </Form.Control>
                  </Form.Row>
                  <Form.Row>
                    <Card.Img
                      style={{
                        marginTop: '4px',
                        border: '1px solid #ff4000 '
                      }}
                      variant="top"
                      src={img3}
                      id="cardimg3"
                    ></Card.Img>
                  </Form.Row>

                  <Form.Row>
                    <Form.Group as={Col} controlId="formGridEmail">
                      <Form.Label className="demdevisLabel">
                        Longueur :
                      </Form.Label>
                      <Form.Control
                        type="number"
                        Name="longueur"
                        value={formdata.longueur}
                        placeholder="Longueur"
                        onChange={onChange}
                        placeholder="Mètres"
                      />
                    </Form.Group>
                    <Form.Group as={Col} controlId="formGridPassword">
                      <Form.Label className="demdevisLabel">
                        Hauteur :
                      </Form.Label>
                      <Form.Control as="select" placeholder="Hauteur">
                        <option>63cm</option>
                        <option>83cm</option>
                        <option>103cm</option>
                        <option>123cm</option>
                        <option>153cm</option>
                        <option>173cm</option>
                        <option>203cm</option>
                      </Form.Control>
                    </Form.Group>
                  </Form.Row>
                  <Form.Row>
                    <Form.Group as={Col} controlId="formGridEmail">
                      <Form.Label className="demdevisLabel">
                        Nbre de coin :
                      </Form.Label>
                      <Form.Control type="number" placeholder="0" />
                    </Form.Group>
                    <Form.Group as={Col} controlId="formGridPassword">
                      <Form.Label className="demdevisLabel">
                        Couleur :
                      </Form.Label>
                      <Form.Control as="select">
                        <option>vert</option>
                        <option>noir</option>
                        <option>anthracite</option>
                      </Form.Control>
                    </Form.Group>
                  </Form.Row>
                  <Form.Row>
                    <Form.Label className="demdevisLabel">Détails :</Form.Label>
                  </Form.Row>
                  <Form.Row>
                    <Col>
                      <Form.Check
                        type="checkbox"
                        label="Portillon"
                        id="idcheck"
                      />
                    </Col>
                    <Col>
                      <Form.Check
                        type="checkbox"
                        label="Soubassement(+20cm)"
                        id="idcheck"
                      />
                    </Col>
                  </Form.Row>

                  <Form.Row>
                    <Form.Label className="demdevisLabel">
                      Type de sol du lieu de pose :
                    </Form.Label>
                  </Form.Row>
                  <Form.Row>
                    <Form.Control
                      type="string"
                      placeholder="ex: terre/béton/pavé/gravier/mixte"
                    />
                  </Form.Row>
                  <Form.Row>
                    <Form.Label className="demdevisLabel">
                      Importer photo :
                    </Form.Label>
                  </Form.Row>
                  <Form.File
                    id="exampleFormControlFile1"
                    // label="Example file input"
                  />
                </Form.Group>
              </Form>
            </Form>
            <Button
              // variant="primary"
              id="validbutton2"
              onClick={() => nextStep('userdata')}
            >
              Valider
            </Button>
          </div>
        )
      case 'cloture bois':
        return (
          <div>
            <i
              className="fas fa-arrow-alt-circle-left fa-2x"
              onClick={() => prevStep('clot')}
            ></i>
            <i className="fas fa-times-circle fa-2x" onClick={hideModal}></i>

            <Form id="formDevis">
              <Form id="formDevis">
                <Form.Group>
                  <Form.Row>
                    <Form.Group as={Col} controlId="formGridEmail">
                      <Form.Label className="demdevisLabel">
                        Longueur :
                      </Form.Label>
                      <Form.Control type="number" placeholder="Mètres" />
                    </Form.Group>
                    <Form.Group as={Col} controlId="formGridPassword">
                      <Form.Label className="demdevisLabel">
                        Hauteur :
                      </Form.Label>
                      <Form.Control as="select" placeholder="Hauteur">
                        <option>63cm</option>
                        <option>83cm</option>
                        <option>103cm</option>
                        <option>123cm</option>
                        <option>153cm</option>
                        <option>173cm</option>
                        <option>203cm</option>
                      </Form.Control>
                    </Form.Group>
                  </Form.Row>
                  <Form.Row>
                    <Form.Group as={Col} controlId="formGridEmail">
                      <Form.Label className="demdevisLabel">
                        Nbre de coin :
                      </Form.Label>
                      <Form.Control type="number" />
                    </Form.Group>
                    <Form.Group as={Col} controlId="formGridPassword">
                      <Form.Label className="demdevisLabel">
                        Couleur :
                      </Form.Label>
                      <Form.Control as="select">
                        <option>vert</option>
                        <option>noir</option>
                        <option>anthracite</option>
                      </Form.Control>
                    </Form.Group>
                  </Form.Row>
                  <Form.Row>
                    <Form.Label className="demdevisLabel">Détails :</Form.Label>
                  </Form.Row>
                  <Form.Row>
                    <Col>
                      <Form.Check
                        type="checkbox"
                        label="Portillon"
                        id="idcheck"
                      />
                    </Col>
                    <Col>
                      <Form.Check
                        type="checkbox"
                        label="Soubassement(+20cm)"
                        id="idcheck"
                      />
                    </Col>
                  </Form.Row>

                  <Form.Row>
                    <Form.Label className="demdevisLabel">
                      Type de sol du lieu de pose :
                    </Form.Label>
                  </Form.Row>
                  <Form.Row>
                    <Form.Control
                      type="string"
                      placeholder="ex: terre/béton/pavé/gravier/mixte"
                    />
                  </Form.Row>
                  <Form.Row>
                    <Form.Label className="demdevisLabel">
                      Importer photo :
                    </Form.Label>
                  </Form.Row>
                  <Form.File
                    id="exampleFormControlFile1"
                    // label="Example file input"
                  />
                </Form.Group>
              </Form>
            </Form>
            <Button
              // variant="primary"
              id="validbutton2"
              onClick={() => nextStep('userdata')}
            >
              Valider
            </Button>
          </div>
        )
      case 'cloture souple':
        return (
          <div>
            <div>
              <i
                className="fas fa-arrow-alt-circle-left fa-2x"
                onClick={() => prevStep('clot')}
              ></i>
            </div>
            <Form id="formDevis">
              <Form id="formDevis">
                <Form.Group>
                  <Form.Row>
                    <Form.Group as={Col} controlId="formGridEmail">
                      <Form.Label className="demdevisLabel">
                        Longueur :
                      </Form.Label>
                      <Form.Control type="number" placeholder="Mètres" />
                    </Form.Group>
                    <Form.Group as={Col} controlId="formGridPassword">
                      <Form.Label className="demdevisLabel">
                        Hauteur :
                      </Form.Label>
                      <Form.Control as="select" placeholder="Hauteur">
                        <option>63cm</option>
                        <option>83cm</option>
                        <option>103cm</option>
                        <option>123cm</option>
                        <option>153cm</option>
                        <option>173cm</option>
                        <option>203cm</option>
                      </Form.Control>
                    </Form.Group>
                  </Form.Row>
                  <Form.Row>
                    <Form.Group as={Col} controlId="formGridEmail">
                      <Form.Label className="demdevisLabel">
                        Nbre de coin :
                      </Form.Label>
                      <Form.Control type="number" />
                    </Form.Group>
                    <Form.Group as={Col} controlId="formGridPassword">
                      <Form.Label className="demdevisLabel">
                        Couleur :
                      </Form.Label>
                      <Form.Control as="select">
                        <option>vert</option>
                        <option>noir</option>
                        <option>anthracite</option>
                      </Form.Control>
                    </Form.Group>
                  </Form.Row>
                  <Form.Row>
                    <Form.Label className="demdevisLabel">Détails :</Form.Label>
                  </Form.Row>
                  <Form.Row>
                    <Col>
                      <Form.Check
                        type="checkbox"
                        label="Portillon"
                        id="idcheck"
                      />
                    </Col>
                    <Col>
                      <Form.Check
                        type="checkbox"
                        label="Soubassement(+20cm)"
                        id="idcheck"
                      />
                    </Col>
                  </Form.Row>

                  <Form.Row>
                    <Form.Label className="demdevisLabel">
                      Type de sol du lieu de pose :
                    </Form.Label>
                  </Form.Row>
                  <Form.Row>
                    <Form.Control
                      type="string"
                      placeholder="ex: terre/béton/pavé/gravier/mixte"
                    />
                  </Form.Row>
                  <Form.Row>
                    <Form.Label className="demdevisLabel">
                      Importer photo :
                    </Form.Label>
                  </Form.Row>
                  <Form.File
                    id="exampleFormControlFile1"
                    // label="Example file input"
                  />
                </Form.Group>
              </Form>
            </Form>
            <Button
              // variant="primary"
              id="validbutton2"
              onClick={() => nextStep('userdata')}
            >
              Valider
            </Button>
          </div>
        )

      case 'userdata':
        return (
          <div>
            <div>
              <i
                className="fas fa-arrow-alt-circle-left fa-2x"
                onClick={() => prevStep(window.last)}
              ></i>
            </div>

            <Form>
              <Form.Group>
                <Form.Row>
                  <Col>
                    <Form.Label className="demdevisLabel">
                      Nom et prénom
                    </Form.Label>
                    <Form.Row>
                      <Form.Control
                        className="inputuserdata"
                        type="string"
                        Name="name"
                        value={userdata.name}
                        onChange={onChange4}
                        onBlur={onBlur4}
                        isInvalid={!!errors.name}
                      />
                      <Form.Control.Feedback type="invalid">
                        {errors.name}
                      </Form.Control.Feedback>
                    </Form.Row>
                    <Form.Row>
                      <Form.Label className="demdevisLabel">
                        Code Postal
                      </Form.Label>
                    </Form.Row>
                    <Form.Row>
                      <Form.Control
                        className="inputuserdata"
                        type="number"
                        Name="codePostal"
                        value={userdata.codePostal}
                        onChange={onChange4}
                        onBlur={onBlur4}
                        isInvalid={!!errors.codePostal}
                      />
                      <Form.Control.Feedback type="invalid">
                        {errors.codePostal}
                      </Form.Control.Feedback>
                    </Form.Row>
                  </Col>
                  <Col>
                    <Form.Label className="demdevisLabel">Email</Form.Label>
                    <Row>
                      <Form.Control
                        className="inputuserdata"
                        type="email"
                        Name="email"
                        value={userdata.email}
                        onChange={onChange4}
                        onBlur={onBlur4}
                        isInvalid={!!errors.email}
                      />
                      <Form.Control.Feedback type="invalid">
                        {errors.email}
                      </Form.Control.Feedback>
                    </Row>
                    <Form.Row>
                      <Form.Label className="demdevisLabel">
                        Téléphone
                      </Form.Label>
                    </Form.Row>
                    <Form.Row>
                      <Form.Control
                        className="inputuserdata"
                        type="number"
                        Name="tel"
                        value={userdata.tel}
                        onChange={onChange4}
                        onBlur={onBlur4}
                        isInvalid={!!errors.tel}
                      />
                      <Form.Control.Feedback type="invalid">
                        {errors.tel}
                      </Form.Control.Feedback>
                    </Form.Row>
                  </Col>
                </Form.Row>
                <Form.Row>
                  <Col></Col>
                  <Col></Col>
                </Form.Row>

                <Form.Row>
                  <Form.Label className="demdevisLabel">
                    Commentaires{' '}
                  </Form.Label>
                </Form.Row>
                <Form.Row>
                  <textarea
                    id="commentaire"
                    Name="commentaire"
                    style={{ height: '88px' }}
                    value={formdata.commentaire}
                    rows="5"
                    onChange={onChange}
                    placeholder="Indiquer toutes informations que vous jugez utiles"
                  />
                </Form.Row>
              </Form.Group>
            </Form>
            <Button id="validbutton" onClick={onSubmit}>
              Envoyer
            </Button>
          </div>
        )
      default:
    }
  }

  return (
    <div className={'demnormouter'}>
      <div className={'demnorm'}> {body()}</div>
    </div>
  )
}

DemandeDevis.propTypes = {
  hideModal: PropTypes.func.isRequired,
  uploadDemande: PropTypes.func.isRequired,
  testmail: PropTypes.func.isRequired,
  modal: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired
}
const mapStateToProps = (state) => ({
  modal: state.modal,
  auth: state.auth
})

export default connect(mapStateToProps, { hideModal, uploadDemande, testmail })(
  DemandeDevis
)
