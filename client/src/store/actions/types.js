import keyMirror from 'keymirror'

export const ActionTypes = keyMirror({
  HIDE_MODAL: null,
  SHOW_MODAL: null,
  HIDE_MODAL_USER: null,
  SHOW_MODAL_USER: null
})
export const ModalEmail = keyMirror({
  HIDE_EMAIL: null,
  SHOW_EMAIL: null,
  SEND_EMAIL: null
})

export const GET_YEAR_COMPTA = 'GET_YEAR_COMPTA'
export const COMPTA_ERROR = 'COMPTA_ERROR'
export const GET_COMPTAS = 'GET_COMPTAS'

export const CLEAR_DEVIS = 'CLEAR_DEVIS'
export const CLEAR_DEMANDE = 'CLEAR_DEMANDE'
export const CLEAR_FACTURE = 'CLEAR_FACTURE'
export const CLEAR_CHANTIER = 'CLEAR_CHANTIER'
export const CLEAR_EVENT = 'CLEAR_EVENT'

export const GET_FACTURE = 'GET_FACTURE'
export const ADD_FACTURE = 'ADD_FACTURE'
export const GET_FACTURES = 'GET_FACTURES'
export const UPDATE_FACTURE = 'UPDATE_FACTURE'
export const FACTURE_ERROR = 'FACTURE_ERROR'

export const GET_PRICE = 'GET_PRICE'
export const GET_PRICES = 'GET_PRICES'
export const ADD_PRICE = 'ADD_PRICE'
export const EDIT_PRICE = 'EDIT_PRICE'
export const DELETE_PRICE = 'DELETE_PRICE'
export const PRICE_ERROR = 'PRICE_ERROR'

export const UPDATE_CHANTIER = 'UPDATE_CHANTIER'
export const GET_ALLCHANTIER = 'GET_ALLCHANTIER'
export const GET_CHANTIER = 'GET_CHANTIER'
export const DELETE_CHANTIER = 'DELETE_CHANTIER'
export const CHANTIER_ERROR = 'CHANTIER_ERROR'

export const HIDE_CALENDAR = 'HIDE_CALENDAR'
export const SHOW_CALENDAR = 'SHOW_CALENDAR'
export const GET_EVENTS = 'GET_EVENTS'
export const GET_EVENT = 'GET_EVENT'
export const ADD_EVENT = 'CREATE_EVENT'
export const UPDATE_EVENT = 'UPDATE_EVENT'
export const DELETE_EVENT = 'DELETE_EVENT'
export const EVENT_ERROR = 'EVENT_ERROR'

export const GET_USERS = 'GET_USERS'
export const USER_ERROR = 'USER_ERROR'

export const ADD_DEVIS = 'ADD_DEVIS'
export const UPDATE_DEVIS = 'UPDATE_DEVIS'
export const DELETE_DEVIS = 'DELETE_DEVIS'
export const GET_ALLDEVIS = 'GET_ALLDEVIS'
export const GET_DEVIS = 'GET_DEVIS'
export const SET_NUMBERDEVIS = 'SET_NUMBERDEVIS'
export const DEVIS_ERROR = 'DEVIS_ERROR'

export const STOP_REDIRECT = 'STOP_REDIRECT'

export const ADD_DEMANDE = 'ADD_DEMANDE'
export const DELETE_DEMANDE = 'DELETE_DEMANDE'
export const GET_DEMANDE = 'GET_DEMANDE'
export const GET_DEMANDES = 'GET_DEMANDES'
export const DEMANDE_ERROR = 'DEMANDE_ERROR'

export const SET_ALERT = 'SET_ALERT'
export const REMOVE_ALERT = 'REMOVE_ALERT'

export const REGISTER_SUCCESS = 'REGISTER_SUCCESS'
export const REGISTER_FAIL = 'REGISTER_FAIL'

export const USER_LOADED = 'USER_LOADED'

export const AUTH_ERROR = 'AUTH_ERROR'

export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGIN_FAIL = 'LOGIN_FAIL'
export const LOGOUT = 'LOGOUT'

export const GET_PROFILE = 'GET_PROFILE'
export const PROFILE_ERROR = 'PROFILE_ERROR'

export const GET_PROFILES = 'GET_PROFILES'
export const CLEAR_PROFILE = 'CLEAR_PROFILE'

export const GET_REPOS = 'GET_REPOS'
export const NO_REPOS = 'NO_REPOS'
export const UPDATE_PROFILE = 'UPDATE_PROFILE'

export const ACCOUNT_DELETED = 'ACCOUNT_DELETED'
export const GET_POSTS = 'GET_POSTS'
export const GET_POST = 'GET_POST'
export const POST_ERROR = 'POST_ERROR'
export const UPDATE_LIKES = 'UPDATE_LIKES'
export const DELETE_POST = 'DELETE_POST'
export const ADD_POST = 'ADD_POST'
export const ADD_COMMENT = 'ADD_COMMENT'
export const REMOVE_COMMENT = 'REMOVE_COMMENT'
