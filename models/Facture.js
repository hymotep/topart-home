const mongoose = require('mongoose')

const FactureSchema = new mongoose.Schema({
  numero: { type: String, required: true },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user',
    required: true
  },
  chantier: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'chantier'
  },
  line: { type: Object, required: true },

  totalHtva: { type: Number, required: true },
  totalTvac: { type: Number, required: true },
  tvaAmount: { type: Number, required: true },
  tva: { type: Number, required: true },

  titre: { type: String, required: true },
  dateLivraison: { type: Date, required: true },
  date: { type: String, required: true },
  dateEcheance: { type: String, required: true },
  adresse: { type: String, required: true },
  payed: { type: Boolean, required: true },
  read: { type: Boolean, default: false }
})

module.exports = Facture = mongoose.model('facture', FactureSchema)
