const mongoose = require('mongoose')

const ToolSchema = new mongoose.Schema({
  name: { type: Object, required: true, unique: true },
  Date: { type: Date, default: Date.now }
})

/* userSchema.method('toClient', function() {
    var obj = this.toObject();
 
    //Rename fields
    obj.id = obj._id;
    delete obj._id;
 
    return obj;
}); */

module.exports = Tool = mongoose.model('tool', ToolSchema)

// var Devis = mongoose.model('devis', devisSchema);
// export default Devis;
