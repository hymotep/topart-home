import React, { Component, useEffect, Fragment } from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Spinner from '../layout/Spinner'
import { getAllPrice, addPrice, addType } from '../../store/actions/price.js'
import { tbois, crigide } from './Prix.js'
import { Table, Form, Col } from 'react-bootstrap'

const ListePrix = ({
  getAllPrice,
  addPrice,
  addType,
  price: { loading, prices },
  auth: { user }
}) => {
  useEffect(() => {
    getAllPrice()
  }, [getAllPrice])

  let data = {
    addtype: '',
    type: 'tbois',
    name: '',
    data: null,
    obj: new Object()
  }

  let [formData, setFormData] = React.useState(data)

  let onChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value })
  }
  function addAll() {
    formData.obj = new Object()
    Object.entries(crigide).map(
      // Object.entries(tbois).map(
      ([key, value]) => ((formData.obj[key] = value), addPrice(formData))
      // <div>
      //   {key} : {value === true ? 'true' : value}
      // </div>
    )
  }

  function addIt() {
    formData.data = formData.data.split(',')
    formData.type = 'tbois'
    formData.obj[formData.name] = formData.data
    addPrice(formData)
  }

  return loading ? (
    <Spinner />
  ) : (
    <Fragment>
      <div style={{ marginTop: '5px' }}>
        <input
          name="addtype"
          value={formData.addtype}
          onChange={onChange}
        ></input>

        <button onClick={() => addType(formData)}> add type </button>
      </div>

      {/* <div>
        <select name="type" value={formData.type} onChange={onChange}>
          {prices && prices.map((value, key) => <option>{value.name}</option>)}
        </select>
        <button onClick={() => addAll()}> add all </button>
      </div> */}

      <h1>Terrasse bois</h1>
      <div>
        <input
          name="name"
          placeholder="nom"
          value={formData.name}
          onChange={onChange}
        ></input>
        <input
          name="data"
          placeholder="longueur,largeur,prix"
          value={formData.data}
          onChange={onChange}
        ></input>
        <button onClick={() => addIt()}>add price</button>
      </div>
      <Table striped bordered>
        <thead>
          <tr>
            <th>Nom</th>
            <th>Longueur</th>
            <th>Largeur </th>
            <th>Prix</th>
          </tr>
        </thead>
        <tbody>
          {prices.map(
            (value, key) =>
              value.name === 'tbois' &&
              Object.entries(value.data).map(([key, value]) => (
                <tr>
                  <td>{key}</td>
                  <td>{value[0]}</td>
                  <td>{value[2] ? value[1] : ''}</td>
                  <td>{value[2] || value[1]}</td>
                </tr>
              ))
          )}
        </tbody>
      </Table>
      <h1>Cloture rigide</h1>
      <Table striped bordered>
        <thead>
          <tr>
            <th>Nom</th>
            <th>Prix cloture</th>
            <th>Prix poteau </th>
            <th>Nbre attache</th>
          </tr>
        </thead>
        {/* <tbody> */}
        {prices.map(
          (value, key) =>
            value.name === 'crigide' &&
            // key.localeCompare('test') !== 0 &&
            Object.entries(value.data).map(
              ([key, value]) =>
                key.localeCompare('test') !== 0 && (
                  <tbody>
                    <tr>
                      <td colspan="4" style={{}}>
                        {key}
                      </td>
                    </tr>

                    {Object.entries(value).map(([key, value]) => (
                      <tr>
                        <td>{key}</td>
                        <td>{value[0]} </td>
                        <td>{value[1]} </td>
                        <td>{value[2]} </td>
                      </tr>
                    ))}
                  </tbody>
                )
            )
        )}
        {/* </tbody> */}
      </Table>
    </Fragment>
  )
}

ListePrix.propTypes = {
  getCurrentProfile: PropTypes.func.isRequired,
  getAllPrice: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  price: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  price: state.price
})

export default connect(mapStateToProps, { getAllPrice, addPrice, addType })(
  ListePrix
)
