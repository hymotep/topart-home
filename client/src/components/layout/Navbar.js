import React, { Fragment } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { logout } from '../../store/actions/auth'
import logo from '../img/th21.2.png'
// import logo from '../img/lt.png'
// import logo from '../img/logo22.png'
import btest from '../img/B.png'
// import logo from '../img/logobrun.png'
import { Navbar, Nav } from 'react-bootstrap'
// import '../modals/template.css'ra
import '../../App.css'

import { showModal, showModalUser } from '../../store/actions/modal'
import { showCalendar } from '../../store/actions/event'
import { showEmail } from '../../store/actions/email'
import { stopRedirect } from '../../store/actions/auth'
import DemandeDevis from '../devis/creation/DemandeDevis'
import UserData from '../dashboard/UserData'
import Calendar from '../calendar/calendar'

const changeback = () => {
  // if (window.scrollY >= 80) {
  //   document.getElementById('navvs').style.display = 'block'
  //   console.log('test')
  // } else {
  //   document.getElementById('navvs').style.display = 'none'
  // }
}

window.addEventListener('scroll', changeback)

const NavbarComp = ({
  auth: { user, isAuthenticated, loading },
  logout,
  showModal,
  showModalUser,
  stopRedirect,
  showCalendar,
  showEmail
}) => {
  const authLinks = (
    <div className="navdroite">
      {user && user.admin ? (
        <span>
          <Link to="/dashboard" className="demdevisbutton">
            <i className="fas fa-list"></i>{' '}
            <span className="hide-sm">Dashboard</span>
          </Link>

          <Link to="/allfacture" className="demdevisbutton">
            <i className="fas fa-euro-sign"></i>{' '}
            <span className="hide-sm">Gestion des factures</span>
          </Link>
          <Link to="/chantiers" className="demdevisbutton">
            <i className="fas fa-list"></i>{' '}
            <span className="hide-sm">Mes Chantiers</span>
          </Link>
          <button onClick={showCalendar} className="demdevisbutton">
            <i className="far fa-calendar"></i>{' '}
            <span className="hide-sm">Calendar</span>
          </button>
        </span>
      ) : (
        <span>
          <button onClick={showModalUser} className="demdevisbutton">
            Mon Profil
          </button>

          <Link to="/devisfacture" className="demdevisbutton">
            Devis / Facture
          </Link>
        </span>
      )}

      <span>
        <Link to="/" onClick={showModal} className="demdevisbutton2">
          Devis
        </Link>
      </span>

      <DemandeDevis />
      <UserData />
      <Calendar />
      <a onClick={logout} href="#!">
        <i className="fas fa-sign-out-alt" />{' '}
        <span className="hide-sm">Logout</span>
      </a>
    </div>
  )
  const guestLinks = (
    <Fragment>
      {/* <div className="navdroite"> */}
      <Link
        onClick={stopRedirect}
        to="/realisation"
        style={{ color: 'white', marginRight: '10px' }}
      >
        A propos
      </Link>
      <Link onClick={stopRedirect} to="/realisation" style={{ color: 'white' }}>
        Realisation
      </Link>

      <Link
        to="/"
        onClick={showEmail}
        style={{ color: 'white', marginRight: '6px', marginLeft: '6px' }}
      >
        Contact
      </Link>
      <Link
        to="/"
        onClick={showModal}
        className="demdevisbutton2"
        id="nonmobidev"
      >
        Devis Gratuit
      </Link>
      <Link to="/demandedevis" className="demdevisbutton" id="mobidev">
        <span className="hide-sm">Devis Gratuit version mobile</span>
      </Link>

      <DemandeDevis />

      <Link to="/login" style={{ color: 'white' }}>
        <i className="fa fa-sign-in" aria-hidden="true"></i>
      </Link>
      {/* </div> */}
    </Fragment>
  )

  return (
    <div>
      <Link to="/">
        <img
          id="imglogo"
          src={logo}
          alt="..."
          style={{
            width: '40px',
            height: '40px',
            top: '4px',
            left: '4px',
            position: 'fixed',
            zIndex: '5'
          }}
        />
      </Link>

      <Navbar
        collapseOnSelect
        expand="lg"
        variant="dark"
        className="  fixed-top  navbar1  "
      >
        <Fragment>
          {isAuthenticated ? (
            <div></div>
          ) : (
            <div>
              {/* <img
                id="imglogo"
                src={logo}
                alt="..."
                style={{
                  width: '40px',
                  height: '40px',
                  top: '2px',
                  left: '2px'
                }}
              /> */}
              <div
                className="navbar-text "
                id="navtext"
                style={{ marginLeft: '40px' }}
              >
                <em className="hide-sm">
                  {' '}
                  <a>Topart Home</a>
                </em>
                <span>
                  <a
                    style={{ marginLeft: 4, color: 'white' }}
                    href="tel://+32471906347"
                  >
                    <i className="fa fa-phone" aria-hidden="true"></i>{' '}
                    +32/471.90.63.74
                  </a>
                </span>
                <span style={{ marginLeft: 10 }}>
                  <i className="fa fa-envelope" aria-hidden="true"></i>
                  <em
                    className="hide-sm"
                    onClick={() => {
                      navigator.clipboard.writeText(this.state.textToCopy)
                    }}
                  >
                    {' '}
                    info@toparthome.com
                  </em>
                </span>
                <span style={{ margin: 2 }}>
                  <a
                    style={{ marginLeft: 8, color: 'white' }}
                    className="fa fa-facebook"
                    href="https://www.facebook.com/TopartHome"
                  ></a>
                  <a
                    style={{ marginLeft: 8 }}
                    className="fa fa-instagram"
                    aria-hidden="true"
                  ></a>
                </span>
              </div>
            </div>
          )}
        </Fragment>

        <Navbar.Toggle aria-controls="responsive-navbar-nav " id="togglepos" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto"></Nav>
          <Nav>{isAuthenticated ? authLinks : guestLinks}</Nav>
        </Navbar.Collapse>
      </Navbar>
    </div>
  )
}

NavbarComp.propTypes = {
  logout: PropTypes.func.isRequired,
  showModal: PropTypes.func.isRequired,
  showEmail: PropTypes.func.isRequired,
  showModalUser: PropTypes.func.isRequired,
  showCalendar: PropTypes.func.isRequired,
  stopRedirect: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  event: state.event
})

export default connect(mapStateToProps, {
  logout,
  showModal,
  showModalUser,
  stopRedirect,
  showCalendar,
  showEmail
})(NavbarComp)
