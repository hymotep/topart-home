import React, { useEffect, Fragment } from 'react'
import PropTypes from 'prop-types'

import { connect } from 'react-redux'
import { getAllChantier } from '../../store/actions/chantier.js'
import { getThisYearCompta } from '../../store/actions/compta.js'

import { Table } from 'react-bootstrap'

import { Link } from 'react-router-dom'
import { showModalUser } from '../../store/actions/modal'

const AllChantier = ({
  getAllChantier,
  chantier: { chantiers },
  auth: { loading, user, isAuthenticated },
  compta: { yearcompta },
  getThisYearCompta
}) => {
  useEffect(() => {
    getAllChantier()
  }, [getAllChantier])
  useEffect(() => {
    getThisYearCompta()
  }, [getThisYearCompta])

  let [search, setsearch] = React.useState({ search: '' })

  const myFunction = (e) => {
    setsearch({ ...search, search: e.target.value })
  }

  return (
    <Fragment>
      {!loading ? (
        chantiers &&
        yearcompta && (
          <Fragment>
            {/* <h3>Reste à etre payé au total :</h3> */}
            <div>Total Htva : {yearcompta.totalHtva}</div>
            <div>Total tva :{yearcompta.tvaAmount}</div>
            <input
              type="text"
              id="myInput"
              value={search.search}
              onChange={myFunction}
              // onkeyup={myFunction}
              placeholder="Recherches"
            />
            <h3>Mes chantiers </h3>
            <div id="tablechantier">
              <Table striped bordered hover id="myTable">
                <thead>
                  <tr>
                    <th>Num</th>
                    <th>Date Début</th>
                    <th>User</th>
                    <th>Reste à payer</th>
                    <th>Link</th>
                  </tr>
                </thead>
                <tbody>
                  {chantiers &&
                    chantiers.map((value, key) =>
                      // (value.dateDeb &&
                      //   value.dateDeb
                      //     .substring(0, 10)
                      //     .toLowerCase()
                      //     .includes(
                      //       search.search || search.search.toLowerCase()
                      //     )) ||
                      value.user.name &&
                      value.user.name
                        .toLowerCase()
                        .includes(
                          search.search || search.search.toLowerCase()
                        ) ? (
                        //     ||
                        // (value.user.email &&
                        //   value.user.email
                        //     .toLowerCase()
                        //     .includes(
                        //       search.search || search.search.toLowerCase()
                        //     )) ||
                        // (value.numero &&
                        //   value.numero
                        //     .toString()
                        //     .includes(
                        //       search.search || search.search.toLowerCase()
                        //     ))
                        <tr key={value.numero}>
                          <td>{value.numero} </td>
                          <td>{value.dateDeb.substring(0, 10)} </td>
                          <td>
                            {value.user.name} / {value.user.email}
                          </td>
                          <td>{value.stillToPay}€</td>

                          <td>
                            {user && !loading && (
                              <Link
                                id="linkresize"
                                to={`/chantier/${value._id}`}
                                className="btn btn-primary btnlist"
                              >
                                <i className="fas fa-plus" />
                              </Link>
                            )}

                            {/* {user && !loading && user.admin === true && (
                              <div>
                                <button
                                  onClick={() => deleteDevis(value._id)}
                                  type="button"
                                  className="btn btn-danger btnlist"
                                >
                                  <i className="fas fa-times" />
                                </button>
                              </div>
                            )} */}
                          </td>
                        </tr>
                      ) : (
                        <tr></tr>
                      )
                    )}
                </tbody>
              </Table>
            </div>
          </Fragment>
        )
      ) : (
        <div></div>
      )}
    </Fragment>
  )
}

AllChantier.propTypes = {
  getAllChantier: PropTypes.func.isRequired,
  getThisYearCompta: PropTypes.func.isRequired,
  chantier: PropTypes.object.isRequired,
  compta: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  chantier: state.chantier,
  auth: state.auth,
  compta: state.compta
})

export default connect(mapStateToProps, {
  getAllChantier,
  showModalUser,
  getThisYearCompta
})(AllChantier)
