import { setAlert } from './alert'
import {
  GET_PRICES,
  ADD_PRICE,
  DELETE_PRICE,
  EDIT_PRICE,
  PRICE_ERROR
} from './types'
import api from '../../outils/api'

//Get one price
export const getAPrice = (search) => async (dispatch) => {
  try {
    const body = JSON.stringify(search)
    const res = await api.post('/price/few/', body)
    dispatch({
      type: GET_PRICES,
      payload: res.data
    })
  } catch (err) {
    dispatch({
      type: PRICE_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    })
  }
}
//Get all prices
export const getAllPrice = () => async (dispatch) => {
  try {
    const res = await api.get('/price')
    dispatch({
      type: GET_PRICES,
      payload: res.data
    })
  } catch (err) {
    dispatch({
      type: PRICE_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    })
  }
}

// export const addType = (formData) => async (dispatch) => {
//   try {
//     const res = await api.post('/price', formData)

//     dispatch(setAlert('Type Added', 'success'))
//   } catch (err) {
//     const errors = err.response.data.errors

//     if (errors) {
//       errors.forEach((error) => dispatch(setAlert(error.msg, 'danger')))
//     }
//   }
// }
export const addPrice = (formData, update) => async (dispatch) => {
  try {
    const res = await api.post('/price', formData)

    if (update === true) {
      dispatch({
        type: EDIT_PRICE,
        payload: res.data
      })
    } else {
      dispatch({
        type: ADD_PRICE,
        payload: res.data
      })
    }

    dispatch(setAlert('Price Added', 'success'))
  } catch (err) {
    const errors = err.response.data.errors

    if (errors) {
      errors.forEach((error) => dispatch(setAlert(error.msg, 'danger')))
    }
  }
}

// Delete price
export const deletePrice = (id) => async (dispatch) => {
  try {
    await api.delete(`/price/${id}`)

    dispatch({
      type: DELETE_PRICE,
      payload: id
    })

    dispatch(setAlert('Prix Supprimé', 'success'))
  } catch (err) {}
}
