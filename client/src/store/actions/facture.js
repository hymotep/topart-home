import {
  GET_FACTURE,
  GET_FACTURES,
  ADD_FACTURE,
  UPDATE_FACTURE,
  FACTURE_ERROR,
  GET_CHANTIER
} from './types'
import api from '../../outils/api'

// Get all facture by chantier  id
export const getFactureByChantier = (id) => async (dispatch) => {
  try {
    const res = await api.get(`/facture/chantier/${id}`)

    dispatch({
      type: GET_FACTURES,
      payload: res.data
    })
  } catch (err) {
    dispatch({
      type: FACTURE_ERROR,
      payload: { msg: err.response }
    })
  }
}
// Get facture by id
export const getFactureById = (id) => async (dispatch) => {
  try {
    const res = await api.get(`/facture/${id}`)

    dispatch({
      type: GET_FACTURE,
      payload: res.data
    })
  } catch (err) {
    dispatch({
      type: FACTURE_ERROR,
      payload: { msg: err.response }
    })
  }
}

//enregistrer facture

export const saveFacture = (data) => async (dispatch) => {
  const body2 = JSON.stringify(data)
  try {
    const res2 = await api.post('/facture/', body2)

    let tempName = data.email.split('@')
    let mail = {
      title: 'Devis',
      // email:data.user.email,
      email: 'julien.topart@hotmail.com',
      name: data.name,
      comment:
        "<div>Votre facture est disponible sur le site <a href='http://localhost:3000'>Topart Home</a></div>" +
        "<div>Connectez vous via votre email, votre mot de passe provisoire est ' " +
        tempName[0] +
        "th21 ' </div>",
      send: true
    }
    let body = JSON.stringify(mail)
    const res = await api.post('/email/', body)
  } catch (err) {
    dispatch({
      type: FACTURE_ERROR,
      payload: { msg: err.response }
    })
  }
}
//enregistrer facture custom

export const saveFactureCustom = (form, registered) => async (dispatch) => {
  const body2 = JSON.stringify(form)
  try {
    if (!registered) {
      let name = form.user.name
      let email = form.user.email
      let temp = form.user.email.split('@')

      let password = temp[0] + 'th21'
      const body2 = JSON.stringify({ name, email, password })
      await api.post('/users', body2)
    }
    const res2 = await api.post('/facture/custom', body2)

    dispatch({
      type: ADD_FACTURE,
      payload: res2.data
    })
  } catch (err) {
    dispatch({
      type: FACTURE_ERROR,
      payload: { msg: err.response }
    })
  }
}

// Get all facture
export const getAllFacture = () => async (dispatch) => {
  try {
    const res = await api.get('/facture/')

    dispatch({
      type: GET_FACTURES,
      payload: res.data
    })
  } catch (err) {
    dispatch({
      type: FACTURE_ERROR,
      payload: { msg: err.response }
    })
  }
}
// Valider,invalider paiement facture
export const factureIsPayed = (form) => async (dispatch) => {
  try {
    const body = JSON.stringify(form)
    const res = await api.put('/facture/', body)
    dispatch({
      type: UPDATE_FACTURE,
      payload: res.data
    })
    dispatch({
      type: GET_CHANTIER,
      payload: res.data.chantier
    })
  } catch (err) {
    dispatch({
      type: FACTURE_ERROR,
      payload: { msg: err.response }
    })
  }
}

// Get all facture by user
export const getFactureByUser = (id) => async (dispatch) => {
  try {
    const res = await api.get(`/facture/user/${id}`)

    dispatch({
      type: GET_FACTURES,
      payload: res.data
    })
  } catch (err) {
    dispatch({
      type: FACTURE_ERROR,
      payload: { msg: err.response }
    })
  }
}
