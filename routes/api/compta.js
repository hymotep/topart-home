const express = require('express')
const router = express.Router()
const Compta = require('../../models/Compta.js')
const Devis = require('../../models/Devis.js')
const demandeDevis = require('../../models/DemandeDevis.js')
const checkObjectId = require('../../middleware/checkObjectId')
const { body } = require('express-validator')

const admin = require('../../middleware/admin.js')
const auth = require('../../middleware/auth.js')

//get compta par année
router.get('/:year', admin, async (req, res) => {
  try {
    const compta = await Compta.findOne({ year: req.params.year })
    res.json(compta)
  } catch (err) {
    console.error(err.message)
    res.status(500).send('Server Error')
  }
})
module.exports = router

//get stat
router.get('/stat/:month', admin, async (req, res) => {
  try {
    let year = new Date().getFullYear().toString()
    // let deb = '2021-' + req.params.month + '-01'
    // let fin = '2021-' + req.params.month + '-31'
    let deb = year + '-01-01'
    let fin = year + '-12-31'
    let dev = [
      { name: 'jan', total: 0, accepte: 0, totalHtva: 0, accepteHtva: 0 },
      { name: 'fev', total: 0, accepte: 0, totalHtva: 0, accepteHtva: 0 },
      { name: 'mars', total: 0, accepte: 0, totalHtva: 0, accepteHtva: 0 },
      { name: 'avril', total: 0, accepte: 0, totalHtva: 0, accepteHtva: 0 },
      { name: 'mai ', total: 0, accepte: 0, totalHtva: 0, accepteHtva: 0 },
      { name: 'juin', total: 0, accepte: 0, totalHtva: 0, accepteHtva: 0 },
      { name: 'juil', total: 0, accepte: 0, totalHtva: 0, accepteHtva: 0 },
      { name: 'aout', total: 0, accepte: 0, totalHtva: 0, accepteHtva: 0 },
      { name: 'sept', total: 0, accepte: 0, totalHtva: 0, accepteHtva: 0 },
      { name: 'oct', total: 0, accepte: 0, totalHtva: 0, accepteHtva: 0 },
      { name: 'nov', total: 0, accepte: 0, totalHtva: 0, accepteHtva: 0 },
      { name: 'dec', total: 0, accepte: 0, totalHtva: 0, accepteHtva: 0 }
    ]
    const devret = await Devis.find({
      date: {
        $gte: deb,
        $lt: fin
      }
    })
    let tmp
    devret.forEach((elem) => {
      tmp = elem.date.getMonth()
      dev[tmp].totalHtva += elem.totalHtva
      dev[tmp].total += 1
      if (elem.accepted) {
        dev[tmp].accepte += 1
        dev[tmp].accepteHtva += elem.totalHtva
      }
    })
    res.json({
      devis: dev
    })
  } catch (err) {
    console.error(err.message)
    res.status(500).send('Server Error')
  }
})
module.exports = router
