const mongoose = require('mongoose')

const DevisSchema = new mongoose.Schema({
  titre: { type: String, required: true },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user',
    required: true
  },
  demande: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'demandeDevis'
  },
  numero: { type: Number, required: true },
  lineAuto: { type: Object },
  lineExtra: { type: Array, required: true },
  totalHtva: { type: Number, required: true },
  totalTvac: { type: Number, required: true },
  tvaAmount: { type: Number, required: true },
  tva: { type: Number, required: true },
  extra: { type: Object },

  send: { type: Boolean, required: true },
  accepted: { type: Boolean, default: false },
  dateDeb: { type: Date },
  dateFin: { type: Date },
  date: {
    type: Date,
    default: Date.now
  },
  adresse: { type: String },
  read: { type: Boolean, default: false },
  pdf: { type: Boolean, default: false }
})

module.exports = Devis = mongoose.model('devis', DevisSchema)
