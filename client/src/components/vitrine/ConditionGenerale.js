import React from 'react'

const ConditionGenerale = (props) => {
  return (
    <div>
      <div>
        <button onClick={() => window.history.back()}>Retour</button>
      </div>

      <h2>CONDITIONS GENERALES POUR TRAVAUX DE CONSTRUCTION</h2>

      <div>
        Article 1. Les présentes conditions générales sont applicables à
        l’ensemble des rapports contractuels entre les parties et prévaudront
        sur toutes autres conditions générales même si celles-ci devaient
        spécifier l’inverse.
      </div>
      <div>
        Article 2. Les offres de prix communiquées par la SOCIETE, soit par
        devis, soit par simple courrier, demeurent valables durant 30 jours à
        compter de leur envoi. La signature du devis par le client ou le
        paiement de l’acompte valent acceptation de l’offre. En cas de hausse du
        prix des matières premières, salaires, charges sociales, et taxes, la
        SOCIETE pourra appliquer un supplément de prix. Dans cette hypothèse,
        cette dernière notifiera par écrit ce supplément au client préalablement
        à l’entame des travaux.
      </div>
      <div>
        Article 3. La responsabilité de la SOCIETE ne pourra jamais être engagée
        sur pied des articles 1384 et 1797 du code civil pour un montant
        supérieur au double du prix des travaux. Seul le préjudice direct du
        client sera indemnisé à l’exclusion des dommages indirects ou par
        répercussion. Par ailleurs, tous les événements présentant pour la
        SOCIETE un obstacle insurmontable à l’exécution normale de ses
        obligations et le contraignant à suspendre temporairement ou
        définitivement ses travaux ne pourront donner lieu à l’octroi de
        dommages et intérêts au profit du client. Ils ne pourront davantage
        entraîner la résolution du contrat aux torts de la SOCIETE. En outre,
        les offres étant basée sur les informations fournies par le client, la
        SOCIETE ne pourra en aucune façon être tenue responsable des désordres
        résultant d’un défaut d’informations, d’informations incomplètes ou
        inexactes.
      </div>
      <div>
        Article 4. Les délais ne sont renseignés qu’à titre indicatif. Ils
        seront suspendus pour tous cas de force majeure, en cas de non-respect
        des conditions de paiement, ou si les renseignements à fournir par le
        client n’ont pas été transmis à temps, s’avèrent incomplets ou inexacts.
        Le retard dans l’exécution des travaux ne sera susceptible d’engendrer
        l’octroi de dommages et intérêts que s’il est incontestablement démontré
        qu’il découle d’une faute lourde dans le chef de la SOCIETE. Le client
        ne pourra invoquer les délais pour demander la résolution du contrat,
        réclamer des dommages et intérêts ou faire valoir toute autre
        revendication.
      </div>
      <div>
        Article 5. Les factures sont payables en euros à l’adresse de la SOCIETE
        au comptant. Le règlement du prix des travaux se fera comme suit : - 50
        % avant le début des travaux - 40% à la mise sous tension - 10% à la
        réception électrique Les factures non soldées à l’échéance produiront,
        de plein droit et sans mise en demeure, un intérêt équivalent à celui
        prévu par la loi du 2 août 2002 relative à la lutte contre les retards
        de paiement dans les transactions commerciales. Une indemnité
        forfaitaire de dommages et intérêts de 15% du montant resté impayé, avec
        un minimum de 150,00 €, sera également due de plein droit et sans mise
        en demeure. En outre, tout retard de paiement entraînera de plein droit
        et sans mise en demeure au préalable, la suspension des travaux. Dans
        cette hypothèse, la SOCIETE se réserve le droit de résoudre le ou les
        contrats en cours et ceux dont font preuve les factures impayées.
        Ceux-ci seront résolus de plein droit et sans mise en demeure préalable
        par la seule notification de la volonté de la SOCIETE au client par
        lettre recommandée à la poste ; ceci, sans préjudice du droit de la
        SOCIETE d’exiger l’entière exécution des contrats en cours. Les
        marchandises restent la propriété de la SOCIETE jusqu’au complet
        paiement du prix. Les risques affectant les marchandises présentes chez
        le client pour la réalisation des chantiers sont supportées par celui-ci
        durant toute la durée de leur présence.
      </div>
      <div>
        Article 6. Pour être valable, toute réclamation relative à une facture
        devra être transmise par écrit, et par recommandé, à l’adresse de la
        SOCIETE, vingt jours calendrier après sa réception. A défaut, le client
        ne pourra plus contester la facture.
      </div>
      <div>
        Article 7. Toute annulation ou renonciation à l’exécution de travaux ou
        parties de ceux-ci par le client, entrainera immédiatement le paiement
        du solde du prix des travaux réalisés, outre une indemnité correspondant
        à 30% du montant des travaux qui auront été décommandés.
      </div>
      <div>
        Article 8. L’envoi de la facture reprenant le solde final vaut demande
        de réception si celle-ci n’a pas été réalisée antérieurement. A défaut
        de réclamation par lettre recommandée dans les vingt jours à dater de la
        facturation, les travaux seront considérés comme réceptionnés de manière
        définitive et sans réserve.
      </div>
      <div>
        Article 9. En cas de litige, autre que le recouvrement de factures
        impayées, les parties s’engagent à préalablement recourir à une
        médiation en désignant un médiateur agrée et à participer à deux séances
        de deux heures minimum aux fins de tenter de trouver une solution
        amiable.
      </div>
      <div>
        Article 10. Tous les litiges seront de la compétence exclusive des
        juridictions de l’arrondissement judiciaire de Bruxelles, même en cas
        d’appel à garantie ou de pluralité de défendeurs. La SOCIETE se réserve
        toutefois le droit de citer devant le Juge du siège du ou de l’un des
        défendeurs. Aucun mode de paiement ou d’exécution n’apportera novation
        ou dérogation à la présente clause expresse d’attribution exclusive de
        compétence. Le droit belge sera seul applicable.
      </div>
    </div>
  )
}

ConditionGenerale.propTypes = {}

export default ConditionGenerale
