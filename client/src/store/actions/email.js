import { ModalEmail } from './types'

import api from '../../outils/api'

export const showEmail = () => (dispatch) => {
  dispatch({
    type: ModalEmail.SHOW_EMAIL
  })
}

export const hideEmail = () => (dispatch) => {
  dispatch({
    type: ModalEmail.HIDE_EMAIL
  })
}
export const sendmail = (form) => async (dispatch) => {
  const body = JSON.stringify(form)
  const res = await api.post('/email/', body)

  //   dispatch({
  //     type: ModalEmail.SEND_EMAIL
  //   })
}
