const mongoose = require('mongoose')

const PriceSchema = new mongoose.Schema({
  type: { type: String },
  name: { type: String, required: true, unique: true },
  longueur: { type: Number },
  nombre: { type: Number },
  largeur: { type: Number },
  prixHtva: { type: Number, required: true },
  prixTvac: { type: Number, required: true },
  poteauHtva: { type: Number },
  poteauTvac: { type: Number },
  hauteur: { type: Number }
})

module.exports = Price = mongoose.model('price', PriceSchema)
