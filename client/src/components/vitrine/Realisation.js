import React from 'react'
import ReactCompareImage from 'react-compare-image'

import { Redirect } from 'react-router-dom'

import { connect } from 'react-redux'

import PropTypes from 'prop-types'

import bordera from '../img/graviera.jpg'
import border from '../img/gravier.jpg'

import { Carousel, Row, Col, Container } from 'react-bootstrap'

import terrasse2 from '../img/terasse2.jpg'
import terrasse3 from '../img/terasse3.jpg'
import terrasse from '../img/terasse.jpg'
import av1 from '../img/av1.jpg'
import ap1 from '../img/ap1.jpg'
import av2 from '../img/av2.jpg'
import ap2 from '../img/ap2.jpg'
import av3 from '../img/av3.jpg'
import ap3 from '../img/ap3.jpg'
import av4 from '../img/av4.jpg'
import ap4 from '../img/ap4.jpg'
import av5 from '../img/av5.jpg'
import ap5 from '../img/ap5.jpg'
import av6 from '../img/av6.jpg'
import ap6 from '../img/ap6.jpg'
import av7 from '../img/av7.jpg'
import ap7 from '../img/ap7.jpg'
import av8 from '../img/av8.jpg'
import ap8 from '../img/ap8.jpg'
import av9 from '../img/av9.jpg'
import ap9 from '../img/ap9.jpg'
import av10 from '../img/av10.jpg'
import ap10 from '../img/ap10.jpg'
import av11 from '../img/av11.jpg'
import ap11 from '../img/ap11.jpg'
import av12 from '../img/av12.jpg'
import ap12 from '../img/ap12.jpg'

const Realisation = ({ auth: { isAuthenticated, redirect } }) => {
  if (isAuthenticated && redirect) {
    return <Redirect to="/alldevis" />
  }
  return (
    <div>
      <div id="navvs" className="navi"></div>

      <div id="real">
        <Container>
          <Row>
            <Col sm>
              <div className="compare">
                {' '}
                <ReactCompareImage leftImage={av1} rightImage={ap1} />
              </div>{' '}
            </Col>
            <Col sm>
              <div className="compare">
                {' '}
                <ReactCompareImage leftImage={av2} rightImage={ap2} />
              </div>{' '}
            </Col>
          </Row>
          <Row>
            <Col sm>
              {' '}
              <div className="compare">
                <ReactCompareImage leftImage={av3} rightImage={ap3} />
              </div>
            </Col>
            <Col sm>
              {' '}
              <div className="compare">
                <ReactCompareImage leftImage={av4} rightImage={ap4} />{' '}
              </div>
            </Col>
          </Row>
          <Row>
            <Col sm>
              {' '}
              <div className="compare">
                <ReactCompareImage leftImage={av5} rightImage={ap5} />
              </div>
            </Col>
            <Col sm>
              {' '}
              <div className="compare">
                <ReactCompareImage leftImage={av6} rightImage={ap6} />{' '}
              </div>
            </Col>
          </Row>
        </Container>

        <div className="compare">
          <ReactCompareImage leftImage={av7} rightImage={ap7} />
        </div>
        <div className="compare">
          <ReactCompareImage leftImage={av8} rightImage={ap8} />
        </div>
        <div className="compare">
          <ReactCompareImage leftImage={av9} rightImage={ap9} />
        </div>
        <div className="compare">
          <ReactCompareImage leftImage={av10} rightImage={ap10} />
        </div>
        <div className="compare">
          <ReactCompareImage leftImage={av11} rightImage={ap11} />
        </div>
        <div className="compare">
          <ReactCompareImage leftImage={av12} rightImage={ap12} />
        </div>
        <div className="compare">
          <ReactCompareImage leftImage={bordera} rightImage={border} />
        </div>

        <Carousel
          controls={false}
          indicators={false}
          style={{ marginTop: '20px' }}
        >
          <Carousel.Item interval={1500}>
            <img className="d-block w-100" src={terrasse2} alt="First slide" />
          </Carousel.Item>
          <Carousel.Item interval={1500}>
            <img className="d-block w-100" src={terrasse3} alt="Second slide" />
          </Carousel.Item>
          <Carousel.Item interval={1500}>
            <img className="d-block w-100" src={terrasse} alt="Third slide" />
          </Carousel.Item>
        </Carousel>
      </div>
    </div>
  )
}

Realisation.propTypes = { auth: PropTypes.object.isRequired }
const mapStateToProps = (state) => ({
  auth: state.auth
})

export default connect(mapStateToProps)(Realisation)
