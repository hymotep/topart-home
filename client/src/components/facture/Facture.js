import React, { useEffect, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Table, Form, Col } from 'react-bootstrap'
import { getFactureById, factureIsPayed } from '../../store/actions/facture.js'
import Spinner from '../layout/Spinner'
import logo from '../img/toparthomelogodevis2.png'
import { jsPDF } from 'jspdf'
import html2canvas from 'html2canvas'

const Facture = ({
  facture: { facture, loading },
  getFactureById,
  factureIsPayed,
  match,
  auth: { user }
}) => {
  useEffect(() => {
    getFactureById(match.params.id)
  }, [])

  function pay() {
    let forms = { id: facture._id, payed: !facture.payed }
    factureIsPayed(forms)
  }

  function capture() {
    const pdf = new jsPDF()

    html2canvas(document.querySelector('#facturetab'), {
      scrollY: -window.scrollY,
      scrollX: -window.scrollX
    }).then((canvas) => {
      const imgData = canvas.toDataURL('image/png')

      pdf.addImage(logo, 10, 10, 80, 40)
      pdf.addImage(imgData, 'PNG', 15, 100, 170, 100)
      pdf.setFontSize(8)

      pdf.text(120, 15, 'Facture : ' + facture.numero)
      pdf.text(120, 25, 'Client : ' + facture.user.name)

      pdf.text(120, 35, 'Adresse Facturation :' + facture.adresse)

      pdf.text(
        120,
        45,
        'Date de livraison :' + facture.dateLivraison.substring(0, 10)
      )
      pdf.text(120, 55, 'Date de facturation :' + facture.date)
      pdf.text(120, 65, 'Echéance :' + facture.dateEcheance)
      pdf.save(facture.numero + '.pdf')
    })
  }

  return (
    <Fragment>
      {loading ? (
        <Spinner />
      ) : (
        facture &&
        user && (
          <div>
            <button
              onClick={() => window.history.back()}
              className="demdevisbutton2"
            >
              Retour
            </button>
            <div className="tabledevis">
              <Form.Row style={{ width: '700px' }}>
                <Col>
                  <img
                    style={{ width: '300px', height: '150px' }}
                    src={logo}
                    alt="..."
                  ></img>
                </Col>
                <Col>
                  <div>
                    <div>Facture :{facture.numero}</div>
                    <div>Nom Client :{facture.user.name}</div>
                    <div>Adresse :{facture.adresse}</div>
                    <div></div>
                    <div>
                      Date de Livraison:{facture.dateLivraison.substring(0, 10)}{' '}
                    </div>
                    <div>Date de facturation:{facture.date} </div>
                    <div>Echéance :{facture.dateEcheance} </div>
                  </div>
                </Col>
              </Form.Row>
              <Table bordered id="facturetab">
                <thead className="testing">
                  <tr>
                    <th>Designation</th>
                    <th>Prix unitaire</th>
                    <th>Quantité </th>
                    <th>Total</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td colSpan="4">{facture.titre}</td>
                  </tr>

                  {facture.line &&
                    Object.entries(facture.line).map(([key, value]) => (
                      <tr>
                        {facture.line[key].map((x) => (
                          <td>{x} </td>
                        ))}
                      </tr>
                    ))}

                  <tr>
                    <td colSpan="3">Total Htva</td>
                    <td>{facture.totalHtva} </td>
                  </tr>
                  <tr>
                    <td colSpan="3">tva de {facture.tva || 21} %</td>
                    <td>{facture.tvaAmount} </td>
                  </tr>
                  <tr>
                    <td colSpan="3">Total Tvac</td>
                    <td>{facture.totalTvac}</td>
                  </tr>
                </tbody>
              </Table>
            </div>
            <button onClick={() => capture()}>Télécharger en format pdf</button>
            {user.admin && (
              <button className="demdevisbutton2" onClick={() => pay()}>
                {facture.payed
                  ? 'Annuler paiement'
                  : 'Valider paiement facture'}
              </button>
            )}
          </div>
        )
      )}
    </Fragment>
  )
}

Facture.propTypes = {
  getFactureById: PropTypes.func.isRequired,
  factureIsPayed: PropTypes.func.isRequired,
  facture: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  facture: state.facture,
  auth: state.auth
})

export default connect(mapStateToProps, {
  getFactureById,
  factureIsPayed
})(Facture)
