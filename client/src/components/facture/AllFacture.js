import React, { useEffect, Fragment } from 'react'
import PropTypes from 'prop-types'

import { connect } from 'react-redux'
import { getAllFacture, factureIsPayed } from '../../store/actions/facture.js'

import { Table } from 'react-bootstrap'

import { Link } from 'react-router-dom'

const AllFacture = ({
  getAllFacture,
  facture: { factures, loading },
  factureIsPayed
}) => {
  useEffect(() => {
    getAllFacture()
  }, [getAllFacture])

  // let search = ''
  let [search, setsearch] = React.useState({ search: '' })

  const myFunction = (e) => {
    setsearch({ ...search, search: e.target.value })
  }
  function pay(id, payed) {
    let forms = { id: id, payed: !payed }
    factureIsPayed(forms)
  }

  return (
    <Fragment>
      {!loading ? (
        factures && (
          <Fragment>
            <input
              type="text"
              id="myInput"
              value={search.search}
              onChange={myFunction}
              // onkeyup={myFunction}
              placeholder="Recherches"
            />

            <h3>Factures </h3>
            <div id="tabledevis">
              <Table striped bordered hover id="myTable">
                <thead>
                  <tr>
                    <th>Num</th>
                    <th>Date Facturation</th>
                    <th>Client</th>
                    <th>Custom</th>
                    <th>Total Tvac</th>
                    <th>Payé</th>
                    <th>Link</th>
                  </tr>
                </thead>
                <tbody>
                  {factures &&
                    factures.map((value, key) =>
                      (value.date &&
                        value.date
                          .substring(0, 10)
                          .toLowerCase()
                          .includes(
                            search.search && search.search.toLowerCase()
                          )) ||
                      (value.user.name &&
                        value.user.name
                          .toLowerCase()
                          .includes(
                            search.search && search.search.toLowerCase()
                          )) ||
                      (value.user.email &&
                        value.user.email
                          .toLowerCase()
                          .includes(
                            search.search && search.search.toLowerCase()
                          )) ||
                      (value.numero &&
                        value.numero
                          .toString()
                          .includes(
                            search.search && search.search.toLowerCase()
                          )) ? (
                        <tr key={value.numero}>
                          <td>{value.numero} </td>
                          <td>{value.date.substring(0, 10)} </td>
                          <td>
                            {value.user.name}/{value.user.email}
                          </td>
                          <td>
                            {!value.chantier ? (
                              <i
                                className="fas fa-check fa-3x"
                                style={{ color: 'green' }}
                              />
                            ) : (
                              <div></div>
                            )}
                          </td>
                          <td>{value.totalTvac} </td>
                          <td>
                            {value.payed === true ? (
                              <div>
                                <button
                                  className="demdevisbutton2"
                                  onClick={() => pay(value._id, value.payed)}
                                >
                                  <i class="fas fa-euro-sign"></i>
                                </button>
                              </div>
                            ) : (
                              <div>
                                <button
                                  className="payno"
                                  onClick={() => pay(value._id, value.payed)}
                                >
                                  <i class="fas fa-euro-sign"></i>
                                </button>
                              </div>
                            )}
                          </td>
                          <td>
                            <Link
                              id="linkresize"
                              to={`/facture/${value._id}`}
                              className="btn btn-primary btnlist"
                            >
                              <i className="fas fa-plus" />
                            </Link>
                          </td>
                        </tr>
                      ) : (
                        <tr></tr>
                      )
                    )}
                </tbody>
              </Table>
            </div>
          </Fragment>
        )
      ) : (
        <div></div>
      )}
    </Fragment>
  )
}

AllFacture.propTypes = {
  getAllFacture: PropTypes.func.isRequired,
  factureIsPayed: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  facture: state.facture
})

export default connect(mapStateToProps, {
  getAllFacture,
  factureIsPayed
})(AllFacture)
