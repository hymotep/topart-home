import React from 'react'

import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { getAllPrice, addPrice } from '../../store/actions/price.js'

const Dashboard = ({
  getAllPrice,
  addPrice,
  addType,
  price: { loading, prices },
  auth: { user }
}) => {
  return (
    <div>
      <div style={{ marginTop: '20px' }}>
        <Link to="/alldevis" className="demdevisbutton">
          Gestion Devis
        </Link>
      </div>
      <div style={{ marginTop: '20px' }}>
        <Link to="/creationdevis" className="demdevisbutton">
          Créer Devis Personnalisé
        </Link>
      </div>
      <div style={{ marginTop: '20px' }}>
        <Link to="/creationfacturecustom" className="demdevisbutton">
          Créer Facture Custom
        </Link>
      </div>
      <div style={{ marginTop: '20px' }}>
        <Link to="/compta" className="demdevisbutton">
          Comptabilité
        </Link>
      </div>
      <div style={{ marginTop: '20px' }}>
        <Link to="/listeprix" className="demdevisbutton">
          Liste Prix
        </Link>
      </div>
    </div>
  )
}

Dashboard.propTypes = {
  getAllPrice: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  price: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  price: state.price
})

export default connect(mapStateToProps, { getAllPrice, addPrice })(Dashboard)
