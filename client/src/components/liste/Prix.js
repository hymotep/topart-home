export const tbois = {
  //Longueur,largeur,prix
  premprix: [240, 14.5, 6.95],
  meleze: [300, 14.5, 10.7], // 0.435m² -- 24.6e/m² hauteur = 2.6
  compositeb: [220, 15, 41.99], //0.99m² -- 41.99e/m² hauteur = 2.5
  compositea: [220, 15, 41.99], //0.99m² -- 41.99e/m² hauteur = 2.5
  pinthermo: [330, 11.5, 16.79], //0.379 -- 44.3 /m² hauteur =2.6
  bangkiraiL: [305, 14.5, 23.99], //0.44 --  54.5 /m² hauteur = 2.1
  bangkirai: [305, 14.5, 28.99], //0.44  -- 65.50e/m² hauteur = 2.5
  bangkiraiDFL: [396, 14.5, 36.99], //0.57 -- 65e/m² hauteur = 2.5

  //Longueur,hauteur,largueur,prix
  lambtraite: [300, 8.6, 3.5, 7.34], // 2.44 eur/mètre
  lambcomp: [290, 4, 6, 13.15], // 4.53 eur/mètre
  lambazobe: [250, 6.8, 4.5, 12.49], // 5 eur/mètre
  lambalu: [400, 5, 3, 24.99] //6.2475 eur/mètre
}
export const tboisaccess = {
  //nombre ,prix
  visMetal: [250, 50],
  visNormal: [200, 30],
  clip: [100, 26.9],

  contour: [290, 15.29],

  //taille max ,prix
  plot: { 40: 2.35, 80: 2.69, 140: 2.89, 230: 3.29 }
}
export const tpave = {}

export const crigide = {
  torino: {
    //cloture ,poteau ,nombre attache
    103: [20.5, 13.0, 2],
    123: [23.99, 14.9, 2],
    153: [29.9, 16.9, 3],
    173: [32.9, 18.9, 4],
    203: [39.9, 20.5, 4]
    //coin ,1piece,
    // attache: [2.5, 2.29],
    // coin: [2.5]
  },
  classic: {
    63: [25.89, 13.0, 2],
    103: [31.9, 13.0, 2],
    123: [36.91, 14.9, 2],
    153: [44.9, 16.9, 3],
    173: [50.9, 18.9, 4],
    203: [57.9, 20.5, 4]
    //6 pieces , 25pieces
    // attache: [13, 39.99]
  },
  nylofor2d: {
    103: [44.9, 16.7, 3], //150 poteau
    123: [52.9, 17.48, 4],
    143: [62.9, 17.48, 4],
    163: [69.99, 20.9, 4],
    183: [75.9, 26.5, 5]
    //10 piece
    // attache: [8.99, 8.99]
  }
  // attache: {
  //   milieu: 2.29,
  //   coin: 2.5
  // }
}
export const crigideAccess = {
  torino: {
    //coin ,1piece,
    attache: [2.5, 2.29],
    coin: [2.5]
  },
  classic: {
    //6 pieces , 25pieces
    attache: [13, 39.99]
  },
  nylofor2d: {
    //10 piece
    attache: [9.99, 9.99]
  }
}
export const csouple = {}
export const cbois = {}
