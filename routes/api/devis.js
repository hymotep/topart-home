const express = require('express')
const router = express.Router()
const Devis = require('../../models/Devis.js')
const User = require('../../models/User.js')
const Counters = require('../../models/Counters.js')
const Chantier = require('../../models/Chantier.js')
const Event = require('../../models/Event.js')
const auth = require('../../middleware/auth.js')
const checkObjectId = require('../../middleware/checkObjectId')
const { body } = require('express-validator')
const admin = require('../../middleware/admin.js')
const { check, validationResult } = require('express-validator')
const DemandeDevis = require('../../models/DemandeDevis.js')

//devis download
router.post('/pdf/:id', [auth, checkObjectId('id')], async (req, res) => {
  try {
    const devis = await Devis.findOneAndUpdate(
      { _id: req.params.id },
      { pdf: true },
      { new: true }
    ).populate('user', ['email', 'name', 'adresse'])
    res.json(devis)
  } catch (err) {
    console.error(err.message)
    res.status(500).send('Server Error')
  }
})
//devis read
router.post('/read/:id', [auth, checkObjectId('id')], async (req, res) => {
  try {
    const devis = await Devis.findOneAndUpdate(
      { _id: req.params.id },
      { read: true },
      { new: true }
    ).populate('user', ['email', 'name', 'adresse'])
    res.json(devis)
  } catch (err) {
    console.error(err.message)
    res.status(500).send('Server Error')
  }
})

//crée devis
router.post(
  '/',
  [
    admin,
    [
      check('titre', 'Please include a valid email').not().isEmpty(),
      check('user', 'Password is required').not().isEmpty(),
      check('demande', 'Password is required').not().isEmpty(),
      check('lineAuto', 'lineAuto is required').exists(),
      check('lineExtra', 'lineExtra is required').exists(),
      check('totalHtva', 'totalHtva is required').isNumeric(),
      check('totalTvac', 'totalTvac is required').isNumeric(),
      check('tvaAmount', 'tvaAmount is required').isNumeric(),
      check('tva', 'tva is required').isNumeric(),
      check('extra', 'extra is required').exists(),
      check('send', 'send is required').exists()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() })
    }
    try {
      const data = req.body

      devis = new Devis({
        titre: data.titre,
        user: data.user,
        demande: data.demande,
        numero: data.numero,
        lineAuto: data.lineAuto,
        lineExtra: data.lineExtra,
        totalHtva: data.totalHtva,
        totalTvac: data.totalTvac,
        tvaAmount: data.tvaAmount,
        tva: data.tva,
        extra: data.extra,
        send: data.send,
        adresse: data.adresse
      })
      // if(Number(data.numero.charAt(str.length-1)) >=1)
      await DemandeDevis.findOneAndUpdate(
        { _id: data.demande },
        { devis: true }
      )
      const dev = await devis.save()
      res.json(dev)
    } catch (err) {
      console.error(err.message)
      res.status(500).send('Server error')
    }
  }
)

//crée devis personnalisé
router.post(
  '/custom/',
  [
    admin,
    [
      check('titre', 'Please include a valid email').not().isEmpty(),
      check('user', 'user is required').not().isEmpty(),
      // check('lineAuto', 'lineAuto is required').not().isEmpty(),
      // check('lineExtra', 'lineExtra is required').not().isEmpty(),
      check('totalHtva', 'totalHtva is required').isNumeric(),
      check('totalTvac', 'totalTvac is required').isNumeric(),
      check('tvaAmount', 'tvaAmount is required').isNumeric(),
      check('tva', 'tva is required').isNumeric(),
      check('extra', 'extra is required').not().isEmpty(),
      check('send', 'send is required').exists(),
      check('adresse', 'adresse is required').not().isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() })
    }

    try {
      const data = req.body
      let temp = await User.findOne({ email: data.user.email })
      let dat = new Date().getFullYear().toString()

      let dateToday =
        new Date().getDate() +
        '/' +
        (new Date().getMonth() < 10
          ? '0' + new Date().getMonth()
          : new Date().getMonth()) +
        '/' +
        dat
      let numDevis = await Counters.findOneAndUpdate(
        { _id: 'numdevis' },
        { $inc: { sequence_value: 1 } }
      )

      devis = new Devis({
        titre: data.titre,
        user: temp._id,
        numero: dat + numDevis.sequence_value.toString() + '9',
        lineExtra: data.data,
        totalHtva: data.totalHtva,
        totalTvac: data.totalTvac,
        tvaAmount: data.tvaAmount,
        tva: data.tva,
        extra: data.extra,
        dateDeb: data.dateDeb,
        dateFin: data.dateFin,
        send: req.body.send,
        adresse: req.body.adresse
      })
      const dev = await devis.save()
      res.json(dev)
    } catch (err) {
      console.error(err.message)
      res.status(500).send('Server error')
    }
  }
)

//update devis

router.put(
  '/',
  [
    admin,
    [
      check('id', 'Please include a valid email').not().isEmpty(),
      check('lineExtra', 'lineExtra is required').exists(),
      check('totalHtva', 'totalHtva is required').isNumeric(),
      check('totalTvac', 'totalTvac is required').isNumeric(),
      check('tvaAmount', 'tvaAmount is required').isNumeric(),
      check('send', 'Password is required').exists()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() })
    }

    try {
      const data = req.body
      let devis = await Devis.findOneAndUpdate(
        { _id: req.body.id },
        {
          lineExtra: data.lineExtra,
          totalHtva: data.totalHtva,
          totalTvac: data.totalTvac,
          tvaAmount: data.tvaAmount,
          tva: data.tva,
          send: req.body.send
        },
        { new: true }
      )
      return devis
    } catch (err) {
      console.error(err.message)
      res.status(500).send('Server error')
    }
  }
)

// Get all devis
router.get('/', admin, async (req, res) => {
  try {
    const devis = await Devis.find()
      .populate('user', ['email', 'name', 'adresse'])
      .sort({ date: -1 })
    res.json(devis)
  } catch (err) {
    console.error(err.message)
    res.status(500).send('Server Error')
  }
})
//get all sended devis by client
router.get('/user/:id', [auth, checkObjectId('id')], async (req, res) => {
  try {
    const devis = await Devis.find({ user: req.params.id, send: true }).sort({
      numero: -1
    })
    res.json(devis)
  } catch (err) {
    console.error(err.message)
    res.status(500).send('Server Error')
  }
})

//Get one devis
router.get('/:id', [checkObjectId('id')], async (req, res) => {
  try {
    const devis = await Devis.findById(req.params.id).populate('user', [
      'email',
      'name',
      'adresse'
    ])

    res.json(devis)
  } catch (err) {
    console.error(err.message)

    res.status(500).send('Server Error')
  }
})

//delete devis
router.delete('/:id', [admin, checkObjectId('id')], async (req, res) => {
  try {
    //delete devis
    const devis = await Devis.find({ _id: req.params.id, accepted: false })
    await devis.remove()
    //deleete chantier
    const chant = await Chantier.findOne({ devis: req.params.id })
    //deleete event
    if (chant) {
      await Event.findOneAndRemove({ groupeId: chant._id })
      await chant.remove()
    }

    res.json({ msg: 'Devis/chantier/event supprimé ' })
  } catch (err) {
    console.error(err.message)

    res.status(500).send('Server Error')
  }
})

//renvoie le nombre de devis pour une meme demande)
router.get('/number/:id', [auth, checkObjectId('id')], async (req, res) => {
  try {
    let nbredevis = 0
    nbredevis = await Devis.find({
      demande: req.params.id
    }).countDocuments()

    res.json(nbredevis + 1)
  } catch (err) {
    console.error(err.message)
    res.status(500).send('Server Error')
  }
})

//delete all devis
router.delete('/', admin, async (req, res) => {
  try {
    await Devis.deleteMany()

    res.json({ msg: 'All devis deleted' })
  } catch (err) {
    console.error(err.message)
    res.status(500).send('Server Error')
  }
})

//sign devis
router.put(
  '/sign/',
  [
    auth,
    [
      check('id', 'id').not().isEmpty(),
      check('adresse', 'adresse is required').not().isEmpty()
    ]
  ],
  async (req, res) => {
    let { id, adresse } = req.body
    try {
      const devis = await Devis.findOne({ _id: id })

      if (devis.user.toString() !== req.user.id) {
        return res.status(401).json({ msg: 'Pas autorisé' })
      }

      devis.accepted = true
      devis.adresse = adresse
      await DemandeDevis.findOneAndRemove({ _id: devis.demande })

      await devis.save()
      await Devis.remove({ demande: devis.demande, accepted: false })

      res.json(nbredevis)
    } catch (err) {
      console.error(err.message)
      res.status(500).send('Server Error')
    }
  }
)

module.exports = router
