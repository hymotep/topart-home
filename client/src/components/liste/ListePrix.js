import React, { useEffect, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Spinner from '../layout/Spinner'
import {
  getAllPrice,
  addPrice,
  deletePrice
} from '../../store/actions/price.js'
// import { tbois, crigide, tboisaccess } from './Prix.js'
import { Table, Form, Col } from 'react-bootstrap'

const ListePrix = ({
  getAllPrice,
  addPrice,
  deletePrice,
  price: { loading, prices },
  auth: { user }
}) => {
  useEffect(() => {
    getAllPrice()
  }, [getAllPrice])

  let data = {
    type: '',
    name: '',
    longueur: '',
    largeur: '',
    prixHtva: '',
    prixTvac: '',
    hauteur: ' ',
    nombre: ''
  }
  let update = false

  let [formData, setFormData] = React.useState(data)

  const [errors, setErrors] = React.useState({})

  let onChange = (e) => {
    switch (e.target.name) {
      case 'prixHtva':
        setFormData({
          ...formData,
          [e.target.name]: e.target.value,
          prixTvac: roundDecimal(e.target.value * 1.21)
        })
        break

      case 'prixTvac':
        setFormData({
          ...formData,
          [e.target.name]: e.target.value,
          prixHtva: roundDecimal(e.target.value / 1.21)
        })
        break

      default: {
        setFormData({ ...formData, [e.target.name]: e.target.value })
      }
    }
    if (!!errors[e.target.name])
      setErrors({
        ...errors,
        [e.target.name]: null
      })
  }

  const findFormErrors = () => {
    const { name, longueur, largeur, prixHtva, prixTvac } = formData
    const newErrors = {}
    if (!name || name === '') newErrors.name = 'Insérer un nom!'
    if (!longueur || longueur === '')
      newErrors.longueur = 'Insérer une longueur!'
    if (!largeur || largeur === '') newErrors.largeur = 'Insérer une largeur!'
    if (!prixHtva || prixHtva === '' || !prixTvac || prixTvac === '')
      newErrors.prixTvac = 'Insérer Prix ht ou Tvac!'

    return newErrors
  }
  // function addAll() {
  //   Object.entries(tbois).map(([key, value]) => {
  //     let form = {
  //       type: 'tbois',
  //       name: key,
  //       longueur: value[0],
  //       largeur: value[1],
  //       prixHtva: roundDecimal(value[2] / 1.21),
  //       prixTvac: value[2]
  //     }
  //     addPrice(form)
  //   })

  //   Object.entries(tboisaccess).map(([key, value]) => {
  //     let form = {
  //       type: 'tboisaccess',
  //       name: key,
  //       nombre: value[0],
  //       prixHtva: roundDecimal(value[1] / 1.21),
  //       prixTvac: value[1]
  //     }
  //     addPrice(form)
  //   })

  //   Object.entries(crigide).map(([key, value]) => {
  //     Object.entries(value).map(([key2, value2]) => {
  //       let form = {
  //         type: 'crigide',
  //         name: key + ' ' + key2,
  //         nombre: value2[2],
  //         prixHtva: roundDecimal(value2[0] / 1.21),
  //         prixTvac: value2[0],
  //         poteauHtva: roundDecimal(value2[1] / 1.21),
  //         poteauTvac: value2[1]
  //       }
  //       addPrice(form)
  //     })
  //   })
  // }
  const type = [
    'tbois',
    'tboisaccess',
    'crigide',
    'crigideaccess',
    'cbois',
    'csouple'
  ]

  function addIt() {
    const newErrors = findFormErrors()
    // Conditional logic:
    if (Object.keys(newErrors).length > 0) {
      // We got errors!
      setErrors(newErrors)
    } else {
      addPrice(formData, update)
      setFormData(data)
      getAllPrice().then((up) => (update = false))
    }
  }
  function del(id) {
    deletePrice(id)
    setFormData(data)
    update = false
  }

  function edit(id) {
    let temp = prices.filter((item) => item._id === id)
    let resp = temp[0]
    setFormData(resp)
    update = true
  }
  function clear() {
    setFormData(data)
    update = false
  }

  function typeinput() {
    switch (formData.type) {
      case 'tbois': {
        return (
          <div id="inputprix" style={{ width: '600px' }}>
            <Form.Row>
              <Col>
                <Form.Control
                  name="name"
                  id="name"
                  placeholder="name"
                  type="string"
                  value={formData.name}
                  onChange={onChange}
                  isInvalid={!!errors.name}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.name}
                </Form.Control.Feedback>
              </Col>
              <Col>
                <Form.Control
                  name="longueur"
                  placeholder="longueur"
                  type="number"
                  value={formData.longueur}
                  onChange={onChange}
                  isInvalid={!!errors.longueur}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.longueur}
                </Form.Control.Feedback>
              </Col>
              <Col>
                <Form.Control
                  name="largeur"
                  placeholder="largeur"
                  type="number"
                  value={formData.largeur}
                  onChange={onChange}
                  isInvalid={!!errors.largeur}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.largeur}
                </Form.Control.Feedback>
              </Col>
              <Col>
                <Form.Control
                  name="prixHtva"
                  placeholder="prixHtva"
                  type="number"
                  value={formData.prixHtva}
                  onChange={onChange}
                  isInvalid={!!errors.prixHtva}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.prixHtva}
                </Form.Control.Feedback>
              </Col>
              <Col>
                <Form.Control
                  name="prixTvac"
                  placeholder="prixTvac"
                  type="number"
                  value={formData.prixTvac}
                  onChange={onChange}
                  isInvalid={!!errors.prixTvac}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.prixTvac}
                </Form.Control.Feedback>
              </Col>
            </Form.Row>
          </div>
        )
      }
      case 'tboisaccess': {
        return (
          <div id="inputprix">
            <input
              name="name"
              id="name"
              placeholder="name"
              type="string"
              value={formData.name}
              onChange={onChange}
            ></input>
            <input
              name="nombre"
              placeholder="nombre"
              type="number"
              value={formData.nombre}
              onChange={onChange}
            ></input>
            <input
              name="prixHtva"
              placeholder="prixHtva"
              type="number"
              value={formData.prixHtva}
              onChange={onChange}
            ></input>
            <input
              name="prixTvac"
              placeholder="prixTvac"
              type="number"
              value={formData.prixTvac}
              onChange={onChange}
            ></input>
          </div>
        )
      }
      case 'crigide': {
        return (
          <div id="inputprix">
            <input
              name="name"
              id="name"
              placeholder="name"
              type="string"
              value={formData.name}
              onChange={onChange}
            ></input>
            <input
              name="nombre"
              id="nombre"
              placeholder="nombre"
              type="number"
              value={formData.nombre}
              onChange={onChange}
            ></input>
            <input
              name="hauteur"
              id="hauteur"
              placeholder="hauteur"
              type="string"
              value={formData.hauteur}
              onChange={onChange}
            ></input>

            <input
              name="prixHtva"
              placeholder="prixTvac"
              type="number"
              value={formData.prixHtva}
              onChange={onChange}
            ></input>
            <input
              name="prixTvac"
              placeholder="prixTvac"
              type="number"
              value={formData.prixTvac}
              onChange={onChange}
            ></input>

            <input
              name="poteauHtva"
              placeholder="poteau Htva"
              type="number"
              value={formData.poteauHtva}
              onChange={onChange}
            ></input>
            <input
              name="poteauTvac"
              placeholder="poteau Tvac"
              type="number"
              value={formData.poteauTvac}
              onChange={onChange}
            ></input>
          </div>
        )
      }
      case 'crigideaccess': {
        return (
          <div id="inputprix">
            <input
              name="name"
              id="name"
              placeholder="name"
              type="string"
              value={formData.name}
              onChange={onChange}
            ></input>
            <input
              name="nombre"
              placeholder="nombre"
              type="number"
              value={formData.nombre}
              onChange={onChange}
            ></input>
            <input
              name="prixHtva"
              placeholder="prixHtva"
              type="number"
              value={formData.prixHtva}
              onChange={onChange}
            ></input>
            <input
              name="prixTvac"
              placeholder="prixTvac"
              type="number"
              value={formData.prixTvac}
              onChange={onChange}
            ></input>
          </div>
        )
      }
      case 'cbois': {
        break
      }
      case 'csouple': {
        break
      }
      default: {
      }
    }
  }

  function roundDecimal(number) {
    return Math.floor(number * 100) / 100
  }
  return loading ? (
    <Spinner />
  ) : (
    <Fragment>
      <button onClick={() => window.history.back()} className="demdevisbutton2">
        Retour
      </button>
      <div style={{ marginTop: '5px' }}>
        <select name="type" id="type" value={formData.type} onChange={onChange}>
          <option value=""> </option>
          {type.map((x) => (
            <option value={x} key={x}>
              {x}{' '}
            </option>
          ))}
        </select>
        {typeinput()}
        {/* <button onClick={() => addAll()}>add all</button> */}
        <button onClick={() => addIt()}>add price</button>
        <button onClick={() => clear()}>nettoyer</button>
      </div>

      <h1>Terrasse bois</h1>

      <Table striped bordered>
        <thead>
          <tr>
            <th>Nom</th>
            <th>L</th>
            <th>l</th>
            <th>Htva</th>
            <th>Tvac</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {prices &&
            prices.map(
              (value) =>
                value.type === 'tbois' && (
                  <tr key={value.name}>
                    <td>{value.name}</td>
                    <td>{value.longueur}</td>
                    <td>{value.largeur}</td>
                    <td>{value.prixHtva}</td>
                    <td>{value.prixTvac}</td>
                    <td>
                      <button onClick={() => edit(value._id)}>edit</button>
                      <button onClick={() => del(value._id)}>delete</button>
                    </td>
                  </tr>
                )
            )}
        </tbody>
      </Table>
      <h1>Terrasse accessoires </h1>

      <Table striped bordered>
        <thead>
          <tr>
            <th>Nom</th>
            <th>nombre</th>
            <th>Htva</th>
            <th>Tvac</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {prices &&
            prices.map(
              (value) =>
                value.type === 'tboisaccess' && (
                  <tr key={value.name}>
                    <td>{value.name}</td>
                    <td>{value.nombre}</td>
                    <td>{value.prixHtva}</td>
                    <td>{value.prixTvac}</td>
                    <td>
                      <button onClick={() => edit(value._id)}>edit</button>
                      <button onClick={() => del(value._id)}>delete</button>
                    </td>
                  </tr>
                )
            )}
        </tbody>
      </Table>
      <h1>Cloture rigide</h1>
      <Table striped bordered>
        <thead>
          <tr>
            <th>Nom</th>
            <th>Prix cloture</th>
            <th>Prix poteau </th>
            <th>Nbre attache</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {prices &&
            prices.map(
              (value) =>
                value.type === 'crigide' && (
                  <tr key={value.name}>
                    <td>{value.name}</td>
                    <td>{value.prixTvac}</td>
                    <td>{value.poteauTvac}</td>
                    <td>{value.nombre}</td>
                    <td>
                      <button onClick={() => edit(value._id)}>edit</button>
                      <button onClick={() => del(value._id)}>delete</button>
                    </td>
                  </tr>
                )
            )}
        </tbody>
      </Table>
    </Fragment>
  )
}

ListePrix.propTypes = {
  getAllPrice: PropTypes.func.isRequired,
  deletePrice: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  price: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  price: state.price
})

export default connect(mapStateToProps, { getAllPrice, addPrice, deletePrice })(
  ListePrix
)
