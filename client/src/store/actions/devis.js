import { setAlert } from './alert'
import {
  ADD_DEVIS,
  GET_ALLDEVIS,
  GET_DEVIS,
  DELETE_DEVIS,
  UPDATE_DEVIS,
  DEVIS_ERROR
} from './types'
import api from '../../outils/api'
import apifile from '../../outils/apifile'

function dataURItoBlob(dataURI) {
  var binary = atob(dataURI.split(',')[1])
  var array = []
  for (var i = 0; i < binary.length; i++) {
    array.push(binary.charCodeAt(i))
  }
  return new Blob([new Uint8Array(array)], { type: 'image/jpeg' })
}

export const read = (id) => async (dispatch) => {
  try {
    await api.post(`/devis/read/${id}`).then((res) =>
      dispatch({
        type: UPDATE_DEVIS,
        payload: res.data
      })
    )
  } catch (err) {
    dispatch({
      type: DEVIS_ERROR,
      payload: { msg: err.response }
    })
  }
}
export const download = (id) => async (dispatch) => {
  try {
    await api.post(`/devis/pdf/${id}`).then((res) =>
      dispatch({
        type: UPDATE_DEVIS,
        payload: res.data
      })
    )
  } catch (err) {
    dispatch({
      type: DEVIS_ERROR,
      payload: { msg: err.response }
    })
  }
}

export const sign = (file, name, id, adresse) => async (dispatch) => {
  var blob = dataURItoBlob(file)
  var fd = new FormData(document.forms[0])
  fd.append('canvasImage', blob, name + '.jpg')

  let form = { id: id, adresse: adresse }
  const body = JSON.stringify(form)
  try {
    await apifile.post('/uploadfile/uploadcanvas', fd)
    console.log('la2')

    await api.post('chantier/', body)
    console.log('la3')

    await api.put('devis/sign', body).then(
      (res2) =>
        dispatch({
          type: GET_DEVIS,
          payload: res2.data
        }),
      dispatch(setAlert('Devis signé', 'success'))
    )
  } catch (err) {
    const errors = err.response.data.errors

    if (errors) {
      errors.forEach((error) => dispatch(setAlert(error.msg, 'danger')))
    }

    dispatch({
      type: DEVIS_ERROR,
      payload: err.response
    })
  }
}

export const uploadDevisCustom = (data, registered) => async (dispatch) => {
  const body = JSON.stringify(data)

  try {
    if (!registered) {
      let name = data.user.name
      let email = data.user.email
      let temp = data.user.email.split('@')

      let password = temp[0] + 'th21'
      const body2 = JSON.stringify({ name, email, password })
      await api.post('/users', body2)
    }
    const res2 = await api.post('/devis/custom/', body).then(
      (res) =>
        dispatch({
          type: ADD_DEVIS,
          payload: res2.data
        }),
      dispatch(setAlert('Devis ajouté', 'success'))
    )

    if (data.send) {
      let tempName = data.user.email.split('@')
      let mail = {
        title: 'Devis',
        // email:data.user.email,
        email: 'julien.topart@hotmail.com',
        name: data.user.name,
        comment:
          "<div>Votre devis est disponible sur le site <a href='http://localhost:3000'>Topart Home</a></div>" +
          "<div>Connectez vous via votre email, votre mot de passe provisoire est ' " +
          tempName[0] +
          "th21 ' </div>",
        send: true
      }
      let body = JSON.stringify(mail)
      const res = await api.post('/email/', body)
    }
  } catch (err) {
    let errors = null

    if (err.response) {
      errors = err.response.data.errors
    }
    if (errors) {
      errors.forEach((error) => dispatch(setAlert(error.msg, 'danger')))
      dispatch({
        type: DEVIS_ERROR,
        payload: { msg: err.response.statusText, status: err.response.status }
      })
    }
  }
}

export const uploadDevis = (data) => async (dispatch) => {
  const body = JSON.stringify(data)

  try {
    const res = await api.post('/devis/', body).then(
      (r) =>
        dispatch({
          type: ADD_DEVIS,
          payload: res.data
        }),
      dispatch(setAlert('Devis ajouté', 'success'))
    )
    if (data.send) {
      let tempName = data.user.email.split('@')
      let mail = {
        title: 'Devis',
        // email:data.user.email,
        email: 'julien.topart@hotmail.com',
        name: data.user.name,
        comment:
          "<div>Votre devis est disponible sur le site <a href='http://localhost:3000'>Topart Home</a></div>" +
          "<div>Connectez vous via votre email, votre mot de passe provisoire est ' " +
          tempName[0] +
          "th21 ' </div>",
        send: true
      }
      let body = JSON.stringify(mail)
      const res = await api.post('/email/', body)
    }
  } catch (err) {
    dispatch({
      type: DEVIS_ERROR,
      payload: { msg: err.response }
    })
  }
}
export const updateDevis = (data) => async (dispatch) => {
  const body = JSON.stringify(data)

  try {
    await api.put('/devis/', body).then(
      (res) =>
        dispatch({
          type: UPDATE_DEVIS,
          payload: res.data
        }),
      dispatch(setAlert('Devis mis à jour', 'success'))
    )
    if (data.send) {
      let tempName = data.user.email.split('@')
      let mail = {
        title: 'Devis',
        // email:data.user.email,
        email: 'julien.topart@hotmail.com',
        name: data.user.name,
        comment:
          "<div>Votre devis est disponible sur le site <a href='http://localhost:3000'>Topart Home</a></div>" +
          "<div>Connectez vous via votre email, votre mot de passe provisoire est ' " +
          tempName[0] +
          "th21 ' </div>",
        send: true
      }
      let body = JSON.stringify(mail)
      const res = await api.post('/email/', body)
    }
  } catch (err) {
    dispatch({
      type: DEVIS_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    })
  }
}

//Get all devis
export const getAllDevis = () => async (dispatch) => {
  try {
    const res = await api.get('/devis')
    dispatch({
      type: GET_ALLDEVIS,
      payload: res.data
    })
  } catch (err) {
    dispatch({
      type: DEVIS_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    })
  }
}
//Get all devis by user
export const getAllDevisByUser = (userId) => async (dispatch) => {
  try {
    const res = await api.get(`/devis/user/${userId}`)
    dispatch({
      type: GET_ALLDEVIS,
      payload: res.data
    })
  } catch (err) {
    dispatch({
      type: DEVIS_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    })
  }
}

// Get demande by ID
export const getDevisById = (demandeId) => async (dispatch) => {
  try {
    const res = await api.get(`/devis/${demandeId}`)

    dispatch({
      type: GET_DEVIS,
      payload: res.data
    })
  } catch (err) {
    dispatch({
      type: DEVIS_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    })
  }
}

// Delete demande
export const deleteDevis = (demandeId) => async (dispatch) => {
  try {
    await api.delete(`/devis/${demandeId}`)

    dispatch({
      type: DELETE_DEVIS,
      payload: demandeId
    })

    dispatch(setAlert('Devis , chantiers Supprimé', 'success'))
  } catch (err) {
    dispatch({
      type: DEVIS_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    })
  }
}
