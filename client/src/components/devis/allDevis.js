import React, { useEffect, Fragment } from 'react'
import PropTypes from 'prop-types'

import { connect } from 'react-redux'
import { getAllDemande, deleteDemande } from '../../store/actions/demande.js'
import { getAllDevis, deleteDevis } from '../../store/actions/devis.js'
import { getAllChantier } from '../../store/actions/chantier.js'

import { Table } from 'react-bootstrap'

import { Link } from 'react-router-dom'
import { showModalUser } from '../../store/actions/modal'

const AllDevis = ({
  getAllDemande,
  deleteDemande,
  deleteDevis,
  getAllDevis,
  demande: { demandes },
  devis: { alldevis },
  auth: { loading, user, isAuthenticated },
  showModalUser
}) => {
  if (!loading && isAuthenticated && user && user.new) {
    showModalUser()
  }
  useEffect(
    () => {
      getAllDevis()
    },
    [getAllDevis],
    alldevis
  )
  useEffect(() => {
    getAllDemande()
  }, [getAllDemande])

  function typeDevis(type) {
    switch (type) {
      case 'Terrasse bois': {
        return 'devisterrassebois'
      }
      case 'cloture souple':
      case 'cloture rigide':
      case 'cloture bois': {
        return 'deviscloture'
      }
      default: {
      }
    }
  }
  // let search = ''
  let [search, setsearch] = React.useState({ search: '' })

  const myFunction = (e) => {
    setsearch({ ...search, search: e.target.value })
  }

  return (
    <Fragment>
      {!loading ? (
        alldevis &&
        demandes && (
          <Fragment>
            <input
              type="text"
              id="myInput"
              value={search.search}
              onChange={myFunction}
              // onkeyup={myFunction}
              placeholder="Recherches"
            />

            <h3>Devis </h3>
            <div id="tabledevis">
              {/* <input
                type="text"
                id="myInput"
                value={search.search}
                onChange={myFunction}
                // onkeyup={myFunction}
                placeholder="Recherches"
              /> */}
              <Table striped bordered hover id="myTable">
                <thead>
                  <tr>
                    <th>Num</th>
                    <th>Date</th>
                    <th>User</th>
                    <th>Send</th>
                    <th>Lu</th>
                    <th>pdf</th>
                    <th>Sign</th>
                    <th>Link</th>
                  </tr>
                </thead>
                <tbody>
                  {alldevis &&
                    alldevis.map((value, key) =>
                      (value.user.name &&
                        value.user.name
                          .toLowerCase()
                          .includes(
                            search.search && search.search.toLowerCase()
                          )) ||
                      (value.numero &&
                        value.numero
                          .toString()
                          .includes(
                            search.search && search.search.toLowerCase()
                          )) ? (
                        <tr key={key}>
                          <td>{value.numero} </td>
                          <td>{value.date.substring(0, 10)} </td>
                          <td>
                            {value.user.name} / {value.user.email}
                          </td>
                          <td>
                            {value.send === true ? (
                              <i
                                className="fas fa-check "
                                style={{ color: 'green' }}
                              />
                            ) : (
                              <div></div>
                            )}
                          </td>
                          <td>
                            {value.read && value.read === true ? (
                              <i
                                className="fas fa-check "
                                style={{ color: 'green' }}
                              />
                            ) : (
                              <div></div>
                            )}
                          </td>
                          <td>
                            {value.pdf && value.pdf === true ? (
                              <i
                                className="fas fa-check "
                                style={{ color: 'green' }}
                              />
                            ) : (
                              <div></div>
                            )}
                          </td>
                          <td>
                            {value.accepted === true ? (
                              <i
                                className="fas fa-check "
                                style={{ color: 'green' }}
                              />
                            ) : (
                              <div></div>
                            )}
                          </td>
                          <td>
                            {user && !loading && (
                              <Link
                                to={`/devis/${value._id}`}
                                className="btn btn-success btnlist"
                              >
                                <i className="fas fa-plus" />
                              </Link>
                            )}

                            {/* {user &&
                              !loading &&
                              user.admin === true &&
                              value.accepted === false && (
                                <div>
                                  <button
                                    onClick={() => deleteDevis(value._id)}
                                    type="button"
                                    className="btn btn-danger btnlist"
                                  >
                                    <i className="fas fa-times" />
                                  </button>
                                </div>
                              )} */}
                          </td>
                        </tr>
                      ) : (
                        <tr></tr>
                      )
                    )}
                </tbody>
              </Table>
            </div>
            <h3>Demandes </h3>
            <div id="tabledemande">
              <Table striped bordered hover id="myTable">
                <thead>
                  <tr>
                    <th>Num</th>
                    <th>Date</th>
                    <th>User</th>
                    <th>Type</th>
                    <th>Créé</th>
                    <th>Link</th>
                  </tr>
                </thead>
                <tbody>
                  {demandes &&
                    demandes.map((value, key) =>
                      (value.user.name &&
                        value.user.name
                          .toLowerCase()
                          .includes(search && search.search.toLowerCase())) ||
                      (value.numero &&
                        value.numero
                          .toString()
                          .includes(search && search.search.toLowerCase())) ||
                      (value.data.type &&
                        value.data.type
                          .toLowerCase()
                          .includes(search && search.search.toLowerCase())) ||
                      (value.data.typemat &&
                        value.data.typemat
                          .toLowerCase()
                          .includes(search && search.search.toLowerCase())) ? (
                        <tr key={key}>
                          <td>{value.numero} </td>
                          <td>{value.date.substring(0, 10)} </td>
                          <td>
                            {value.user.name} / {value.user.email}
                          </td>
                          <td>
                            {value.data.type} / {value.data.typemat}
                          </td>
                          <td>
                            {value.devis && (
                              <i
                                className="fas fa-check "
                                style={{ color: 'green' }}
                              />
                            )}
                          </td>
                          <td>
                            {user && !loading && user.admin === true && (
                              <div>
                                <Link
                                  // to={`/createdevis/${value._id}`}
                                  to={`/${typeDevis(value.data.type)}/${
                                    value._id
                                  }`}
                                  className="btn btn-success btnlist"
                                >
                                  <i className="fas fa-plus" />
                                </Link>
                                <button
                                  onClick={() => deleteDemande(value._id)}
                                  type="button"
                                  className="btn btn-danger btnlist"
                                >
                                  <i className="fas fa-times" />
                                </button>
                              </div>
                            )}
                          </td>
                        </tr>
                      ) : (
                        <tr></tr>
                      )
                    )}
                </tbody>
              </Table>
            </div>
          </Fragment>
        )
      ) : (
        <div></div>
      )}
    </Fragment>
  )
}

AllDevis.propTypes = {
  getAllDemande: PropTypes.func.isRequired,
  deleteDemande: PropTypes.func.isRequired,
  getAllDevis: PropTypes.func.isRequired,
  deleteDevis: PropTypes.func.isRequired,
  demande: PropTypes.object.isRequired,
  devis: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  demande: state.demande,
  devis: state.devis,
  auth: state.auth
})

export default connect(mapStateToProps, {
  getAllDemande,
  deleteDemande,
  getAllDevis,
  deleteDevis,
  getAllChantier,
  showModalUser
})(AllDevis)
