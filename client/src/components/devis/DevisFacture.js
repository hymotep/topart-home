import React, { useEffect, Fragment } from 'react'
import PropTypes from 'prop-types'

import { connect } from 'react-redux'
import { getAllUserDemande } from '../../store/actions/demande.js'
import { getAllDevisByUser } from '../../store/actions/devis.js'
import { getFactureByUser } from '../../store/actions/facture.js'

import { Table } from 'react-bootstrap'

import { Link } from 'react-router-dom'
import { showModalUser } from '../../store/actions/modal'

const DevisFacture = ({
  getAllUserDemande,
  getAllDevisByUser,
  getFactureByUser,
  demande: { demandes },
  facture: { factures },
  devis: { alldevis },
  auth: { loading, user, isAuthenticated },
  showModalUser
}) => {
  if (!loading && isAuthenticated && user && user.new) {
    showModalUser()
  }

  useEffect(() => {
    getAllDevisByUser(user && user._id)
  }, [getAllDevisByUser, user && user._id])
  useEffect(() => {
    getAllUserDemande(user && user._id)
  }, [getAllUserDemande, user && user._id])
  useEffect(() => {
    getFactureByUser(user && user._id)
  }, [getFactureByUser, user && user._id])

  // let search = ''
  let [search, setsearch] = React.useState({ search: '' })

  const myFunction = (e) => {
    setsearch({ ...search, search: e.target.value })
  }

  return (
    <Fragment>
      {!loading ? (
        alldevis &&
        demandes && (
          <Fragment>
            <input
              type="text"
              id="myInput"
              value={search.search}
              onChange={myFunction}
              // onkeyup={myFunction}
              placeholder="Recherches"
            />
            <h3>Mes demandes </h3>
            <div id="tabledemande">
              <Table striped bordered hover id="myTable">
                <thead>
                  <tr>
                    <th>Date demande</th>
                    <th>Type</th>
                    <th>Matériel</th>
                    <th>Surface</th>
                  </tr>
                </thead>
                <tbody>
                  {demandes &&
                    demandes.map((value, key) => (
                      <tr>
                        <td>{value.date.substring(0, 10)} </td>

                        <td>{value.data.type}</td>
                        <td>{value.data.typemat}</td>
                        <td>
                          {value.data.longueur} x {value.data.largeur}
                          {value.data.m2}
                        </td>
                      </tr>
                    ))}
                </tbody>
              </Table>
            </div>
            <h3>Mes devis </h3>
            <div id="tabledevis">
              <Table striped bordered hover id="myTable">
                <thead>
                  <tr>
                    <th>Num</th>
                    <th>Date</th>
                    <th>Accepté</th>
                    <th>Link</th>
                  </tr>
                </thead>
                <tbody>
                  {alldevis &&
                    alldevis.map((value, key) => (
                      <tr>
                        <td>{value.numero} </td>
                        <td>{value.date.substring(0, 10)} </td>

                        <td>
                          {value.accepted === true ? (
                            <i
                              className="fas fa-check fa-3x"
                              style={{ color: 'green' }}
                            />
                          ) : (
                            <div></div>
                          )}
                        </td>
                        <td>
                          {user && !loading && (
                            <Link
                              id="linkresize"
                              to={`/devis/${value._id}`}
                              className="btn btn-primary btnlist"
                            >
                              <i className="fas fa-plus" />
                            </Link>
                          )}
                        </td>
                      </tr>
                    ))}
                </tbody>
              </Table>
            </div>

            <h3>Mes factures </h3>
            <div id="tabledevis">
              <Table striped bordered hover id="myTable">
                <thead>
                  <tr>
                    <th>Num</th>
                    <th>Date</th>
                    <th>Paiement recu</th>
                    <th>Link</th>
                  </tr>
                </thead>
                <tbody>
                  {factures &&
                    factures.map((value, key) => (
                      <tr>
                        <td>{value.numero} </td>
                        <td>{value.date.substring(0, 10)} </td>

                        <td>
                          {value.accepted === true ? (
                            <i
                              className="fas fa-check fa-3x"
                              style={{ color: 'green' }}
                            />
                          ) : (
                            <div></div>
                          )}
                        </td>
                        <td>
                          {user && !loading && (
                            <Link
                              id="linkresize"
                              to={`/facture/${value._id}`}
                              className="btn btn-primary btnlist"
                            >
                              <i className="fas fa-plus" />
                            </Link>
                          )}
                        </td>
                      </tr>
                    ))}
                </tbody>
              </Table>
            </div>
          </Fragment>
        )
      ) : (
        <div></div>
      )}
    </Fragment>
  )
}

DevisFacture.propTypes = {
  getAllUserDemande: PropTypes.func.isRequired,
  getAllDevisByUser: PropTypes.func.isRequired,
  getFactureByUser: PropTypes.func.isRequired,
  demande: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  devis: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
  facture: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  demande: state.demande,
  devis: state.devis,
  auth: state.auth,
  facture: state.facture,
  user: state.user
})

export default connect(mapStateToProps, {
  getAllUserDemande,

  getAllDevisByUser,
  getFactureByUser,
  showModalUser
})(DevisFacture)
