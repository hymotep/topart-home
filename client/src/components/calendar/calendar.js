import React, { useEffect } from 'react'

import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
  getEvents,
  hideCalendar,
  updateEvent
} from '../../store/actions/event.js'
import { Modal, Button } from 'react-bootstrap'

import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import interactionPlugin from '@fullcalendar/interaction'
import timeGridPlugin from '@fullcalendar/timegrid'
import frLocale from '@fullcalendar/core/locales/fr'

const Calendar = ({
  auth: { user },
  event: { events, calendarOpen },
  getEvents,
  hideCalendar,
  updateEvent
}) => {
  useEffect(() => {
    getEvents()
  }, [])

  const dateClick = (info) => {
    // if (!data.find((elem) => elem.date === info.startStr)) {
    //   let title = prompt()
    // }
    // data.find((elem) => console.log(elem.date))
    // let title = prompt()
  }

  function renderEventContent(eventInfo) {
    return (
      <>
        {/* <div>Text : {eventInfo.timeText}</div> */}
        <div>Title : {eventInfo.event.title}</div>
      </>
    )
  }

  const handleEventChange = (changeInfo) => {
    let info = changeInfo.event.toPlainObject()
    let data = {
      id: info.extendedProps._id,
      start: info.start,
      end: info.end,
      chantier: info.extendedProps.groupeId
    }
    updateEvent(data)

    // this.props.updateEvent(changeInfo.event.toPlainObject()).catch(() => {
    //   reportNetworkError()
    //   changeInfo.revert()
    // })
  }

  return (
    <Modal show={calendarOpen} centered id="modal-cal">
      <Modal.Header>
        <Modal.Title>Calendar</Modal.Title>
        <i
          className="fas fa-times-circle fa-2x"
          id="calendartest"
          onClick={hideCalendar}
        ></i>
      </Modal.Header>
      <Modal.Body>
        {calendarOpen && (
          <div style={{ height: '700px' }}>
            <FullCalendar
              plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
              headerToolbar={{
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth,timeGridWeek'
              }}
              eventContent={renderEventContent}
              locale="fr"
              locales={[frLocale]}
              initialView="dayGridMonth"
              selectable={true}
              select={dateClick}
              height="500px"
              // validRange={{ start: today }}
              events={events}
              // events={events}
              editable={true}
              eventChange={handleEventChange}
            />
          </div>
        )}
      </Modal.Body>
      {/* <Modal.Footer>
        <Button variant="secondary" onClick={hideCalendar}>
          Close
        </Button>
        <Button variant="primary" onClick={hideCalendar}>
          Save Changes
        </Button>
      </Modal.Footer> */}
    </Modal>
  )
}

Calendar.propTypes = {
  hideCalendar: PropTypes.func.isRequired,
  getEvents: PropTypes.func.isRequired,
  updateEvent: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  event: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  event: state.event
})

export default connect(mapStateToProps, {
  getEvents,
  updateEvent,
  hideCalendar
})(Calendar)
