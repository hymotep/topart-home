const mongoose = require('mongoose')

const eventSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    required: true
  },
  start: {
    type: String,
    required: true
  },
  end: {
    type: String
  },
  groupeId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'chantier',
    required: true
  },
  id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user',
    required: true
  },
  color: { type: String, required: true },
  textcolor: { type: String, default: 'black' }
})

module.exports = Event = mongoose.model('Event', eventSchema)
