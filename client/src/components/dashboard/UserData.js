import React, { useState, useEffect } from 'react'
import { Modal, Button, Form } from 'react-bootstrap'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { hideModalUser } from '../../store/actions/modal'
import Spinner from '../layout/Spinner'

const UserData = ({
  auth: { user, loading },
  modal: { userDataOpen },
  hideModalUser
}) => {
  const [formData, setformData] = useState({})
  const [errors, setErrors] = useState({})

  const onChange = (e) => {
    setformData({ ...formData, [e.target.name]: e.target.value })
  }
  const onChange2 = (e) => {
    switch (e.target.name) {
      case 'name': {
        user.name = e.target.value
        setErrors({
          ...errors,
          ['name']: null
        })
        break
      }
      case 'adresse': {
        user.adresse = e.target.value
        setErrors({
          ...errors,
          ['adresse']: null
        })
        break
      }
      case 'codePostal': {
        user.codePostal = e.target.value
        setErrors({
          ...errors,
          ['codePostal']: null
        })
        break
      }
      default: {
      }
    }
  }

  function showPwd(id, el) {
    let x = document.getElementById(id)
    let i = el
    if (x.type === 'password') {
      x.type = 'string'

      i.className = 'fa fa-eye-slash rel-icon'
    } else {
      x.type = 'password'
      i.className = 'fa fa-eye rel-icon'
    }
  }

  const findFormErrors = (type) => {
    const newErrors = {}
    if (!user.name || user.name === '') {
      newErrors.name = 'indiquer un nom!'
    }
    if (!user.adresse || user.adresse === '') {
      newErrors.adresse = 'indiquer une adresse!'
    }
    if (!user.codePostal || user.codePostal === '') {
      newErrors.codePostal = 'indiquer un code postal!'
    }
    if (
      (!formData.oldpassword || formData.oldpassword === '') &&
      formData.passwordConfirm &&
      !user.new
    ) {
      newErrors.newpassword = 'indiquer ancien mot de passe !'
    }
    if (
      (!formData.newpassword || formData.newpassword === '') &&
      formData.newpasswordConfirm
    ) {
      newErrors.newpassword = 'indiquer mot de passe!'
    }
    if (
      (!formData.newpasswordConfirm || formData.newpasswordConfirm === '') &&
      formData.newpassword
    ) {
      newErrors.newpasswordConfirm = 'indiquer la confirmation !'
    } else if (
      formData.newpasswordConfirm &&
      formData.newpassword &&
      formData.newpassword.localCompare(formData.newpasswordConfirm) !== 0
    ) {
      newErrors.newpasswordConfirm = 'ne correspond pas !'
    }

    return newErrors
  }

  function save() {
    // get our new errors
    const newErrors = findFormErrors()
    // Conditional logic:

    if (Object.keys(newErrors).length > 0) {
      // We got errors!
      setErrors(newErrors)
    } else {
      console.log('save')
    }
  }

  return (
    <Modal show={userDataOpen} id="modal-devis" centered>
      <Modal.Header>
        <Modal.Title>Donnée utilisateur</Modal.Title>
        <i className="fas fa-times-circle fa-2x" onClick={hideModalUser}></i>
      </Modal.Header>
      <Modal.Body>
        {loading ? (
          <Spinner />
        ) : (
          user && (
            <div>
              <Form>
                <Form.Group>
                  <Form.Label>Nom </Form.Label>
                  <Form.Control
                    type="string"
                    name="name"
                    value={user.name}
                    onChange={onChange2}
                    isInvalid={!!errors.name}
                    style={{ width: '200px' }}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.name}
                  </Form.Control.Feedback>
                  <Form.Label>Adresse</Form.Label>

                  <Form.Control
                    type="string"
                    name="adresse"
                    value={user.adresse}
                    onChange={onChange2}
                    isInvalid={!!errors.adresse}
                    style={{ width: '200px' }}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.adresse}
                  </Form.Control.Feedback>
                  <Form.Label>Code Postal</Form.Label>
                  <Form.Control
                    type="number"
                    name="codePostal"
                    value={user.codePostal}
                    onChange={onChange2}
                    isInvalid={!!errors.codePostal}
                    style={{ width: '200px' }}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.codePostal}
                  </Form.Control.Feedback>

                  {!user.new && (
                    <div>
                      <Form.Label>
                        Ancien mot de passe{' '}
                        <i
                          className="fa fa-eye-slash rel-icon"
                          onClick={(e) => showPwd('oldpassword', e.target)}
                        ></i>{' '}
                      </Form.Label>
                      <Form.Control
                        type="password"
                        id="oldpassword"
                        name="oldpassword"
                        value={formData.oldpassword}
                        onChange={onChange}
                        isInvalid={!!errors.oldpassword}
                        style={{ width: '200px' }}
                      />

                      <Form.Control.Feedback type="invalid">
                        {errors.oldpassword}
                      </Form.Control.Feedback>
                    </div>
                  )}

                  <Form.Label>
                    Nouveau mot de passe{' '}
                    <i
                      className="fa fa-eye-slash rel-icon"
                      onClick={(e) => showPwd('newpassword', e.target)}
                    ></i>{' '}
                  </Form.Label>
                  <Form.Control
                    type="string"
                    id="newpassword"
                    name="newpassword"
                    value={formData.newpassword}
                    onChange={onChange}
                    isInvalid={!!errors.newpassword}
                    style={{ width: '200px' }}
                  />

                  <Form.Control.Feedback type="invalid">
                    {errors.newpassword}
                  </Form.Control.Feedback>

                  <Form.Label>
                    Nouveau mot de passe confirmation{' '}
                    <i
                      className="fa fa-eye-slash rel-icon"
                      onClick={(e) => showPwd('newpasswordConfirm', e.target)}
                    ></i>
                  </Form.Label>
                  <Form.Control
                    type="string"
                    id="newpasswordConfirm"
                    name="newpasswordConfirm"
                    value={formData.newpasswordConfirm}
                    onChange={onChange}
                    isInvalid={!!errors.newpasswordConfirm}
                    style={{ width: '200px' }}
                  />

                  <Form.Control.Feedback type="invalid">
                    {errors.newpasswordConfirm}
                  </Form.Control.Feedback>
                </Form.Group>
              </Form>
            </div>
          )
        )}
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary" onClick={save}>
          Sauver
        </Button>
      </Modal.Footer>
    </Modal>
  )
}

UserData.propTypes = {
  auth: PropTypes.object.isRequired,

  modal: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  modal: state.modal
})

export default connect(mapStateToProps, { hideModalUser })(UserData)
