import React, { useState } from 'react'

import PropTypes from 'prop-types'
import { Modal, Button, Form } from 'react-bootstrap'
import { hideEmail, sendmail } from '../../store/actions/email.js'

import { connect } from 'react-redux'

const Email = ({ hideEmail, email: { emailOpen }, sendmail }) => {
  let data = { name: '', email: '', comment: '', title: '', send: false }
  const [formData, setFormData] = useState(data)
  const [errors, setErrors] = useState({})

  const onChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    })
    if (!!errors[e.target.name])
      setErrors({
        ...errors,
        [e.target.name]: null
      })
  }

  function send() {
    formData.title = formData.title + ' pour ' + formData.name
    sendmail(formData)
  }

  return (
    <div>
      <Modal show={emailOpen} centered id="modal-cal">
        <Modal.Header id="modalheader">
          <Modal.Title>Contact Rapide</Modal.Title>
          <i
            className="fas fa-times-circle fa-2x"
            id="calendartest"
            onClick={hideEmail}
          ></i>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group>
              <Form.Label>Nom</Form.Label>
              <Form.Control
                type="string"
                onChange={onChange}
                value={formData.name}
                name="name"
              ></Form.Control>
              <Form.Label>Votre mail</Form.Label>
              <Form.Control
                type="email"
                onChange={onChange}
                value={formData.email}
                name="email"
              ></Form.Control>
              <Form.Label>Titre</Form.Label>
              <Form.Control
                type="string"
                onChange={onChange}
                value={formData.title}
                name="title"
              ></Form.Control>
              <Form.Label>Votre message</Form.Label>
              <Form.Control
                as="textarea"
                rows={7}
                onChange={onChange}
                value={formData.comment}
                name="comment"
              ></Form.Control>
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          {/* <Button variant="secondary" onClick={sendmail(formData)}>
            Close
          </Button> */}
          <Button variant="secondary" onClick={() => send()}>
            Envoyer
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  )
}

Email.propTypes = {
  hideEmail: PropTypes.func.isRequired,
  sendmail: PropTypes.func.isRequired,

  email: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  email: state.email
})
export default connect(mapStateToProps, {
  hideEmail,
  sendmail
})(Email)
